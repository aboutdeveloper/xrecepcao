﻿<?php
require_once 'header.php';
require_once 'aside.php';
require_once 'classes/paciente.class.php';
require_once 'classes/funcoes.class.php';

$p = new Paciente();
$funcoes = new Funcoes();



?>

 <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <!-- Start: Topbar -->
      <header id="topbar" class="alt">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-icon">
              <a href="saida-materiais.php">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-active">
              <a href="saida-materiais.php">Procedimentos</a>
            </li>
            <li class="crumb-trail">
              <a href="saida-materiais.php">Pesquisa de pacientes</a>
            </li>
          </ol>
        </div>
      </header>

      <!-- End: Topbar -->      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">
        <!-- begin: .tray-center -->
        <div class="tray tray-center">
          <div class="mw1000 center-block">
            <!-- Begin: Admin Form -->
            <div class="admin-form">
              <div class="panel heading-border">
                <div class="panel-body bg-light">
                    <div class="section-divider mb40" id="spy1">
                      <span>Cadastro de Pacientes</span>
                    </div>
                      <div class="row">
                        <?php 
                        if(empty($_GET['exclusao'])){
                          $_GET['exclusao'] = "";

                        }

                        if(isset($_GET['nome']) && !empty($_GET['nome']) or isset($_GET['data_nasc']) && !empty($_GET['data_nasc']) or isset($_GET['id']) && !empty($_GET['id'])){

                          $nome_paciente = addslashes($_GET['nome']);
                          $nascimento_paciente = addslashes($funcoes->entradaData($_GET['data_nasc']));
                          $id_paciente = addslashes($_GET['id']);


                          $pacientes = $p->pesquisarPaciente($nome_paciente, $nascimento_paciente, $id_paciente);

                          if(empty($pacientes)){

                            ?>
                            <div class="alert alert-warning alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              <i class="fa fa-remove pr10"></i>
                              Não foi encontrado nenhum resultado para sua pesquisa !
                            </div>

                            <?php

                          }

                        }

                        ?>

                    </div>
                    <div class="row">
                    <?php
                    // Caso o GET['exclusao'] esteja vazio ele seta pata 0 assim não aparecer nenhuma mesangem de erro do php.

                      if($_GET['exclusao'] == 1){

                      ?>
                              <div class="alert alert-success alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                  <i class="fa fa-remove pr10"></i>
                                 <strong> O Paciente foi excluido com sucesso !</strong>
                              </div>
                      <?php
                        
                     

                      }
                      if($_GET['exclusao'] == 2){

                        ?>
                          <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                  <i class="fa fa-remove pr10"></i>
                                 <strong> O produto não pode ser excluído porque está sendo utilizado em outras áreas do sistema !</strong>
                              </div>
                        <?php

                      }

                      ?>
                    </div>
                    <div class="row">
                      <a href="add-paciente.php" class="btn btn-success"><b>Adicionar Paciente</b></a>
                    </div>
                    <br>
                    <br>
                    <form method="GET">
                      <div class="row">
                        <div class="col-md-2">
                          <div class="form-group">
                            <label for="number"><b>ID:</b></label>
                            <input type="number" name="id" id="id_paciente" class="form-control" >
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="nome"><b>Nome:</b></label>
                            <input type="text" name="nome" id="nome_paciente" class="form-control">
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group">
                            <label for="nome"><b>Nascimento:</b></label>
                            <input type="text" name="data_nasc" id="maskedDate nascimento_paciente" maxlength="10" autocomplete="off" class="form-control date">
                          </div>
                        </div>
                        <div class="col-md-2" style="margin-top: 25px;">
                          <input type="submit" class="btn btn-primary btn-lg" id="btn-pesquisar" value="Pesquisar">
                        </div>
                      </div><!-- Fim da row -->
                    </form>

                    <div class="row">
                      <table class="table table-hover table-condensed table-bordered" id="pacientes">
                        <thead>
                          <tr>
                            <th style="text-align: center;">ID</th>
                            <th style="text-align: center;">Paciente</th>
                            <th style="text-align: center;">Nascimento</th>
                            <th style="text-align: center;">Ações</th>
                          </tr>
                        </thead>
                        <?php 

                        if(!empty($pacientes)){

                          foreach($pacientes as $paciente):
                          ?>

                          <tbody style="font-size: 14px;">


                          <tr>
                            <td style="text-align: center;">
                              <?php echo $paciente['id'];?>
                            </td>
                            <td style="text-align: center;">
                              <?php echo $paciente['nome'];?>
                            </td>
                            <td style="text-align: center;">
                                <?php echo $paciente['data_nasc'] = $funcoes->saidaData($paciente['data_nasc'])?>
                            </td>
                            <td>
                              <div class="btn-group">
                                  <a href="editar-paciente.php?id=<?php echo $paciente['id'];?>" class="btn btn-system" type="button">
                                    <i class="fa fa-pencil"></i>
                                  </a>
                                  <a href="excluir-paciente.php?id=<?php echo $paciente['id'];?>" class="btn btn-danger" type="button">
                                    <i class="fa fa-trash-o"></i>
                                  </a>
                                </div>
                              
                            </td>
                          </tr>

                          <?php
                        endforeach;

                        }

                          
                        ?>
                        
                        

                        </tbody>
                      </table>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- end: .tray-center -->
      </section>
      <!-- End: Content -->
    </section>
  </div>
  <!-- End: Main -->

  <style>
  /* demo page styles */
  body { min-height: 2300px; }
  
  .content-header b,
  .admin-form .panel.heading-border:before,
  .admin-form .panel .heading-border:before {
    transition: all 0.7s ease;
  }
  /* responsive demo styles */
  @media (max-width: 800px) {
    .admin-form .panel-body { padding: 18px 12px; }
    .option-group .option { display: block; }
    .option-group .option + .option { margin-top: 8px; }
  }
  </style>
    
<!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- Time/Date Plugin Dependencies -->
  <script src="vendor/plugins/globalize/globalize.min.js"></script>
  <script src="vendor/plugins/moment/moment.min.js"></script>

  <!-- BS Dual Listbox Plugin -->
  <script src="vendor/plugins/duallistbox/jquery.bootstrap-duallistbox.min.js"></script>

  <!-- Bootstrap Maxlength plugin -->
  <script src="vendor/plugins/maxlength/bootstrap-maxlength.min.js"></script>

  <!-- Select2 Plugin Plugin -->
  <script src="vendor/plugins/select2/select2.min.js"></script>

  <!-- Typeahead Plugin -->
  <script src="vendor/plugins/typeahead/typeahead.bundle.min.js"></script>

  <!-- TagManager Plugin -->
  <script src="vendor/plugins/tagmanager/tagmanager.js"></script>

  <!-- DateRange Plugin -->
  <script src="vendor/plugins/daterange/daterangepicker.min.js"></script>

  <!-- DateTime Plugin -->
  <script src="vendor/plugins/datepicker/js/bootstrap-datetimepicker.min.js"></script>

  <!-- BS Colorpicker Plugin -->
  <script src="vendor/plugins/colorpicker/js/bootstrap-colorpicker.min.js"></script>

  <!-- MaskedInput Plugin -->
  <script src="vendor/plugins/jquerymask/jquery.maskedinput.min.js"></script>

      <!-- Datatables -->
  <script src="vendor/plugins/datatables/media/js/jquery.dataTables.js"></script>

  <!-- Datatables Tabletools addon -->
  <script src="vendor/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>

  <!-- Datatables ColReorder addon -->
  <script src="vendor/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>

  <!-- Datatables Bootstrap Modifications  -->
  <script src="vendor/plugins/datatables/media/js/dataTables.bootstrap.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  jQuery(document).ready(function() {

    "use strict";

    // Init Theme Core    
    Core.init();



    // Init Select2 - Basic Single
    $(".cidades").select2();



    // Init Select2 - Contextuals (via html classes)
    $(".select2-primary").select2(); // select2 contextual - primary
    $(".select2-success").select2(); // select2 contextual - success
    $(".select2-info").select2();    // select2 contextual - info
    $(".select2-warning").select2(); // select2 contextual - warning  

    // Init Bootstrap Maxlength Plugin
    $('input[maxlength]').maxlength({
      threshold: 15,
      placement: "right"
    });

    // Dual List Plugin Init
    var demo1 = $('.demo1').bootstrapDualListbox({
      nonSelectedListLabel: 'Options',
      selectedListLabel: 'Selected',
      preserveSelectionOnMove: 'moved',
      moveOnSelect: true,
      nonSelectedFilter: 'ion ([7-9]|[1][0-2])'
    });

    $("#demoform").submit(function() {
      alert("Options Selected: " + $('.demo1').val());
      return false;
    });

    // Init Twitter Typeahead.js
    var substringMatcher = function(strs) {
      return function findMatches(q, cb) {
        var matches, substrRegex;

        // an array that will be populated with substring matches
        matches = [];

        // regex used to determine if a string contains the substring `q`
        substrRegex = new RegExp(q, 'i');

        // iterate through the pool of strings and for any string that
        // contains the substring `q`, add it to the `matches` array
        $.each(strs, function(i, str) {
          if (substrRegex.test(str)) {
            // the typeahead jQuery plugin expects suggestions to a
            // JavaScript object, refer to typeahead docs for more info
            matches.push({
              value: str
            });
          }
        });

        cb(matches);
      };
    };

    var states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California',
      'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii',
      'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana',
      'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota',
      'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire',
      'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota',
      'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island',
      'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont',
      'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'
    ];

    // Init Typeahead Plugin with state aray
    $('.typeahead').typeahead({
      hint: true,
      highlight: true,
      minLength: 1
    }, {
      name: 'states',
      displayKey: 'value',
      source: substringMatcher(states)
    });

    // DateRange plugin options
    var rangeOptions = {
      ranges: {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
        'Last 7 Days': [moment().subtract('days', 6), moment()],
        'Last 30 Days': [moment().subtract('days', 29), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
      },
      startDate: moment().subtract('days', 29),
      endDate: moment()
    }

    // Init DateRange plugin
    $('#daterangepicker1').daterangepicker();

    // Init DateRange plugin
    $('#daterangepicker2').daterangepicker(
      rangeOptions,
      function(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      }
    );

    // Init DateRange plugin
    $('#inline-daterange').daterangepicker(
      rangeOptions,
      function(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      }
    );

    // Init DateTimepicker - fields
    $('#datetimepicker1').datetimepicker();
    $('#datetimepicker2').datetimepicker();

    // Init DateTimepicker - inline + range detection
    $('#datetimepicker3').datetimepicker({
      defaultDate: "9/4/2014",
      inline: true,
    });

    // Init DateTimepicker - fields + Date disabled (only time picker)
    $('#datetimepicker5').datetimepicker({
      defaultDate: "9/25/2014",
      pickDate: false,
    });
    // Init DateTimepicker - fields + Date disabled (only time picker)
    $('#datetimepicker6').datetimepicker({
      defaultDate: "9/25/2014",
      pickDate: false,
    });
    // Init DateTimepicker - inline + Date disabled (only time picker)
    $('#datetimepicker7').datetimepicker({
      defaultDate: "9/25/2014",
      pickDate: false,
      inline: true
    });

    // Init Colorpicker plugin
    $('#demo_apidemo').colorpicker({
      color: bgPrimary
    });
    $('.demo-auto').colorpicker();

    // Init jQuery Tags Manager 
    $(".tm-input").tagsManager({
      tagsContainer: '.tags',
      prefilled: ["Miley Cyrus", "Apple", "A Long Tag", "Na uh"],
      tagClass: 'tm-tag-info',
    });

    // Init Boostrap Multiselects
    $('#multiselect1').multiselect();
    $('#multiselect2').multiselect({
      includeSelectAllOption: true
    });
    $('#multiselect3').multiselect();
    $('#multiselect4').multiselect({
      enableFiltering: true,
    });
    $('#multiselect5').multiselect({
      buttonClass: 'multiselect dropdown-toggle btn btn-primary'
    });
    $('#multiselect6').multiselect({
      buttonClass: 'multiselect dropdown-toggle btn btn-info'
    });
    $('#multiselect7').multiselect({
      buttonClass: 'multiselect dropdown-toggle btn btn-success'
    });
    $('#multiselect8').multiselect({
      buttonClass: 'multiselect dropdown-toggle btn btn-warning'
    });

    // Init jQuery spinner init - default
    $("#spinner1").spinner();

    // Init jQuery spinner init - currency 
    $("#spinner2").spinner({
      min: 5,
      max: 2500,
      step: 25,
      start: 1000,
      //numberFormat: "C"
    });

    // Init jQuery spinner init - decimal  
    $("#spinner3").spinner({
      step: 0.01,
      numberFormat: "n"
    });

    // jQuery Time Spinner settings
    $.widget("ui.timespinner", $.ui.spinner, {
      options: {
        // seconds
        step: 60 * 1000,
        // hours
        page: 60
      },
      _parse: function(value) {
        if (typeof value === "string") {
          // already a timestamp
          if (Number(value) == value) {
            return Number(value);
          }
          return +Globalize.parseDate(value);
        }
        return value;
      },

      _format: function(value) {
        return Globalize.format(new Date(value), "t");
      }
    });

    // Init jQuery Time Spinner
    $("#spinner4").timespinner();

    // Init jQuery Masked inputs
    $('.date').mask('99/99/9999');
    $('.time').mask('99:99:99');
    $('.date_time').mask('99/99/9999 99:99:99');
    $('.zip').mask('99999-999');
    $('.phone').mask('(999) 999-9999');
    $('.phoneext').mask("(999) 999-9999 x99999");
    $(".money").mask("999,999,999.999");
    $(".product").mask("999.999.999.999");
    $(".tin").mask("99-9999999");
    $(".ssn").mask("999-99-9999");
    $(".ip").mask("9ZZ.9ZZ.9ZZ.9ZZ");
    $(".eyescript").mask("~9.99 ~9.99 999");
    $(".custom").mask("9.99.999.9999");

     // Init DataTables
    $('#datatable').dataTable({
      "sDom": 't<"dt-panelfooter clearfix"ip>',
      "oTableTools": {
        "sSwfPath": "vendor/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
      }
    });

    $('#pacientes').dataTable({
      "aoColumnDefs": [{
        'bSortable': false,
        'aTargets': [1],
        
      }],
      "oLanguage": {
        "oPaginate": {
          "sPrevious": "Anterior",
          "sNext": "Proximo",
          "sZeroRecords": "No records to display",
          "sEmptyTable": "Sem registros ",
           "sSearch": "Filter records:",


        }
      },
      "iDisplayLength": 2,
      "aLengthMenu": [
        [5, 10, 25, 50, -1],
        [5, 10, 25, 50, "All"]
      ],
      "sDom": '<"dt-panelmenu clearfix"lfr>t<"dt-panelfooter clearfix"ip>',
      "oTableTools": {
        "sSwfPath": "vendor/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
      }
    });

    $('#datatable3').dataTable({
      "aoColumnDefs": [{
        'bSortable': false,
        'aTargets': [-1]
      }],
      "oLanguage": {
        "oPaginate": {
          "sPrevious": "",
          "sNext": ""
        }
      },
      "iDisplayLength": 5,
      "aLengthMenu": [
        [5, 10, 25, 50, -1],
        [5, 10, 25, 50, "All"]
      ],
      "sDom": '<"dt-panelmenu clearfix"Tfr>t<"dt-panelfooter clearfix"ip>',
      "oTableTools": {
        "sSwfPath": "vendor/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
      }
    });

    $('#datatable4').dataTable({
      "aoColumnDefs": [{
        'bSortable': false,
        'aTargets': [-1]
      }],
      "oLanguage": {
        "oPaginate": {
          "sPrevious": "",
          "sNext": ""
        }
      },
      "iDisplayLength": 5,
      "aLengthMenu": [
        [5, 10, 25, 50, -1],
        [5, 10, 25, 50, "All"]
      ],
      "sDom": 'T<"panel-menu dt-panelmenu"lfr><"clearfix">tip',

      "oTableTools": {
        "sSwfPath": "vendor/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
      }
    });

    // Multi-Column Filtering
    $('#datatable5 thead th').each(function() {
      var title = $('#datatable5 tfoot th').eq($(this).index()).text();
      $(this).html('<input type="text" class="form-control" placeholder="Search ' + title + '" />');
    });

    // DataTable
    var table5 = $('#datatable5').DataTable({
      "sDom": 't<"dt-panelfooter clearfix"ip>',
      "ordering": false
    });

    // Apply the search
    table5.columns().eq(0).each(function(colIdx) {
      $('input', table5.column(colIdx).header()).on('keyup change', function() {
        table5
          .column(colIdx)
          .search(this.value)
          .draw();
      });
    });

    // ABC FILTERING
    var table6 = $('#datatable6').DataTable({
      "sDom": 't<"dt-panelfooter clearfix"ip>',
      "ordering": false
    });

    var alphabet = $('<div class="dt-abc-filter"/>').append('<span class="abc-label">Search: </span> ');
    var columnData = table6.column(0).data();
    var bins = bin(columnData);

    $('<span class="active"/>')
      .data('letter', '')
      .data('match-count', columnData.length)
      .html('None')
      .appendTo(alphabet);

    for (var i = 0; i < 26; i++) {
      var letter = String.fromCharCode(65 + i);

      $('<span/>')
        .data('letter', letter)
        .data('match-count', bins[letter] || 0)
        .addClass(!bins[letter] ? 'empty' : '')
        .html(letter)
        .appendTo(alphabet);
    }

    $('#datatable6').parents('.panel').find('.panel-menu').addClass('dark').html(alphabet);

    alphabet.on('click', 'span', function() {
      alphabet.find('.active').removeClass('active');
      $(this).addClass('active');

      _alphabetSearch = $(this).data('letter');
      table6.draw();
    });

    var info = $('<div class="alphabetInfo"></div>')
      .appendTo(alphabet);

    var _alphabetSearch = '';

    $.fn.dataTable.ext.search.push(function(settings, searchData) {
      if (!_alphabetSearch) {
        return true;
      }
      if (searchData[0].charAt(0) === _alphabetSearch) {
        return true;
      }
      return false;
    });

    function bin(data) {
      var letter, bins = {};
      for (var i = 0, ien = data.length; i < ien; i++) {
        letter = data[i].charAt(0).toUpperCase();

        if (bins[letter]) {
          bins[letter]++;
        } else {
          bins[letter] = 1;
        }
      }
      return bins;
    }

    // ROW GROUPING
    var table7 = $('#datatable7').DataTable({
      "columnDefs": [{
        "visible": false,
        "targets": 2
      }],
      "order": [
        [2, 'asc']
      ],
      "sDom": 't<"dt-panelfooter clearfix"ip>',
      "displayLength": 25,
      "drawCallback": function(settings) {
        var api = this.api();
        var rows = api.rows({
          page: 'current'
        }).nodes();
        var last = null;

        api.column(2, {
          page: 'current'
        }).data().each(function(group, i) {
          if (last !== group) {
            $(rows).eq(i).before(
              '<tr class="row-label ' + group.replace(/ /g, '').toLowerCase() + '"><td colspan="5">' + group + '</td></tr>'
            );
            last = group;
          }
        });
      }
    });

    // Order by the grouping
    $('#datatable7 tbody').on('click', 'tr.row-label', function() {
      var currentOrder = table7.order()[0];
      if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
        table7.order([2, 'desc']).draw();
      } else {
        table7.order([2, 'asc']).draw();
      }
    });

    $('#datatable8').DataTable({
      "sDom": 'Rt<"dt-panelfooter clearfix"ip>',
    });


    // COLLAPSIBLE ROWS
    function format ( d ) {
      // `d` is the original data object for the row
      return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
        '<tr>'+
          '<td class="fw600 pr10">Full name:</td>'+
          '<td>'+d.name+'</td>'+
        '</tr>'+
        '<tr>'+
          '<td class="fw600 pr10">Extension:</td>'+
          '<td>'+d.extn+'</td>'+
        '</tr>'+
        '<tr>'+
          '<td class="fw600 pr10">Extra info:</td>'+
          '<td>And any further details here (images etc)...</td>'+
        '</tr>'+
      '</table>';
    }
     
    var table = $('#datatable9').DataTable({
      "sDom": 'Rt<"dt-panelfooter clearfix"ip>',
      "ajax": "vendor/plugins/datatables/examples/data_sources/objects.txt",
      "columns": [
        {
          "className":      'details-control',
          "orderable":      false,
          "data":           null,
          "defaultContent": ''
        },
        { "data": "name" },
        { "data": "position" },
        { "data": "office" },
        { "data": "salary" }
      ],
      "order": [[1, 'asc']]
    });
     
    // Add event listener for opening and closing details
    $('#datatable9 tbody').on('click', 'td.details-control', function () {
      var tr = $(this).closest('tr');
      var row = table.row( tr );

      if ( row.child.isShown() ) {
        // This row is already open - close it
        row.child.hide();
        tr.removeClass('shown');
      }
      else {
        // Open this row
        row.child( format(row.data()) ).show();
        tr.addClass('shown');
      }
    });


    // MISC DATATABLE HELPER FUNCTIONS

    // Add Placeholder text to datatables filter bar
    $('.dataTables_filter input').attr("placeholder", "Enter Terms...");
    // FIM ADDON DATA TABLE

  });
  </script>
</body>

</html>
