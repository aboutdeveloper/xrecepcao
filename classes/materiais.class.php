<?php 

class Materiais{


	public function getTotalEntradas(){

		global $pdo;

		$sql = $pdo->query("SELECT COUNT(*) AS c FROM requisicaoentrada");
		$contagem = $sql->fetch();

		return $contagem['c'];

	}

		public function getEntradaMateriais($id){

		global $pdo;



		//$data_nova = date('Y/m/d', strtotime($data_inicial));

		$array = array();


		$sql = $pdo->prepare("
			SELECT
			r.id,
			u.nome AS usuario,
			r.id_fornecedor,
			r.nota_fiscal,
			r.data_baixa,
			r.obs,
			r.valor_req_total,
            r.excluido
			FROM 
			requisicaoentrada r
			INNER JOIN usuarios u ON (u.id = r.id_usuario)
			INNER JOIN fornecedor d ON (d.id = r.id_fornecedor) WHERE r.id = :id AND r.excluido = '2'; ");

		$sql->bindValue(':id',$id);
		$sql->execute();

		if($sql->rowCount() > 0){

			$array = $sql->fetchAll();


		}

		return $array;

	}

	public function getSaidasalva($id){



		global $pdo;



		//$data_nova = date('Y/m/d', strtotime($data_inicial));

		$array = array();


		$sql = $pdo->prepare("
			SELECT
			r.id,
			u.nome AS usuario,
			r.id_departamento,
			d.nome as nome_departamento,
			r.data_baixa,
			r.situacao,
			r.obs,
			r.valor_req_total
			FROM 
			requisicaosaida r
			INNER JOIN usuarios u ON (u.id = r.id_usuario)
			INNER JOIN departamento d ON (d.id = r.id_departamento) WHERE r.id = :id AND r.excluido = '1'");

		$sql->bindValue(':id',$id);
		$sql->execute();

		if($sql->rowCount() > 0){

			$array = $sql->fetchAll();


		}

		return $array;

	}
		public function getSaidaMateriais($id){



		global $pdo;



		//$data_nova = date('Y/m/d', strtotime($data_inicial));

		$array = array();


		$sql = $pdo->prepare("
			SELECT
			r.id,
			u.nome AS usuario,
			r.id_departamento,
			d.nome as nome_departamento,
			r.data_baixa,
			r.situacao,
			r.obs,
			r.valor_req_total
			FROM 
			requisicaosaida r
			INNER JOIN usuarios u ON (u.id = r.id_usuario)
			INNER JOIN departamento d ON (d.id = r.id_departamento) WHERE r.id = :id AND r.excluido = '2'");

		$sql->bindValue(':id',$id);
		$sql->execute();

		if($sql->rowCount() > 0){

			$array = $sql->fetchAll();


		}

		return $array;

	}

	public function getEntradasMateriais(){

		global $pdo;



		//$data_nova = date('Y/m/d', strtotime($data_inicial));

		$array = array();


		$sql = $pdo->prepare("
			SELECT
			r.id,
			u.nome AS usuario,
			d.nome_social AS fornecedor,
			r.nota_fiscal,
			r.data_baixa,
			r.obs,
			r.valor_req_total
			FROM 
			requisicaoentrada r
			INNER JOIN usuarios u ON (u.id = r.id_usuario)
			INNER JOIN fornecedor d ON (d.id = r.id_fornecedor) WHERE excluido = '2' ORDER BY data_baixa DESC ");


		$sql->execute();

		if($sql->rowCount() > 0){

			$array = $sql->fetchAll();


		}

		return $array;

	}

	public function getSaidasMateriais(){

		global $pdo;



		//$data_nova = date('Y/m/d', strtotime($data_inicial));

		$array = array();


		$sql = $pdo->prepare("
			SELECT
			r.id,
			u.nome AS usuario,
			u.id AS id_usuario,
			d.nome AS departamento,
			r.data_baixa,
			r.situacao
			FROM 
			requisicaosaida r
			INNER JOIN usuarios u ON (u.id = r.id_usuario)
			INNER JOIN departamento d ON (d.id = r.id_departamento) WHERE excluido = '2' ORDER BY data_baixa DESC ");


		$sql->execute();

		if($sql->rowCount() > 0){

			$array = $sql->fetchAll();


		}

		return $array;

	}

	public function getSaidasMateriaisReq(){

		global $pdo;



		//$data_nova = date('Y/m/d', strtotime($data_inicial));

		$array = array();


		$sql = $pdo->prepare("
			SELECT
			r.id,
			u.nome AS usuario,
			u.id AS id_usuario,
			d.nome AS departamento,
			r.data_baixa,
			r.situacao
			FROM 
			requisicaosaida r
			INNER JOIN usuarios u ON (u.id = r.id_usuario)
			INNER JOIN departamento d ON (d.id = r.id_departamento) WHERE excluido = '2' AND id_usuario = :id_usuario ORDER BY data_baixa DESC ");

		$sql->bindValue(':id_usuario',$_SESSION['cLogin']);
		$sql->execute();

		if($sql->rowCount() > 0){

			$array = $sql->fetchAll();


		}

		return $array;

	}

		public function getItemReqEntrada($id_req_entrada){

		global $pdo;



		//$data_nova = date('Y/m/d', strtotime($data_inicial));

		$array = array();


		$sql = $pdo->prepare("SELECT id_produto, qtd_produto, valor_unitario, valor_total FROM itemreqentrada WHERE id_req_entrada = :id_req_entrada");
		$sql->bindValue(':id_req_entrada',$id_req_entrada);
		$sql->execute();

		if($sql->rowCount() > 0){

			$array = $sql->fetchAll();


		}

		return $array;

	}

	public function getItensReqSaida($id){

		global $pdo;



		//$data_nova = date('Y/m/d', strtotime($data_inicial));

		$array = array();


		$sql = $pdo->prepare("SELECT id, id_produto, qtd_produto, valor_unitario, valor_total FROM itemreqsaida WHERE id_req_saida = :id_req_saida");
		$sql->bindValue(':id_req_saida', $id);
		$sql->execute();

		if($sql->rowCount() > 0){

			$array = $sql->fetchAll();


		}

		return $array;

	}

	public function getItensReqSalva($id){

		global $pdo;



		//$data_nova = date('Y/m/d', strtotime($data_inicial));

		$array = array();


		$sql = $pdo->prepare("SELECT id, id_produto, qtd_produto, valor_unitario, valor_total FROM itemreqsaida WHERE id_req_saida = :id_req_saida");
		$sql->bindValue(':id_req_saida', $id);
		$sql->execute();

		if($sql->rowCount() > 0){

			$array = $sql->fetchAll();


		}

		return $array;

	}

		public function getItensReqSaidaImp($id){

		global $pdo;



		//$data_nova = date('Y/m/d', strtotime($data_inicial));

		$array = array();


		$sql = $pdo->prepare("SELECT 
			itrs.id,
			itrs.id_req_saida,
			itrs.id_produto, 
			p.nome as nome_produto,
			itrs.qtd_produto,
			itrs.valor_unitario,
			itrs.valor_total 
			FROM itemreqsaida itrs
			INNER JOIN produtos p ON (p.id = itrs.id_produto)
			WHERE itrs.id_req_saida = :id_req_saida");

		$sql->bindValue(':id_req_saida', $id);
		$sql->execute();

		if($sql->rowCount() > 0){

			$array = $sql->fetchAll();


		}

		return $array;

	}

	public function getIdReqEntrada(){
		global $pdo;

		$sql = $pdo->query("SELECT id as id_requisicao FROM requisicaoentrada  ORDER BY id DESC limit 1");
		$sql->execute();

		$id_requisicao = $sql->fetch();


		return $id_requisicao['id_requisicao'];


	}

	public function addEntradaMateriais($data_baixa, $id_fornecedor, $nota_fiscal, $produtos, $observacao){

			global $pdo;
			$totalRequisicao = 0;
			$totalProduto = 0;


			foreach($produtos as $produto){
				

				
				$id_produto = $produto['id'];
				$qtd_produto = $produto['qtd'];
				$custo_produto = str_replace(",", ".", $produto['custo']);

				$totalProduto = $qtd_produto * $custo_produto;

				$totalRequisicao = $totalRequisicao + $totalProduto;

				echo "ID ".$id_produto."<br>";
				echo "QTD ".$qtd_produto."<br>";
				echo "CUSTO ".$custo_produto."<br>";
				echo "TOTAL PRODUTO ".$totalProduto."<br>";
				echo "TOTAL REQUISICAO ".$totalRequisicao."<br>";


				$consulta = $pdo->query("SELECT id as id_requisicao FROM requisicaoentrada ORDER BY id DESC limit 1");

				$consulta->execute();
				$resultado = $consulta->fetch();



				$id_req_entrada = $resultado['id_requisicao'];


				$id_req_entrada = $id_req_entrada = $id_req_entrada + 1;

				if(empty($id_req_entrada)){

					$id_req_entrada = 1;

				}

				$consulta1 = $pdo->prepare("INSERT INTO itemreqentrada SET id_req_entrada = :id_req_entrada, data_baixa = :data_baixa, id_fornecedor = :id_fornecedor, id_produto = :id_produto, qtd_produto = :qtd_produto,  valor_unitario = :valor_unitario, valor_total = :valor_total, excluido = '2'");



				$consulta1->bindValue(':id_req_entrada',$id_req_entrada);
				$consulta1->bindValue(':data_baixa',$data_baixa);
				$consulta1->bindValue(':id_fornecedor',$id_fornecedor);
				$consulta1->bindValue(':id_produto',$id_produto);
				$consulta1->bindValue(':qtd_produto',$qtd_produto);
				$consulta1->bindValue(':valor_unitario',$custo_produto);
				$consulta1->bindValue(':valor_total',$totalProduto);
				$consulta1->execute();

				

				}


			$consulta2 = $pdo->prepare("INSERT INTO requisicaoentrada SET id_usuario = :id_usuario, id_fornecedor = :id_fornecedor, nota_fiscal = :nota_fiscal, data_baixa = :data_baixa,  valor_req_total = :valor_req_total, excluido = '2', obs = :obs");
			$consulta2->bindValue(':id_usuario',$_SESSION['cLogin']);
			$consulta2->bindValue(':id_fornecedor',$id_fornecedor);
			$consulta2->bindValue(':nota_fiscal',$nota_fiscal);
			$consulta2->bindValue(':data_baixa',$data_baixa);
			$consulta2->bindValue(':valor_req_total',$totalRequisicao);
			$consulta2->bindValue(':obs',$observacao);
			$consulta2->execute();

			return true;

			} 

			public function rel_preco_unitario($data_inicial, $data_final, $zerados = ''){



			global $pdo;
			// Caso seja setado como um 1 signfica que usuário quer ver os materiais zerados e por isso ele executa esse código
			if($zerados == 1){



			// Obtém o valor total das entradas do produto
			$consulta1 = $pdo->query("SELECT id, nome FROM produtos WHERE ativo = '1'");
			$consulta1->execute();

			$produtos = $consulta1->fetchAll();
			$i = 0;
			foreach($produtos as $produto){


				
				$id_produto = $produto['id']."<br>";
				$nome_produto = $produto['nome']."<br>";

				$consulta[$i]['id_produto'] = $id_produto;
				$consulta[$i]['nome_produto'] = $nome_produto;

				// Obtém o valor de todas as entradas
				$consulta1 = $pdo->prepare("SELECT SUM(qtd_produto) AS total_entradas FROM itemreqentrada WHERE data_baixa BETWEEN :data_inicial AND :data_final AND id_produto = :id_produto AND excluido = '2'");

				
				/*

				Funciona 

				SELECT SUM(qtd_produto) AS total_entradas FROM itemreqentrada WHERE data_baixa BETWEEN '2019-01-01' AND '2019-12-31' AND id_produto = '532' AND excluido = '2';

				"SELECT SUM(qtd_produto) AS total_entradas FROM itemreqentrada WHERE data_baixa BETWEEN :data_inicial AND :data_final AND id_produto = :id_produto AND excluido = '2'

				*/
				$consulta1->bindValue(':id_produto',$id_produto);
				$consulta1->bindValue(':data_inicial',$data_inicial);
				$consulta1->bindValue(':data_final',$data_final);
				$consulta1->execute();

				
				$totalEntradas = $consulta1->fetch();



				// Obtém o valor total das saídas do produto
				$consulta2 = $pdo->prepare("SELECT SUM(qtd_produto) AS total_saidas FROM itemreqsaida WHERE data_baixa BETWEEN :data_inicial AND :data_final AND id_produto = :id_produto AND situacao = '1' AND excluido = '2'");


				$consulta2->bindValue(':id_produto',$id_produto);
				$consulta2->bindValue(':data_inicial',$data_inicial);
				$consulta2->bindValue(':data_final',$data_final);


				$consulta2->execute();


				// Apresenta  o total de saidas
				$totalSaidas = $consulta2->fetch();
				//echo $produto['id']."-";
				//echo $totalSaidas['total_saidas']."<br>";





				 $saldoAtual = $totalEntradas['total_entradas'] - $totalSaidas['total_saidas'];



				 $consulta[$i]['saldo_fisico'] = $saldoAtual;

 				// Obtém a média do valor unitário do produto
				$consulta3 = $pdo->prepare("SELECT valor_unitario FROM itemreqentrada WHERE  id_produto = :id_produto AND excluido = '2'");

				$consulta3->bindValue(':id_produto',$id_produto);
				$consulta3->execute();

				$valor_unitario = $consulta3->fetch();

				
					
				/*

				$consulta3 = $pdo->prepare("SELECT AVG(valor_unitario) AS valor_medio FROM itemreqentrada WHERE data_baixa BETWEEN '1-1-1' AND NOW() AND id_produto = :id_produto");
				$consulta3->bindValue(':id_produto',$id_produto);
				$consulta3->execute();

				$valor_unitario = $consulta3->fetch();
				*/

				// Calcula o saldo financeiro do produto
				//$saldo_financeiro = $saldoAtual * $valor_unitario['valor_medio'];

				//$consulta[$i]['saldo_financeiro'] = $saldo_financeiro;
				$consulta[$i]['valor_unitario'] = $valor_unitario['valor_unitario'];


				/*
				// Calcula o saldo físico do produto
				$saldoTotal = $totalEntradas['total_entradas'] * $valor_unitario['valor_medio'];
				/*

				
				



				/*
				

				echo "O saldo total é: ".$saldoTotal."<br>";
				echo "O nome do produto é: ".$nome_p"<br>"
				echo "A quantidade total de entradas do produto é: ".$totalEntradas['total_entradas']."<br>";
				echo "A quantiadade total atual do produto é: ".$saldoAtual."<br>";
				echo "O saldo médio unitário é: ".$valor_unitario['valor_medio']."<br>";

				
				echo "O saldo financeiro atual é ".$saldo_financeiro;
			    */

				$i++;

			}
			
			return $consulta;



			}else{


			// Obtém o valor total das entradas do produto
			$consulta1 = $pdo->query("SELECT id, nome FROM produtos WHERE ativo = '1'");
			$consulta1->execute();

			$produtos = $consulta1->fetchAll();
			$i = 0;
			foreach($produtos as $produto){


				
				$id_produto = $produto['id']."<br>";
				$nome_produto = $produto['nome']."<br>";

				// Obtém todas as entradas do produto
				$consulta1 = $pdo->prepare("SELECT SUM(qtd_produto) AS total_entradas FROM itemreqentrada WHERE data_baixa BETWEEN :data_inicial AND :data_final AND id_produto = :id_produto AND excluido = '2'");

				$consulta1->bindValue(':id_produto',$id_produto);
				$consulta1->bindValue(':data_inicial',$data_inicial);
				$consulta1->bindValue(':data_final',$data_final);
				$consulta1->execute();

				// Apresenta o total de entradas do prduto
				$totalEntradas = $consulta1->fetch();



				// Obtém o valor total das saídas do produto
				$consulta2 = $pdo->prepare("SELECT SUM(qtd_produto) AS total_saidas FROM itemreqsaida WHERE data_baixa BETWEEN :data_inicial AND :data_final AND id_produto = :id_produto AND situacao = '1' AND excluido = '2'");


				$consulta2->bindValue(':id_produto',$id_produto);
				$consulta2->bindValue(':data_inicial',$data_inicial);
				$consulta2->bindValue(':data_final',$data_final);


				$consulta2->execute();


				// Apresenta  o total de saidas
				$totalSaidas = $consulta2->fetch();
				//echo $produto['id']."-";
				//echo $totalSaidas['total_saidas']."<br>";





				 $saldoAtual = $totalEntradas['total_entradas'] - $totalSaidas['total_saidas'];


 				// Obtém a média do valor unitário do produto
				$consulta3 = $pdo->prepare("SELECT AVG(valor_unitario) AS valor_medio FROM itemreqentrada WHERE data_baixa BETWEEN :data_inicial AND :data_final AND id_produto = :id_produto AND excluido = '2'");

				$consulta3->bindValue(':id_produto',$id_produto);
				$consulta3->bindValue(':data_inicial',$data_inicial);
				$consulta3->bindValue(':data_final',$data_final);
				$consulta3->execute();

				$valor_unitario = $consulta3->fetch();

				// Calcula o saldo financeiro do produto
				$saldo_financeiro = $saldoAtual * $valor_unitario['valor_medio'];



				// Calcula o saldo físico do produto
				$saldoTotal = $totalEntradas['total_entradas'] * $valor_unitario['valor_medio'];

			
				/*
				

				echo "O saldo total é: ".$saldoTotal."<br>";
				echo "O nome do produto é: ".$nome_p"<br>"
				echo "A quantidade total de entradas do produto é: ".$totalEntradas['total_entradas']."<br>";
				echo "A quantiadade total atual do produto é: ".$saldoAtual."<br>";
				echo "O saldo médio unitário é: ".$valor_unitario['valor_medio']."<br>";

				
				echo "O saldo financeiro atual é ".$saldo_financeiro;
			    */

			    if($saldoAtual > 0){

				$consulta[$i]['id_produto'] = $id_produto;
				$consulta[$i]['nome_produto'] = $nome_produto;
			    $consulta[$i]['saldo_fisico'] = $saldoAtual;
 				$consulta[$i]['saldo_financeiro'] = $saldo_financeiro;

 				$i++;

 			  }

			}// Fim do foreach
			
			return $consulta;

		
			}// Fim do else

		}//Fim da função




			public function EditarEntradaMateriais($data_baixa, $id_fornecedor, $nota_fiscal, $produtos, $id_usuario,  $id_requisicao){

			global $pdo;
			$totalRequisicao = 0;
			
			$consulta = $pdo->prepare("DELETE FROM requisicaoentrada WHERE id = :id_requisicao");
			$consulta->bindValue(":id_requisicao", $id_requisicao);
			$consulta->execute();

			$consulta = $pdo->prepare("DELETE FROM itemreqentrada WHERE id_req_entrada = :id_requisicao");
			$consulta->bindValue(":id_requisicao", $id_requisicao);
			$consulta->execute();

			foreach($produtos as $produto){
				
				

				
				$id_produto = $produto['id'];
				$qtd_produto = $produto['qtd'];
				$custo_produto = str_replace(",", ".", $produto['custo']);

				$totalProduto = $qtd_produto * $custo_produto;

				$totalRequisicao = $totalRequisicao + $totalProduto;


				$consulta1 = $pdo->prepare("INSERT INTO itemreqentrada SET id_req_entrada = :id_requisicao, data_baixa = :data_baixa, id_fornecedor = :id_fornecedor, id_produto = :id_produto, qtd_produto = :qtd_produto,  valor_unitario = :valor_unitario, valor_total = :valor_total, excluido = '2'");

				$consulta1->bindValue(':id_requisicao',$id_requisicao);
				$consulta1->bindValue(':data_baixa',$data_baixa);
				$consulta1->bindValue(':id_fornecedor',$id_fornecedor);
				$consulta1->bindValue(':id_produto',$id_produto);
				$consulta1->bindValue(':qtd_produto',$qtd_produto);
				$consulta1->bindValue(':valor_unitario',$custo_produto);
				$consulta1->bindValue(':valor_total',$totalProduto);
				$consulta1->execute();


				}

				$consulta2 = $pdo->prepare("INSERT INTO requisicaoentrada SET id = :id_requisicao, id_usuario = :id_usuario, id_fornecedor = :id_fornecedor, nota_fiscal = :nota_fiscal, data_baixa = :data_baixa,  valor_req_total = :valor_req_total, excluido = '2'");

				$consulta2->bindValue(':id_requisicao',$id_requisicao);
				$consulta2->bindValue(':id_usuario',$_SESSION['cLogin']);
				$consulta2->bindValue(':id_fornecedor',$id_fornecedor);
				$consulta2->bindValue(':nota_fiscal',$nota_fiscal);
				$consulta2->bindValue(':data_baixa',$data_baixa);
				$consulta2->bindValue(':valor_req_total',$totalRequisicao);
				$consulta2->execute();

				return true;

			} 
			

		public function verificaSaldo($data_baixa, $id_departamento, $id_usuario, $produtos){



			/*
			echo $data_baixa;
			echo $id_departamento;
			echo $id_usuario;
			print_r($produtos);
			*/

			global $pdo;
			$totalRequisicao = 0;
			$i = 0;


			foreach($produtos as $produto){




				$id_produto = $produto['id'];
				$qtd_produto = $produto['qtd'];





				// Obtém o valor total das entradas do produto
				$consulta1 = $pdo->prepare("SELECT SUM(qtd_produto) AS total_entradas FROM itemreqentrada WHERE data_baixa BETWEEN '1-1-1' AND NOW() AND id_produto = :id_produto AND excluido = '2'");
				$consulta1->bindValue(':id_produto',$id_produto);
				$consulta1->execute();




				$totalEntradas = $consulta1->fetch();


				// Obtém o valor total das saídas do produto
				$consulta2 = $pdo->prepare("SELECT id, SUM(qtd_produto) AS total_saidas FROM itemreqsaida WHERE data_baixa BETWEEN '1-1-1' AND NOW() AND id_produto = :id_produto AND situacao = '1' AND excluido = '2'");
				$consulta2->bindValue(':id_produto',$id_produto);
				$consulta2->execute();

				$totalSaidas = $consulta2->fetch();

				



				// Obtém o saldo atual
				$saldoAtual = $totalEntradas['total_entradas'] - $totalSaidas['total_saidas'];

				//echo $saldoAtual;
				//exit;
				
				/*
				print_r($totalEntradas)."<br>";
				print_r ($totalSaidas)."<br>";
				echo $saldoAtual."<br>";
				*/
				
				// Obtém o valor total das saídas do produto
				$consulta2 = $pdo->prepare("SELECT ponto_emer FROM produtos WHERE id = :id_produto");
				$consulta2->bindValue(':id_produto',$id_produto);
				$consulta2->execute();

				$resultado = $consulta2->fetch();
				$ponto_emergencia = $resultado['ponto_emer'];

				$saldoEmergencia = $saldoAtual - $ponto_emergencia;



				
				if($qtd_produto >  $saldoEmergencia){


					//echo "Chegou até aqui";
					//exit;


					$consulta3 = $pdo->prepare("SELECT id, nome FROM produtos WHERE id = :id");
					$consulta3->bindValue(":id", $id_produto );
					$consulta3->execute();

					$resultado = $consulta3->fetch();

					$retornoProdutos[$i]['id'] = $resultado['id'];
					$retornoProdutos[$i]['nome'] = $resultado['nome'];



					
					$i++;


					}// Fim do if

				}
				// Fim do foreach
					if(empty($retornoProdutos)){


						$retornoProdutos = "";
						return $retornoProdutos;

					}
					
					return $retornoProdutos;

			} 

			public function verificaIdEmUso($id_requisicao){

			global $pdo;

			$consulta3 = $pdo->prepare("SELECT id FROM requisicaosaida ORDER BY id DESC");
			$consulta3 = $pdo->bindValue(":id_requisicao",$id_requisicao );
			$consulta3->execute();

			$id_em_uso = $consulta3->fetch();
			return $id_em_uso['id'];

			}

		public function addSaidaMateriais($data_baixa, $id_departamento, $id_usuario, $produtos, $observacao, $id_requisicao = ""){



			global $pdo;
			$totalRequisicao = 0;



			if($id_requisicao == 2){


			/*
			echo $data_baixa;
			echo $id_departamento;
			echo $id_usuario;
			print_r($produtos);
			*/

			


			// Obtém o último número do id das resquisição de saída e acrescenta mais 1 para assim definir o número do id do item da requisição do produto( O item da requisição de produto é o próprio produto)
			$consulta3 = $pdo->query("SELECT id FROM requisicaosaida ORDER BY id DESC");
			$consulta3->execute();

			$id_ultimarequisicao = $consulta3->fetch();
			
			$id_req = $id_ultimarequisicao['id'];

			$id_req = $id_req + 1;


			foreach ($produtos as $produto) {


				
				$id_produto = $produto['id'];

				// Obtém a média do valor unitário do produto
				$consulta3 = $pdo->prepare("SELECT AVG(valor_unitario) AS valor_medio FROM itemreqentrada WHERE data_baixa BETWEEN '1-1-1' AND NOW() AND id_produto = :id_produto");
				$consulta3->bindValue(':id_produto',$id_produto);
				$consulta3->execute();
				$valor_unitario = $consulta3->fetch();

				
				$qtd_produto = $produto['qtd'];
				$custo_produto = $valor_unitario['valor_medio'];
				$totalProduto = $qtd_produto * $custo_produto;
				$totalRequisicao = $totalRequisicao + $totalProduto;




					/*
					echo "A quantidade do produto ".$qtd_produto."<br>";
					echo "O custo do produto ".$custo_produto."<br>";
					echo "O total do produto ".$totalProduto."<br>";
					echo "O total da requisição ".$totalRequisicao."<br>";
					*/

					
					/*

					echo $id_req."<br>";
					echo $id_produto."<br>";
					echo $qtd_produto."<br>";
					
					
					echo $custo_produto."<br>";
					echo $totalProduto."<br>";


					*/
					if(empty($custo_produto)){
						$custo_produto = 0;

					}
					
				


					// Após verificado o saldo é inserido os produtos da requisição
					$insercao1 = $pdo->prepare("INSERT INTO itemreqsaida SET id_req_saida = :id_req, data_baixa = :data_baixa, id_produto = :id_produto, qtd_produto = :qtd_produto, valor_unitario = :valor_unitario, valor_total = :valor_total, situacao = '2', excluido = '1'");

					$insercao1->bindValue(':id_req', $id_req);
					$insercao1->bindValue(':data_baixa', $data_baixa);
					$insercao1->bindValue(':id_produto', $id_produto);
					$insercao1->bindValue(':qtd_produto', $qtd_produto);
					$insercao1->bindValue(':valor_unitario', $custo_produto);
					$insercao1->bindValue(':valor_total', $totalProduto);
					$insercao1->execute();






				}// Fim do foreach 2




					// Após verificado o saldo é inserido o cabeçalho da requisição
					$insercao2 = $pdo->prepare("INSERT INTO requisicaosaida SET id_departamento = :id_departamento, id_usuario = :id_usuario, data_requisicao = NOW(), data_baixa = :data_baixa, valor_req_total = :valor_req_total, excluido = 1, situacao = 2, obs = :obs");

					$insercao2->bindValue(':id_departamento', $id_departamento);
					$insercao2->bindValue(':id_usuario', $id_usuario);
					$insercao2->bindValue(':valor_req_total', $totalRequisicao);
					$insercao2->bindValue(':data_baixa', $data_baixa);
					$insercao2->bindValue(':obs', $observacao);
					$insercao2->execute();


				return $id_req;



			}else{

			$totalRequisicao = 0;

			$exclusao1 = $pdo->prepare("DELETE FROM  itemreqsaida WHERE id_req_saida = :id_req");
			$exclusao1->bindValue(':id_req', $id_requisicao);
			$exclusao1->execute();

			//echo "Excluiu a requisição com o respectivo id";
			//exit;

			
			foreach ($produtos as $produto) {


				
				$id_produto = $produto['id'];

				// Obtém a média do valor unitário do produto
				$consulta3 = $pdo->prepare("SELECT AVG(valor_unitario) AS valor_medio FROM itemreqentrada WHERE data_baixa BETWEEN '1-1-1' AND NOW() AND id_produto = :id_produto");
				$consulta3->bindValue(':id_produto',$id_produto);
				$consulta3->execute();
				$valor_unitario = $consulta3->fetch();

				
				$qtd_produto = $produto['qtd'];
				$custo_produto = $valor_unitario['valor_medio'];
				$totalProduto = $qtd_produto * $custo_produto;
				$totalRequisicao = $totalRequisicao + $totalProduto;




					/*
					echo "A quantidade do produto ".$qtd_produto."<br>";
					echo "O custo do produto ".$custo_produto."<br>";
					echo "O total do produto ".$totalProduto."<br>";
					echo "O total da requisição ".$totalRequisicao."<br>";
					*/

					
					/*

					echo $id_req."<br>";
					echo $id_produto."<br>";
					echo $qtd_produto."<br>";
					
					
					echo $custo_produto."<br>";
					echo $totalProduto."<br>";


					*/

					if(empty($custo_produto)){
						$custo_produto = 0;

					}
					
				


					// Após verificado o saldo é inserido os produtos da requisição
					$insercao1 = $pdo->prepare("INSERT INTO itemreqsaida SET id_req_saida = :id_req, data_baixa = :data_baixa, id_produto = :id_produto, qtd_produto = :qtd_produto, valor_unitario = :valor_unitario, valor_total = :valor_total, situacao = '2', excluido = '1'");

					$insercao1->bindValue(':id_req', $id_requisicao);
					$insercao1->bindValue(':data_baixa', $data_baixa);
					$insercao1->bindValue(':id_produto', $id_produto);
					$insercao1->bindValue(':qtd_produto', $qtd_produto);
					$insercao1->bindValue(':valor_unitario', $custo_produto);
					$insercao1->bindValue(':valor_total', $totalProduto);
					$insercao1->execute();






				}// Fim do foreach 2




					// Após verificado o saldo é inserido o cabeçalho da requisição
					$insercao2 = $pdo->prepare("UPDATE requisicaosaida SET id_departamento = :id_departamento, id_usuario = :id_usuario, data_requisicao = NOW(), data_baixa = :data_baixa, valor_req_total = :valor_req_total, excluido = 1, situacao = 2 WHERE id = :id_req");

					$insercao2->bindValue(':id_req', $id_requisicao);
					$insercao2->bindValue(':id_departamento', $id_departamento);
					$insercao2->bindValue(':id_usuario', $id_usuario);
					$insercao2->bindValue(':valor_req_total', $totalRequisicao);
					$insercao2->bindValue(':data_baixa', $data_baixa);
					$insercao2->execute();


				return $id_requisicao;

			}
		}


		public function rel_inventario($data_inicial, $data_final, $zerados = ''){



			global $pdo;
			// Caso seja setado como um 1 signfica que usuário quer ver os materiais zerados e por isso ele executa esse código
			if($zerados == 1){



			// Obtém o valor total das entradas do produto
			$consulta1 = $pdo->query("SELECT id, nome FROM produtos WHERE ativo = '1'");
			$consulta1->execute();

			$produtos = $consulta1->fetchAll();
			$i = 0;
			foreach($produtos as $produto){


				
				$id_produto = $produto['id']."<br>";
				$nome_produto = $produto['nome']."<br>";

				$consulta[$i]['id_produto'] = $id_produto;
				$consulta[$i]['nome_produto'] = $nome_produto;

				// Obtém o valor de todas as entradas
				$consulta1 = $pdo->prepare("SELECT SUM(qtd_produto) AS total_entradas FROM itemreqentrada WHERE data_baixa BETWEEN :data_inicial AND :data_final AND id_produto = :id_produto AND excluido = '2'");

				
				/*

				Funciona 

				SELECT SUM(qtd_produto) AS total_entradas FROM itemreqentrada WHERE data_baixa BETWEEN '2019-01-01' AND '2019-12-31' AND id_produto = '532' AND excluido = '2';

				"SELECT SUM(qtd_produto) AS total_entradas FROM itemreqentrada WHERE data_baixa BETWEEN :data_inicial AND :data_final AND id_produto = :id_produto AND excluido = '2'

				*/
				$consulta1->bindValue(':id_produto',$id_produto);
				$consulta1->bindValue(':data_inicial',$data_inicial);
				$consulta1->bindValue(':data_final',$data_final);
				$consulta1->execute();

				
				$totalEntradas = $consulta1->fetch();



				// Obtém o valor total das saídas do produto
				$consulta2 = $pdo->prepare("SELECT SUM(qtd_produto) AS total_saidas FROM itemreqsaida WHERE data_baixa BETWEEN :data_inicial AND :data_final AND id_produto = :id_produto AND situacao = '1' AND excluido = '2'");


				$consulta2->bindValue(':id_produto',$id_produto);
				$consulta2->bindValue(':data_inicial',$data_inicial);
				$consulta2->bindValue(':data_final',$data_final);


				$consulta2->execute();


				// Apresenta  o total de saidas
				$totalSaidas = $consulta2->fetch();
				//echo $produto['id']."-";
				//echo $totalSaidas['total_saidas']."<br>";





				 $saldoAtual = $totalEntradas['total_entradas'] - $totalSaidas['total_saidas'];



				 $consulta[$i]['saldo_fisico'] = $saldoAtual;

 				// Obtém a média do valor unitário do produto
				$consulta3 = $pdo->prepare("SELECT AVG(valor_unitario) AS valor_medio FROM itemreqentrada WHERE data_baixa BETWEEN :data_inicial AND :data_final AND id_produto = :id_produto AND excluido = '2'");

				$consulta3->bindValue(':id_produto',$id_produto);
				$consulta3->bindValue(':data_inicial',$data_inicial);
				$consulta3->bindValue(':data_final',$data_final);
				$consulta3->execute();

				$valor_unitario = $consulta3->fetch();

				/*

				$consulta3 = $pdo->prepare("SELECT AVG(valor_unitario) AS valor_medio FROM itemreqentrada WHERE data_baixa BETWEEN '1-1-1' AND NOW() AND id_produto = :id_produto");
				$consulta3->bindValue(':id_produto',$id_produto);
				$consulta3->execute();

				$valor_unitario = $consulta3->fetch();
				*/

				// Calcula o saldo financeiro do produto
				$saldo_financeiro = $saldoAtual * $valor_unitario['valor_medio'];

				$consulta[$i]['saldo_financeiro'] = $saldo_financeiro;

				/*
				// Calcula o saldo físico do produto
				$saldoTotal = $totalEntradas['total_entradas'] * $valor_unitario['valor_medio'];
				/*

				
				



				/*
				

				echo "O saldo total é: ".$saldoTotal."<br>";
				echo "O nome do produto é: ".$nome_p"<br>"
				echo "A quantidade total de entradas do produto é: ".$totalEntradas['total_entradas']."<br>";
				echo "A quantiadade total atual do produto é: ".$saldoAtual."<br>";
				echo "O saldo médio unitário é: ".$valor_unitario['valor_medio']."<br>";

				
				echo "O saldo financeiro atual é ".$saldo_financeiro;
			    */

				$i++;

			}
			
			return $consulta;



			}else{


			// Obtém o valor total das entradas do produto
			$consulta1 = $pdo->query("SELECT id, nome FROM produtos WHERE ativo = '1'");
			$consulta1->execute();

			$produtos = $consulta1->fetchAll();
			$i = 0;
			foreach($produtos as $produto){


				
				$id_produto = $produto['id']."<br>";
				$nome_produto = $produto['nome']."<br>";

				// Obtém todas as entradas do produto
				$consulta1 = $pdo->prepare("SELECT SUM(qtd_produto) AS total_entradas FROM itemreqentrada WHERE data_baixa BETWEEN :data_inicial AND :data_final AND id_produto = :id_produto AND excluido = '2'");

				$consulta1->bindValue(':id_produto',$id_produto);
				$consulta1->bindValue(':data_inicial',$data_inicial);
				$consulta1->bindValue(':data_final',$data_final);
				$consulta1->execute();

				// Apresenta o total de entradas do prduto
				$totalEntradas = $consulta1->fetch();



				// Obtém o valor total das saídas do produto
				$consulta2 = $pdo->prepare("SELECT SUM(qtd_produto) AS total_saidas FROM itemreqsaida WHERE data_baixa BETWEEN :data_inicial AND :data_final AND id_produto = :id_produto AND situacao = '1' AND excluido = '2'");


				$consulta2->bindValue(':id_produto',$id_produto);
				$consulta2->bindValue(':data_inicial',$data_inicial);
				$consulta2->bindValue(':data_final',$data_final);


				$consulta2->execute();


				// Apresenta  o total de saidas
				$totalSaidas = $consulta2->fetch();
				//echo $produto['id']."-";
				//echo $totalSaidas['total_saidas']."<br>";





				 $saldoAtual = $totalEntradas['total_entradas'] - $totalSaidas['total_saidas'];


 				// Obtém a média do valor unitário do produto
				$consulta3 = $pdo->prepare("SELECT AVG(valor_unitario) AS valor_medio FROM itemreqentrada WHERE data_baixa BETWEEN :data_inicial AND :data_final AND id_produto = :id_produto AND excluido = '2'");

				$consulta3->bindValue(':id_produto',$id_produto);
				$consulta3->bindValue(':data_inicial',$data_inicial);
				$consulta3->bindValue(':data_final',$data_final);
				$consulta3->execute();

				$valor_unitario = $consulta3->fetch();

				// Calcula o saldo financeiro do produto
				$saldo_financeiro = $saldoAtual * $valor_unitario['valor_medio'];



				// Calcula o saldo físico do produto
				$saldoTotal = $totalEntradas['total_entradas'] * $valor_unitario['valor_medio'];

			
				/*
				

				echo "O saldo total é: ".$saldoTotal."<br>";
				echo "O nome do produto é: ".$nome_p"<br>"
				echo "A quantidade total de entradas do produto é: ".$totalEntradas['total_entradas']."<br>";
				echo "A quantiadade total atual do produto é: ".$saldoAtual."<br>";
				echo "O saldo médio unitário é: ".$valor_unitario['valor_medio']."<br>";

				
				echo "O saldo financeiro atual é ".$saldo_financeiro;
			    */

			    if($saldoAtual > 0){

				$consulta[$i]['id_produto'] = $id_produto;
				$consulta[$i]['nome_produto'] = $nome_produto;
			    $consulta[$i]['saldo_fisico'] = $saldoAtual;
 				$consulta[$i]['saldo_financeiro'] = $saldo_financeiro;

 				$i++;

 			  }

			}// Fim do foreach
			
			return $consulta;

		
			}// Fim do else

		}//Fim da função 

		public function rel_saidas_setor($data_inicial, $data_final, $id_departamento){



			global $pdo;




			// Obtém o valor total das entradas do produto
			$consulta1 = $pdo->prepare("SELECT id AS id_req_saida FROM requisicaosaida WHERE id_departamento = :id_departamento AND data_baixa BETWEEN :data_inicial AND :data_final AND situacao = '1' AND excluido = '2'");
			$consulta1->bindValue(':id_departamento',$id_departamento);
			$consulta1->bindValue(':data_inicial',$data_inicial);
			$consulta1->bindValue(':data_final',$data_final);
			$consulta1->execute();

			$ids_req_saida = $consulta1->fetchAll();


			foreach($ids_req_saida as $id_req_saida){

	

				$id_req_saida = $id_req_saida['id_req_saida'];
				



							// Obtém o valor total das entradas do produto
			$consulta1 = $pdo->prepare("
				SELECT 
				irs.id_produto,
				p.nome as nome_produto,
			    irs.qtd_produto, 
				irs.valor_total
				FROM
				itemreqsaida irs
				INNER JOIN produtos p ON (p.id = irs.id_produto)
				WHERE id_req_saida = :id_req_saida AND data_baixa BETWEEN :data_inicial AND :data_final AND excluido = '2' AND situacao = '1' ORDER BY data_baixa DESC");

			$consulta1->bindValue(':id_req_saida',$id_req_saida);
			$consulta1->bindValue(':data_inicial',$data_inicial);
			$consulta1->bindValue(':data_final',$data_final);
			$consulta1->execute();

			$consulta1 = $consulta1->fetchAll();

			}


			
			return $consulta1;

		
			

		}//Fim da função 

		public function rel_entrada_fornecedor($data_inicial, $data_final, $id_fornecedor, $zerados = ''){


			global $pdo;
			// Caso seja setado como um 1 signfica que usuário quer ver os materiais zerados e por isso ele executa esse código
			if($zerados == 1){



			// Obtém o valor total das entradas do produto
			$consulta1 = $pdo->query("SELECT id, nome FROM produtos WHERE ativo = '1'");
			$consulta1->execute();

			$produtos = $consulta1->fetchAll();
			$i = 0;
			foreach($produtos as $produto){


				
				$id_produto = $produto['id']."<br>";
				$nome_produto = $produto['nome']."<br>";

				$consulta[$i]['id_produto'] = $id_produto;
				$consulta[$i]['nome_produto'] = $nome_produto;

				// Obtém o valor de todas as entradas
				$consulta1 = $pdo->prepare("SELECT SUM(qtd_produto) AS total_entradas FROM itemreqentrada WHERE data_baixa BETWEEN :data_inicial AND :data_final AND id_produto = :id_produto AND excluido = '2'");

				
				/*

				Funciona 

				SELECT SUM(qtd_produto) AS total_entradas FROM itemreqentrada WHERE data_baixa BETWEEN '2019-01-01' AND '2019-12-31' AND id_produto = '532' AND excluido = '2';

				"SELECT SUM(qtd_produto) AS total_entradas FROM itemreqentrada WHERE data_baixa BETWEEN :data_inicial AND :data_final AND id_produto = :id_produto AND excluido = '2'

				*/
				$consulta1->bindValue(':id_produto',$id_produto);
				$consulta1->bindValue(':data_inicial',$data_inicial);
				$consulta1->bindValue(':data_final',$data_final);
				$consulta1->execute();

				
				$totalEntradas = $consulta1->fetch();



				$consulta[$i]['total_entradas'] = $totalEntradas['total_entradas'];

				/*
				// Calcula o saldo físico do produto
				$saldoTotal = $totalEntradas['total_entradas'] * $valor_unitario['valor_medio'];
				/*

				
				



				/*
				

				echo "O saldo total é: ".$saldoTotal."<br>";
				echo "O nome do produto é: ".$nome_p"<br>"
				echo "A quantidade total de entradas do produto é: ".$totalEntradas['total_entradas']."<br>";
				echo "A quantiadade total atual do produto é: ".$saldoAtual."<br>";
				echo "O saldo médio unitário é: ".$valor_unitario['valor_medio']."<br>";

				
				echo "O saldo financeiro atual é ".$saldo_financeiro;
			    */

				$i++;

			}
			if(empty($consulta)){

				$consulta = 2;
				return $consulta;
				exit;

			}
			
			return $consulta;



			}else{


			// Obtém o valor total das entradas do produto
			$consulta1 = $pdo->query("SELECT id, nome FROM produtos WHERE ativo = '1'");
			$consulta1->execute();

			$produtos = $consulta1->fetchAll();
			$i = 0;
			foreach($produtos as $produto){


				
				$id_produto = $produto['id']."<br>";
				$nome_produto = $produto['nome']."<br>";

				// Obtém todas as entradas do produto
				$consulta1 = $pdo->prepare("SELECT SUM(qtd_produto) AS total_entradas FROM itemreqentrada WHERE data_baixa BETWEEN :data_inicial AND :data_final AND id_produto = :id_produto AND excluido = '2'");

				$consulta1->bindValue(':id_produto',$id_produto);
				$consulta1->bindValue(':data_inicial',$data_inicial);
				$consulta1->bindValue(':data_final',$data_final);
				$consulta1->execute();

				// Apresenta o total de entradas do prduto
				$totalEntradas = $consulta1->fetch();



				// Obtém o valor total das saídas do produto
				$consulta2 = $pdo->prepare("SELECT SUM(qtd_produto) AS total_saidas FROM itemreqsaida WHERE data_baixa BETWEEN :data_inicial AND :data_final AND id_produto = :id_produto AND situacao = '1' AND excluido = '2'");


				$consulta2->bindValue(':id_produto',$id_produto);
				$consulta2->bindValue(':data_inicial',$data_inicial);
				$consulta2->bindValue(':data_final',$data_final);


				$consulta2->execute();


				// Apresenta  o total de saidas
				$totalSaidas = $consulta2->fetch();
				//echo $produto['id']."-";
				//echo $totalSaidas['total_saidas']."<br>";





				 $saldoAtual = $totalEntradas['total_entradas'] - $totalSaidas['total_saidas'];


 				// Obtém a média do valor unitário do produto
				$consulta3 = $pdo->prepare("SELECT AVG(valor_unitario) AS valor_medio FROM itemreqentrada WHERE data_baixa BETWEEN :data_inicial AND :data_final AND id_produto = :id_produto AND excluido = '2'");

				$consulta3->bindValue(':id_produto',$id_produto);
				$consulta3->bindValue(':data_inicial',$data_inicial);
				$consulta3->bindValue(':data_final',$data_final);
				$consulta3->execute();

				$valor_unitario = $consulta3->fetch();

				// Calcula o saldo financeiro do produto
				$saldo_financeiro = $saldoAtual * $valor_unitario['valor_medio'];



				// Calcula o saldo físico do produto
				$saldoTotal = $totalEntradas['total_entradas'] * $valor_unitario['valor_medio'];

			
				/*
				

				echo "O saldo total é: ".$saldoTotal."<br>";
				echo "O nome do produto é: ".$nome_p"<br>"
				echo "A quantidade total de entradas do produto é: ".$totalEntradas['total_entradas']."<br>";
				echo "A quantiadade total atual do produto é: ".$saldoAtual."<br>";
				echo "O saldo médio unitário é: ".$valor_unitario['valor_medio']."<br>";

				
				echo "O saldo financeiro atual é ".$saldo_financeiro;
			    */

			    if($saldoAtual > 0){

				$consulta[$i]['id_produto'] = $id_produto;
				$consulta[$i]['nome_produto'] = $nome_produto;
			    $consulta[$i]['total_entradas'] = $totalEntradas['total_entradas'];
 				$consulta[$i]['saldo_financeiro'] = $saldo_financeiro;

 				$i++;

 			  }

			}// Fim do foreach
			if(empty($consulta)){

				$consulta = 2;

				return $consulta;
				exit;

			}
			return $consulta;

		
			}// Fim do else

		}//Fim da função 



		public function rel_total_entradas_saidas($data_inicial, $data_final, $zerados = ''){



			global $pdo;
			// Caso seja setado como um 1 signfica que usuário quer ver os materiais zerados e por isso ele executa esse código
			if($zerados == 1){



			// Obtém o valor total das entradas do produto
			$consulta1 = $pdo->query("SELECT id, nome FROM produtos WHERE ativo = '1'");
			$consulta1->execute();

			$produtos = $consulta1->fetchAll();
			$i = 0;
			foreach($produtos as $produto){


				
				$id_produto = $produto['id']."<br>";
				$nome_produto = $produto['nome']."<br>";

				$consulta[$i]['id_produto'] = $id_produto;
				$consulta[$i]['nome_produto'] = $nome_produto;

				// Obtém o valor de todas as entradas
				$consulta1 = $pdo->prepare("SELECT SUM(qtd_produto) AS total_entradas FROM itemreqentrada WHERE data_baixa BETWEEN :data_inicial AND :data_final AND id_produto = :id_produto AND excluido = '2'");

				
				/*

				Funciona 

				SELECT SUM(qtd_produto) AS total_entradas FROM itemreqentrada WHERE data_baixa BETWEEN '2019-01-01' AND '2019-12-31' AND id_produto = '532' AND excluido = '2';

				"SELECT SUM(qtd_produto) AS total_entradas FROM itemreqentrada WHERE data_baixa BETWEEN :data_inicial AND :data_final AND id_produto = :id_produto AND excluido = '2'

				*/
				$consulta1->bindValue(':id_produto',$id_produto);
				$consulta1->bindValue(':data_inicial',$data_inicial);
				$consulta1->bindValue(':data_final',$data_final);
				$consulta1->execute();

				
				$totalEntradas = $consulta1->fetch();



				// Obtém o valor total das saídas do produto
				$consulta2 = $pdo->prepare("SELECT SUM(qtd_produto) AS total_saidas FROM itemreqsaida WHERE data_baixa BETWEEN :data_inicial AND :data_final AND id_produto = :id_produto AND situacao = '1' AND excluido = '2'");


				$consulta2->bindValue(':id_produto',$id_produto);
				$consulta2->bindValue(':data_inicial',$data_inicial);
				$consulta2->bindValue(':data_final',$data_final);


				$consulta2->execute();


				// Apresenta  o total de saidas
				$totalSaidas = $consulta2->fetch();
				//echo $produto['id']."-";
				//echo $totalSaidas['total_saidas']."<br>";





				 $saldoAtual = $totalEntradas['total_entradas'] - $totalSaidas['total_saidas'];

				 if(empty($totalEntradas['total_entradas'])){

				 	$totalEntradas['total_entradas'] = 0;


				 }

				 if(empty($totalSaidas['total_saidas'])){

				 	$totalSaidas['total_saidas'] = 0;

				 }


				 if(empty($saldoAtual)){

				 	$saldoAtual = 0;

				 }



				 $consulta[$i]['saldo_fisico'] = $saldoAtual;
				 $consulta[$i]['total_entradas'] = $totalEntradas['total_entradas'];
				 $consulta[$i]['total_saidas'] =  $totalSaidas['total_saidas'];

			

				// Calcula o saldo financeiro do produto
			


				/*
				// Calcula o saldo físico do produto
				$saldoTotal = $totalEntradas['total_entradas'] * $valor_unitario['valor_medio'];
				/*

				
				



				/*
				

				echo "O saldo total é: ".$saldoTotal."<br>";
				echo "O nome do produto é: ".$nome_p"<br>"
				echo "A quantidade total de entradas do produto é: ".$totalEntradas['total_entradas']."<br>";
				echo "A quantiadade total atual do produto é: ".$saldoAtual."<br>";
				echo "O saldo médio unitário é: ".$valor_unitario['valor_medio']."<br>";

				
				echo "O saldo financeiro atual é ".$saldo_financeiro;
			    */

				$i++;

			}
			
			return $consulta;



			}else{


			// Obtém o valor total das entradas do produto
			$consulta1 = $pdo->query("SELECT id, nome FROM produtos WHERE ativo = '1'");
			$consulta1->execute();

			$produtos = $consulta1->fetchAll();
			$i = 0;
			foreach($produtos as $produto){


				
				$id_produto = $produto['id']."<br>";
				$nome_produto = $produto['nome']."<br>";

				// Obtém todas as entradas do produto
				$consulta1 = $pdo->prepare("SELECT SUM(qtd_produto) AS total_entradas FROM itemreqentrada WHERE data_baixa BETWEEN :data_inicial AND :data_final AND id_produto = :id_produto AND excluido = '2'");

				$consulta1->bindValue(':id_produto',$id_produto);
				$consulta1->bindValue(':data_inicial',$data_inicial);
				$consulta1->bindValue(':data_final',$data_final);
				$consulta1->execute();

				// Apresenta o total de entradas do prduto
				$totalEntradas = $consulta1->fetch();



				// Obtém o valor total das saídas do produto
				$consulta2 = $pdo->prepare("SELECT SUM(qtd_produto) AS total_saidas FROM itemreqsaida WHERE data_baixa BETWEEN :data_inicial AND :data_final AND id_produto = :id_produto AND situacao = '1' AND excluido = '2'");


				$consulta2->bindValue(':id_produto',$id_produto);
				$consulta2->bindValue(':data_inicial',$data_inicial);
				$consulta2->bindValue(':data_final',$data_final);


				$consulta2->execute();


				// Apresenta  o total de saidas
				$totalSaidas = $consulta2->fetch();
				//echo $produto['id']."-";
				//echo $totalSaidas['total_saidas']."<br>";





				 $saldoAtual = $totalEntradas['total_entradas'] - $totalSaidas['total_saidas'];




				 if(empty($totalEntradas['total_entradas'])){

				 	$totalEntradas['total_entradas'] = 0;


				 }

				 if(empty($totalSaidas['total_saidas'])){

				 	$totalSaidas['total_saidas'] = 0;

				 }

				 
				 if(empty($saldoAtual)){

				 	$saldoAtual = 0;

				 }






			    if($saldoAtual > 0){

				$consulta[$i]['id_produto'] = $id_produto;
				$consulta[$i]['nome_produto'] = $nome_produto;
				$consulta[$i]['total_entradas'] = $totalEntradas['total_entradas'];
				$consulta[$i]['total_saidas'] =  $totalSaidas['total_saidas'];
				$consulta[$i]['saldo_fisico'] = $saldoAtual;
				
				

 				$i++;

 			  }

			}// Fim do foreach
			
			return $consulta;

		
			}// Fim do else

		}//Fim da função 


		public function rel_ponto_pedido(){

			global $pdo;

			// Obtém o valor total das entradas do produto
			$consulta1 = $pdo->query("SELECT id, nome FROM produtos WHERE ativo = '1'");
			$consulta1->execute();

			$produtos = $consulta1->fetchAll();
			$i = 0;
			foreach($produtos as $produto){


				
				$id_produto = $produto['id']."<br>";
				$nome_produto = $produto['nome']."<br>";

				// Obtém todas as entradas do produto
				$consulta1 = $pdo->prepare("SELECT SUM(qtd_produto) AS total_entradas FROM itemreqentrada WHERE data_baixa BETWEEN '1-1-1' AND NOW() AND id_produto = :id_produto AND excluido = '2'");

				$consulta1->bindValue(':id_produto',$id_produto);
				$consulta1->execute();

				// Apresenta o total de entradas do prduto
				$totalEntradas = $consulta1->fetch();



				// Obtém o valor total das saídas do produto
				$consulta2 = $pdo->prepare("SELECT SUM(qtd_produto) AS total_saidas FROM itemreqsaida WHERE data_baixa BETWEEN '1-1-1' AND NOW() AND id_produto = :id_produto AND situacao = '1' AND excluido = '2'");
				$consulta2->bindValue(':id_produto',$id_produto);
				$consulta2->execute();


				// Apresenta  o total de saidas
				$totalSaidas = $consulta2->fetch();
				//echo $produto['id']."-";
				//echo $totalSaidas['total_saidas']."<br>";





				 $saldoAtual = $totalEntradas['total_entradas'] - $totalSaidas['total_saidas'];


				/*
				

				echo "O saldo total é: ".$saldoTotal."<br>";
				echo "O nome do produto é: ".$nome_p"<br>"
				echo "A quantidade total de entradas do produto é: ".$totalEntradas['total_entradas']."<br>";
				echo "A quantiadade total atual do produto é: ".$saldoAtual."<br>";
				echo "O saldo médio unitário é: ".$valor_unitario['valor_medio']."<br>";

				
				echo "O saldo financeiro atual é ".$saldo_financeiro;
			    */

			   	// Obtém o valor total das saídas do produto
				$consulta2 = $pdo->prepare("SELECT ponto_pedido FROM produtos WHERE id = :id_produto");
				$consulta2->bindValue(':id_produto',$id_produto);
				$consulta2->execute();

				$resultado = $consulta2->fetch();
				$ponto_pedido = $resultado['ponto_pedido'];	


				if(!empty($ponto_pedido)){


				if($totalSaidas['total_saidas'] == 0){

					$percentual = "100";


				}else{

					$percentual = ((100 / $totalEntradas['total_entradas']) * (($totalSaidas['total_saidas'] - $ponto_pedido )));

					$percentual = abs($percentual); 

				}


				
				
			    $diferenca = $saldoAtual - $ponto_pedido;
				$consulta[$i]['id_produto'] = $id_produto;
				$consulta[$i]['nome_produto'] = $nome_produto;
			    $consulta[$i]['saldo_fisico'] = $saldoAtual;
 				$consulta[$i]['ponto_pedido'] = $ponto_pedido;
 				$consulta[$i]['diferenca'] = $diferenca;
 				$consulta[$i]['percentual'] = $percentual;


				}



 				$i++;



			}// Fim do foreach

			return $consulta;

		
			}// Fim do else



		public function editarSaidaMateriais($data_requisicao, $id_departamento, $id_usuario, $produtos, $observacao, $id_requisicao){

			global $pdo;
			$totalRequisicao = 0;


			foreach($produtos as $produto){



				$id_produto = $produto['id'];




				// Consulta o valor total das entradas do produto
				$consulta1 = $pdo->prepare("SELECT SUM(qtd_produto) AS total_entradas FROM itemreqentrada WHERE data_baixa BETWEEN '1-1-1' AND NOW() AND id_produto = :id_produto");
				$consulta1->bindValue(':id_produto',$id_produto);
				$consulta1->execute();




				$totalEntradas = $consulta1->fetch();


				// Consulta o valor total das saídas do produto
				$consulta2 = $pdo->prepare("SELECT id, SUM(qtd_produto) AS total_saidas FROM itemreqsaida WHERE data_baixa BETWEEN '1-1-1' AND NOW() AND id_produto = :id_produto AND situacao = '1' AND excluido = '2'");
				$consulta2->bindValue(':id_produto',$id_produto);
				$consulta2->execute();

				$totalSaidas = $consulta2->fetch();


				// Obtém o saldo atual
				$saldoAtual = $totalEntradas['total_entradas'] - $totalSaidas['total_saidas'];



				// Obtém a média do valor unitário do produto
				$consulta3 = $pdo->prepare("SELECT AVG(valor_unitario) AS valor_medio FROM itemreqentrada WHERE data_baixa BETWEEN '1-1-1' AND NOW() AND id_produto = :id_produto");
				$consulta3->bindValue(':id_produto',$id_produto);
				$consulta3->execute();

				$valor_unitario = $consulta3->fetch();




				$qtd_produto = $produto['qtd'];
				$custo_produto = $valor_unitario['valor_medio'];
				$totalProduto = $qtd_produto * $custo_produto;
				$totalRequisicao = $totalRequisicao + $totalProduto;


				/*
				echo $totalProduto."<br>";
				echo $qtd_produto."<br>";
				echo $custo_produto."<br>";
				*/
				



				if($qtd_produto > $saldoAtual){


					$consulta3 = $pdo->prepare("SELECT id, nome FROM produtos WHERE id = :id");
					$consulta3->bindValue(":id", $id_produto );
					$consulta3->execute();

					$retornoConsulta = $consulta3->fetch();

					return $retornoConsulta;


					}// Fim do if


				}// Fim do foreach

				// Foreach acima verifica se há saldo em todos os produtos 
				// Consulta o valor total das entradas do produto
				
				$consulta0 = $pdo->prepare("DELETE FROM itemreqsaida WHERE id_req_saida = :id_req_saida");
				$consulta0->bindValue(':id_req_saida',$id_requisicao);
				$consulta0->execute();


				foreach ($produtos as $produto) {

			
				$id_produto = $produto['id'];

				// Obtém a média do valor unitário do produto
				$consulta3 = $pdo->prepare("SELECT AVG(valor_unitario) AS valor_medio FROM itemreqentrada WHERE data_baixa BETWEEN '1-1-1' AND NOW() AND id_produto = :id_produto");
				$consulta3->bindValue(':id_produto',$id_produto);
				$consulta3->execute();
				$valor_unitario = $consulta3->fetch();

				$qtd_produto = $produto['qtd'];
				$custo_produto = $valor_unitario['valor_medio'];
				$totalProduto = $qtd_produto * $custo_produto;
				$totalRequisicao = $totalRequisicao + $totalProduto;


					/*
					echo "A quantidade do produto ".$qtd_produto."<br>";
					echo "O custo do produto ".$custo_produto."<br>";
					echo "O total do produto ".$totalProduto."<br>";
					echo "O total da requisição ".$totalRequisicao."<br>";
					*/

				
				


					// Após verificado o saldo é inserido os produtos da requisição
					$insercao1 = $pdo->prepare("INSERT INTO itemreqsaida SET id_req_saida = :id_req_saida, id_produto = :id_produto, qtd_produto = :qtd_produto, valor_unitario = :valor_unitario, valor_total = :valor_total");


					$insercao1->bindValue(':id_produto', $id_produto);
					$insercao1->bindValue(':qtd_produto', $qtd_produto);
					$insercao1->bindValue(':valor_unitario', $custo_produto);
					$insercao1->bindValue(':valor_total', $totalProduto);
					$insercao1->bindValue(':id_req_saida', $id_requisicao);
					$insercao1->execute();


				}// Fim do foreach 2

					// Após verificado o saldo é inserido os produtos da requisição
					$insercao2 = $pdo->prepare("UPDATE  requisicaosaida SET id_departamento = :id_departamento, id_usuario = :id_usuario, data_requisicao = :data_requisicao, valor_req_total = :valor_req_total, obs = :obs, excluido = 2, situacao = 2 WHERE  id = :id_req");


					$insercao2->bindValue(':id_departamento', $id_departamento);
					$insercao2->bindValue(':id_usuario', $id_usuario);
					$insercao2->bindValue(':data_requisicao', $data_requisicao);
					$insercao2->bindValue(':valor_req_total', $totalRequisicao);
					$insercao2->bindValue(':obs', $observacao);
					$insercao2->bindValue(':id_req', $id_requisicao);
					$insercao2->execute();

					return 1;

			} 

			public function faturarRequisicao($id_requisicao){

				global $pdo;



				//$data_nova = date('Y/m/d', strtotime($data_inicial));

				$insercao1 = $pdo->prepare("UPDATE requisicaosaida SET situacao = 1 WHERE  id = :id");
				$insercao1->bindValue(':id', $id_requisicao);
				$insercao1->execute();	

				$insercao2 = $pdo->prepare("UPDATE itemreqsaida SET situacao = 1 WHERE  id_req_saida = :id_req");
				$insercao2->bindValue(':id_req', $id_requisicao);
				$insercao2->execute();	

				return 1;			
						
			}

			public function excluirSaidaMateriais($id_requisicao){

				global $pdo;



				//$data_nova = date('Y/m/d', strtotime($data_inicial));

				$insercao1 = $pdo->prepare("UPDATE requisicaosaida SET excluido = 1 WHERE  id = :id");
				$insercao1->bindValue(':id', $id_requisicao);
				$insercao1->execute();	

				$insercao2 = $pdo->prepare("UPDATE itemreqsaida SET excluido = 1 WHERE  id_req_saida = :id_req");
				$insercao2->bindValue(':id_req', $id_requisicao);
				$insercao2->execute();				
						
					}

			public function efetivarSaidaMateriais($id_requisicao){

				global $pdo;



				//$data_nova = date('Y/m/d', strtotime($data_inicial));

				$insercao1 = $pdo->prepare("UPDATE requisicaosaida SET excluido = 2 WHERE  id = :id");
				$insercao1->bindValue(':id', $id_requisicao);
				$insercao1->execute();	

				$insercao2 = $pdo->prepare("UPDATE itemreqsaida SET excluido = 2 WHERE  id_req_saida = :id_req");
				$insercao2->bindValue(':id_req', $id_requisicao);
				$insercao2->execute();				
						
			}

			public function excluirEntradaMateriais($id_requisicao){

				global $pdo;



				//$data_nova = date('Y/m/d', strtotime($data_inicial));

				$insercao1 = $pdo->prepare("UPDATE requisicaoentrada SET excluido = 1 WHERE  id = :id");
				$insercao1->bindValue(':id', $id_requisicao);
				$insercao1->execute();	

				$insercao2 = $pdo->prepare("UPDATE itemreqentrada SET excluido = 1 WHERE  id_req_entrada = :id_req");
				$insercao2->bindValue(':id_req', $id_requisicao);
				$insercao2->execute();				
						
			}

			public function updateSaldoProdutos(){

				global $pdo;

				$produtos = array();

				$sql = $pdo->query("SELECT * FROM produtos");
				$produtos = $sql->fetchAll();

				foreach($produtos as $produto){

					$id_produto = $produto['id'];


					



				$consulta1 = $pdo->prepare("INSERT INTO itemreqentrada SET id_req_entrada = '1000', data_baixa = '2020-03-21', id_fornecedor = '1000', id_produto = :id_produto, qtd_produto = '1000',  valor_unitario = '1', valor_total = '1', excluido = '2'");





				$consulta1->bindValue(':id_produto',$id_produto);
				$consulta1->execute();

				

				
				
				}
				$mensagem = "Produtos cadastrados com sucesso";
				return $mensagem;

				

			}



	}










?>