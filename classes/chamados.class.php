<?php 
require_once "config.php";
require_once "classes/chamados.class.php";

class Chamados{


	public function getChamados($id_usuario){


		global $pdo;

		$array = array();
		$sql = $pdo->prepare("
			SELECT
		 	c.id,
		 	c.data_abertura,
		 	c.data_fechamento,
		 	u.id as id_usuario,
		 	u.nome as nome_usuario,
		 	c.tipo_chamado,
		 	c.situacao,
		 	c.problema
		 	FROM 
		 	chamados c
		 	INNER JOIN usuarios u ON (u.id = c.id_usuario) WHERE id_usuario = :id_usuario ");
		$sql->bindValue(':id_usuario', $id_usuario);
		$sql->execute();

		$array = $sql->fetchAll();


		return $array; 

	}
	public function getChamado($id_chamado){


	global $pdo;

	$array = array();
	$sql = $pdo->prepare("
		SELECT
	 	c.id,
	 	c.data_abertura,
	 	c.data_fechamento,
	 	u.id as id_usuario,
	 	u.nome as nome_usuario,
	 	c.tipo_chamado,
	 	c.situacao,
	 	c.resposta,
	 	c.chamado,
	 	c.resolucao
	 	FROM 
	 	chamados c
	 	INNER JOIN usuarios u ON (u.id = c.id_usuario) WHERE c.id = :id_chamado ");
	$sql->bindValue(':id_chamado', $id_chamado);
	$sql->execute();

	$array = $sql->fetch();


	return $array; 

}

	public function addChamado(){
		global $pdo;

		$sql = $pdo->prepapre("INSERT INTO chamados SET data_abertura = NOW(), ");


	}

}





?>