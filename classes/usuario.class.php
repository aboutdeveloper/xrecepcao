<?php

class Usuarios {

	private $nome;



	public function cadastrar($nome, $cpf,  $email, $senha, $tipo, $ativo){

		global $pdo;

		$sql = $pdo->prepare("INSERT INTO usuarios SET nome = :nome, cpf = :cpf, email = :email, senha = :senha, tipo = :tipo, ativo = :ativo");
		$sql->bindValue(':nome',$nome);
		$sql->bindValue(':cpf',$cpf);
		$sql->bindValue(':email',$email);
		$sql->bindValue(":senha", md5($senha));
		$sql->bindValue(":tipo",$tipo);
		$sql->bindValue(":ativo",$ativo);
		$sql->execute();

		return true;

	}

	public function editarUsuario($nome, $cpf, $email, $tipo, $ativo, $id, $senha = ' '){
		global $pdo; 

		$sql = $pdo->prepare("UPDATE usuarios SET nome = :nome, cpf = :cpf, email = :email, tipo = :tipo, ativo = :ativo, senha = :senha WHERE id = :id");

		$sql->bindValue(':nome',$nome);
		$sql->bindValue(':cpf',$cpf);
		$sql->bindValue(':email',$email);
		$sql->bindValue(':tipo',$tipo);
		$sql->bindValue(':ativo',$ativo);
		$sql->bindValue(':id',$id);
		$sql->bindValue(':senha',md5($senha));
		$sql->execute();

		return true;



	}

		public function editarPerfil( $id, $email = '' ,$cpf = '',$senha = ' '){
		global $pdo; 


		$sql = $pdo->prepare("UPDATE usuarios SET email = :email, cpf = :cpf, senha = :senha WHERE id = :id");

		$sql->bindValue(':email',$email);
		$sql->bindValue(':cpf',$cpf);
		$sql->bindValue(':senha',md5($senha));
		$sql->bindValue(':id',$id);
		$sql->execute();

		return true;

	}



	public function login($usuario, $senha){
		global $pdo;

		

		$sql=$pdo->prepare("SELECT * FROM usuarios WHERE login = :usuario AND senha = :senha");
		$sql->bindValue(':usuario',$usuario);
		$sql->bindValue(':senha',md5($senha));
		$sql->execute();




		if($sql->rowCount() > 0){

			$dado = $sql->fetch();
			$_SESSION['cLogin'] = $dado['id'];
			$_SESSION['tipo'] = $dado['tipo'];
			$_SESSION['nome'] = $dado['nome'];
			
			$this->nome = $dado['nome'];
			return true;

		}else {

			return false;
		
		}

	}

	public function getUsuarios(){
		global $pdo;
		$array = array();

		$sql = $pdo->query("SELECT * FROM usuarios"); 
		$sql->execute();

		if($sql->rowCount() > 0){

			$array = $sql->fetchAll();

			return $array;

		}

	}



	public function getUsuario($id){

		global $pdo;
		$array = array();

		$sql = $pdo->prepare("SELECT * FROM usuarios WHERE id = :id"); 
		$sql->bindValue(':id', $id);
		$sql->execute();

		if($sql->rowCount() > 0){

			$array = $sql->fetch();

			return $array;



		}


	}

	public function getNomeUsuarios(){
		global $pdo;
		$array = array();

		$sql = $pdo->query("SELECT id, nome FROM usuarios"); 
		$sql->execute();

		if($sql->rowCount() > 0){

			$array = $sql->fetchAll();
			return $array;

			}
		}

	public function excluirUsuario($id){
		global $pdo;
		

		$sql = $pdo->prepare("DELETE FROM usuarios WHERE id = :id");
		$sql->bindValue(':id', $id); 
		$sql->execute();

		return true;
	}



}

?>