<?php 

class Atendimento{


	public function getRecepcoes($id_faa = "", $nome_paciente = "", $data_inicial = "", $data_final = ""){

		global $pdo;

		$array = array();

		// Correção do BUG da babilônia 1970-01-01 - NÃO MEXER
		if($data_inicial == "1970-01-01" && $data_final  == "1970-01-01" ){
			$data_inicial = "";
			$data_final = "";

		}

		if(!empty($data_inicial) && !empty($data_final) && empty($nome_paciente)){

			$sql = $pdo->prepare("SELECT 
			rec.id_faa as faa,
			rec.dt_atend_recepcao as data_atendimento,
			rec.hr_atend_recepcao as hora_atendimento,
			pac.id as id_paciente,
			pac.nome nome_paciente,
			pac.data_nasc as nascimento_paciente
			FROM
			tbl_recepcao as rec
			INNER JOIN tbl_pacientes as pac ON (pac.id = rec.id_paciente)
			WHERE dt_atend_recepcao BETWEEN :data_inicial AND :data_final");
			$sql->bindValue(':data_inicial',$data_inicial);
			$sql->bindValue(':data_final',$data_final);
			$sql->execute();

			if($sql->rowCount() > 0){
				$array = $sql->fetchAll();

			}

			return $array;
			exit;
		
		}



		if(!empty($nome_paciente) && !empty($data_inicial) && !empty($data_final)){

			$nome_paciente = "%".$nome_paciente."%";

			$sql = $pdo->prepare("SELECT 
			rec.id_faa as faa,
			rec.dt_atend_recepcao as data_atendimento,
			rec.hr_atend_recepcao as hora_atendimento,
			pac.id as id_paciente,
			pac.nome as nome_paciente,
			pac.data_nasc as nascimento_paciente
			FROM
			tbl_recepcao as rec
			INNER JOIN tbl_pacientes as pac ON (pac.id = rec.id_paciente)
			WHERE pac.nome LIKE :nome_paciente AND dt_atend_recepcao BETWEEN :data_inicial AND :data_final");
			$sql->bindValue(':nome_paciente',$nome_paciente);
			$sql->bindValue(':data_inicial',$data_inicial);
			$sql->bindValue(':data_final',$data_final);
			$sql->execute();

			if($sql->rowCount() > 0){
				$array = $sql->fetchAll();

			}

			return $array;
			exit;
		
		}



		if(!empty($nome_paciente)){


			$nome_paciente = "%".$nome_paciente."%";

			$sql = $pdo->prepare("SELECT 
			rec.id_faa as faa,
			rec.dt_atend_recepcao as data_atendimento,
			rec.hr_atend_recepcao as hora_atendimento,
			pac.id as id_paciente,
			pac.nome as nome_paciente,
			pac.data_nasc as nascimento_paciente
			FROM
			tbl_recepcao as rec
			INNER JOIN tbl_pacientes as pac ON (pac.id = rec.id_paciente)
			WHERE pac.nome LIKE :nome_paciente");
			$sql->bindValue(':nome_paciente',$nome_paciente);
			$sql->execute();

			if($sql->rowCount() > 0){
				$array = $sql->fetchAll();

			}

			return $array;
			exit;
		
		}

		if(!empty($id_faa)){

			$sql = $pdo->prepare("SELECT 
			rec.id_faa as faa,
			rec.dt_atend_recepcao as data_atendimento,
			rec.hr_atend_recepcao as hora_atendimento,
			pac.id as id_paciente,
			pac.nome as nome_paciente,
			pac.data_nasc as nascimento_paciente
			FROM
			tbl_recepcao as rec
			INNER JOIN tbl_pacientes as pac ON (pac.id = rec.id_paciente)
			WHERE rec.id_faa = :id_faa");
			$sql->bindValue(':id_faa',$id_faa);
			$sql->execute();

			if($sql->rowCount() > 0){
				$array = $sql->fetchAll();

			}

			return $array;
			exit;
		
		}

		
	}

	function pesquisarRecepcoes($id_faa = "", $nome_paciente = "", $data_incial = "", $data_final = ""){

		global $pdo;


		$array = "";

		if(!empty($id_faa)){

		$sql = $pdo->prepare("SELECT 
		rec.id_faa as faa,
		rec.dt_atend_recepcao as data_atendimento,
		rec.hr_atend_recepcao as hora_atendimento,
		pac.id as id_paciente,
		pac.nome nome_paciente,
		pac.data_nasc as nascimento_paciente
		FROM
		tbl_recepcao as rec
		INNER JOIN tbl_pacientes as pac ON (pac.id = rec.id_paciente)
		WHERE id_faa = :id_faa");
		$sql->bindValue(":id_faa",$id_faa);
		$sql->execute();

			if($sql->rowCount() > 0){
				
				$array = $sql->fetch();

			}

		return $array;
		exit;


		}

		if(!empty($nome_paciente)){

			$nome_paciente = "%".$nome_paciente."%";

		$sql = $pdo->prepare("SELECT id, nome, data_nasc FROM tbl_recepcao WHERE nome LIKE :nome_paciente");
		$sql->bindValue(":nome_paciente",$nome_paciente);
		$sql->execute();

			if($sql->rowCount() > 0){
				
				$array = $sql->fetchAll();

			}

		return $array;
		exit;


		}

		if(!empty($nascimento_paciente)){

		$sql = $pdo->prepare("SELECT id, nome, data_nasc FROM tbl_pacientes WHERE data_nasc = :data_nasc");
		$sql->bindValue(":data_nasc",$nascimento_paciente);
		$sql->execute();

			if($sql->rowCount() > 0){
				
				$array = $sql->fetchAll();

			}

		return $array;
		exit;


		}



		

		
	}

	function cadastrarRecepcao($id_paciente, $id_profissional, $tipo_atendimento, $id_usuario_cadastro, $motivo, $acao_programatica){

		$dt_atend_recepcao = date("Y-m-d");

		global $pdo;

		// Verifica qual é último id gerado de FAA 
		$consulta = $pdo->query("SELECT id as id_recepcao FROM tbl_recepcao ORDER BY id DESC limit 1");
		$consulta->execute();
		$resultado = $consulta->fetch();



		$id_recepcao = $resultado['id_recepcao'];


		$id_recepcao = $id_recepcao + 1;

		if(empty($id_recepcao)){

			$id_recepcao = 1;

		}

		// Gerar o número de faa com id da recepcao
		$sql = $pdo->prepare("INSERT INTO tbl_faa SET id_paciente = :id_paciente, id_recepcao = :id_recepcao");
		$sql->bindValue(':id_recepcao', $id_recepcao);
		$sql->bindValue(':id_paciente', $id_paciente);
		$sql->execute();

		// Verifica qual é último id gerado de FAA 
		$consulta = $pdo->query("SELECT id as id_faa FROM tbl_faa ORDER BY id DESC limit 1");
		$consulta->execute();
		$resultado = $consulta->fetch();

		$id_faa = $resultado['id_faa'];

		
		$sql = $pdo->prepare("INSERT INTO tbl_recepcao SET id_faa =:id_faa, id_paciente = :id_paciente, id_profissional = :id_profissional, dt_atend_recepcao = :dt_atend_recepcao, hr_atend_recepcao = CURTIME(), tipo_atendimento = :tipo_atendimento, id_usuario_cadastro = :id_usuario_cadastro, motivo = :motivo, acao_programatica = :acao_programatica");

		$sql->bindValue(":id_faa",$id_faa);
		$sql->bindValue(":id_paciente",$id_paciente);
		$sql->bindValue(":id_profissional",$id_profissional);
		$sql->bindValue(":dt_atend_recepcao",$dt_atend_recepcao);
		$sql->bindValue(":tipo_atendimento",$tipo_atendimento);
		$sql->bindValue(":id_usuario_cadastro",$id_usuario_cadastro);
		$sql->bindValue(":motivo",$motivo);
		$sql->bindValue(":acao_programatica",$acao_programatica);
		$sql->execute();

		return true;

	}

	function editarRecepcao($id_paciente, $id_profissional, $tipo_atendimento, $id_usuario_cadastro, $motivo, $acao_programatica, $id_faa){

		$dt_atend_recepcao = date("Y-m-d");

		global $pdo;


		$sql = $pdo->prepare("UPDATE tbl_recepcao SET id_paciente = :id_paciente, id_profissional = :id_profissional, dt_atend_recepcao = :dt_atend_recepcao, hr_atend_recepcao = CURTIME(), tipo_atendimento = :tipo_atendimento, id_usuario_cadastro = :id_usuario_cadastro, motivo = :motivo, acao_programatica = :acao_programatica WHERE id_faa = :id_faa");

		$sql->bindValue(":id_faa",$id_faa);
		$sql->bindValue(":id_paciente",$id_paciente);
		$sql->bindValue(":id_profissional",$id_profissional);
		$sql->bindValue(":dt_atend_recepcao",$dt_atend_recepcao);
		$sql->bindValue(":tipo_atendimento",$tipo_atendimento);
		$sql->bindValue(":id_usuario_cadastro",$id_usuario_cadastro);
		$sql->bindValue(":motivo",$motivo);
		$sql->bindValue(":acao_programatica",$acao_programatica);
		$sql->bindValue(":id_faa",$id_faa);
		$sql->execute();


		$sql = $pdo->prepare("UPDATE tbl_faa SET id_paciente = :id_paciente WHERE id_faa = :id_faa");

		$sql->bindValue(":id_paciente",$id_paciente);
		$sql->bindValue(":id_faa",$id_faa);
		$sql->execute();

		return 1;

	}


	public function getRecepcao($id_faa, $id_paciente = ""){


		global $pdo;

		if(!empty($id_faa) && !empty($id_paciente)){




			// Gerar o número de faa com id da recepcao
			$sql = $pdo->prepare("SELECT * FROM tbl_recepcao WHERE id_faa = :id_faa");
			$sql->bindValue(':id_faa', $id_faa);
			$sql->execute();

			$array = $sql->fetch();


			$retorno['id_faa'] = $array['id_faa'];
			$retorno['id_profissional'] = $array['id_profissional'];
			$retorno['motivo_atendimento'] = $array['motivo'];
			$retorno['data_atendimento'] = $array['dt_atend_recepcao'];
			$retorno['hora_atendimento'] = $array['hr_atend_recepcao'];
			$retorno['tipo_atendimento'] = $array['tipo_atendimento'];
			$retorno['id_usuario_cadastro'] = $array['id_usuario_cadastro'];
			$retorno['acao_programatica'] = $array['acao_programatica'];


			// Gerar o número de faa com id da recepcao
			$sql = $pdo->prepare("SELECT * FROM tbl_pacientes WHERE id = :id_paciente");
			$sql->bindValue(':id_paciente', $id_paciente);
			$sql->execute();

			$array = $sql->fetch();


			$retorno['id_paciente'] = $array['id'];
			$retorno['nome_paciente'] = $array['nome'];
			$retorno['nascimento_paciente'] = $array['data_nasc'];
			$retorno['sexo_paciente'] = $array['sexo'];
			$retorno['cpf_paciente'] = $array['cpf'];
			$retorno['rg_paciente'] = $array['rg'];
			$retorno['pai_paciente'] = $array['pai'];
			$retorno['mae_paciente'] = $array['mae'];
			$retorno['cartao_sus_paciente'] = $array['cartao_sus'];
			$retorno['bairro_paciente'] = $array['id_bairro'];
			$retorno['rua_paciente'] = $array['id_rua'];
			$retorno['complemento_paciente'] = $array['complemento'];
			$retorno['numero_paciente'] = $array['numero'];
			$retorno['email_paciente'] = $array['email'];
			$retorno['contato1'] = $array['contato1'];
			$retorno['parentesco1'] = $array['parentesco1'];
			$retorno['contato2'] = $array['contato2'];
			$retorno['parenetsco2'] = $array['parentesco2'];


			return $retorno;
			exit;



		}else if(!empty($id_faa)){



			// Gerar o número de faa com id da recepcao
			$sql = $pdo->prepare("SELECT 
			faa.id as id_faa,
			pac.id as id_paciente,
			pac.nome as nome_paciente,
			pac.data_nasc as nascimento_paciente,
			pac.sexo as sexo_paciente,
			pac.cpf as cpf_paciente,
			pac.rg as rg_paciente,
			pac.pai as pai_paciente,
			pac.mae as mae_paciente,
			pac.cartao_sus as cartao_sus_paciente,
			pac.id_bairro as bairro_paciente,
			pac.id_rua as rua_paciente,
			pac.complemento as complemento_paciente,
			pac.numero as numero_paciente,
			pac.email as email_paciente,
			pac.data_cadastro as cadastro_paciente,
			pac.contato1,
			pac.parentesco1,
			pac.contato2,
			pac.parentesco2,
			pac.id_usuario_cadastro,
			rec.id_profissional,
			rec.dt_atend_recepcao as data_atendimento,
			rec.hr_atend_recepcao as hora_atendimento,
			rec.tipo_atendimento,
			rec.motivo as motivo_atendimento,
			rec.acao_programatica
			FROM
			tbl_faa as faa
			INNER JOIN tbl_recepcao as rec ON (rec.id = faa.id_recepcao)
			INNER JOIN tbl_pacientes as pac ON (pac.id = faa.id_paciente)
			WHERE id_faa = :id_faa");
			$sql->bindValue(':id_faa', $id_faa);
			$sql->execute();

			if($sql->rowCount() > 0){
					
				$array = $sql->fetch();

			}

			return $array;
			exit;

		}


	}

}


?>