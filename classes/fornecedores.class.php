<?php 


class Fornecedores{

		private $nome;


		public function getFornecedor($id){
		global $pdo;
		$array = array();

		$sql = $pdo->prepare("SELECT id, nome_social, nome_fantasia, cnpj, endereco, bairro, telefone, ativo FROM fornecedor WHERE id = :id");
		$sql->bindValue(':id',$id);
		$sql->execute();

			if($sql->rowCount() > 0){
				$array = $sql->fetch();
			}

		return $array;

		} 


	public function getFornecedores(){
		global $pdo;
		$array = array();

		$sql = $pdo->query("SELECT id, nome_social, ativo FROM fornecedor WHERE ativo = '1'");
		$sql->execute();

		if($sql->rowCount() > 0){
			$array = $sql->fetchAll();

			
		}

		return $array;

	} 

		public function getFornecedoresPainel(){
		global $pdo;
		$array = array();

		$sql = $pdo->query("SELECT id, nome_social, nome_fantasia, cnpj, endereco, bairro, telefone, ativo FROM fornecedor");
		$sql->execute();

		if($sql->rowCount() > 0){
			$array = $sql->fetchAll();

			
		}

		return $array;

	} 

	public function editarFornecedor($nome_social, $nome_fantasia, $cnpj, $endereco, $bairro, $telefone, $ativo, $id){


		global $pdo;

		$sql = $pdo->prepare("UPDATE fornecedor SET nome_social = :nome_social, nome_fantasia = :nome_fantasia, cnpj = :cnpj, endereco = :endereco, bairro = :bairro, telefone = :telefone, ativo = :ativo WHERE id = :id_fornecedor");
		
		$sql->bindValue(':nome_social',$nome_social);
		$sql->bindValue(':nome_fantasia',$nome_fantasia);
		$sql->bindValue(':cnpj',$cnpj);
		$sql->bindValue(':endereco',$endereco);
		$sql->bindValue(':bairro',$bairro);
		$sql->bindValue(':telefone',$telefone);
		$sql->bindValue(':ativo', $ativo);
		$sql->bindValue(':id_fornecedor', $id);
		$sql->execute();

		return true;

	}

	public function cadastrarFornecedor($nome_social, $nome_fantasia, $cnpj, $endereco, $bairro, $telefone, $ativo){

		global $pdo;

		$sql = $pdo->prepare("INSERT INTO fornecedor SET nome_social = :nome_social, nome_fantasia = :nome_fantasia, cnpj = :cnpj, endereco = :endereco, bairro = :bairro, telefone = :telefone, ativo = :ativo");
		
		$sql->bindValue(':nome_social',$nome_social);
		$sql->bindValue(':nome_fantasia',$nome_fantasia);
		$sql->bindValue(':cnpj',$cnpj);
		$sql->bindValue(':endereco',$endereco);
		$sql->bindValue(':bairro',$bairro);
		$sql->bindValue(':telefone',$telefone);
		$sql->bindValue(':ativo', $ativo);
		$sql->execute();


		return true;

	}

	public function excluirFornecedor($id){
		global $pdo;

		if($this->FornecedorUso($id) == 1){

			return 1;

		}else{
			$sql = $pdo->prepare("DELETE FROM fornecedor WHERE id = :id");
			$sql->bindValue(':id',$id);
			$sql->execute();

			return 2;

		}
	}

	public function FornecedorUso($id){
		global $pdo;

		$verificao1 = $pdo->prepare("SELECT id_fornecedor FROM requisicaoentrada WHERE id_fornecedor = :id_fornecedor");
		$verificao1->bindValue(":id_fornecedor", $id);	
		$verificao1->execute();

		if($verificao1->rowCount() > 0){
			return 1;

		}	
	}

}