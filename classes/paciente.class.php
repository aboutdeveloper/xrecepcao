<?php 


class Paciente{
	private $nome;

		public function getPaciente($id){
			global $pdo;
			$array = array();

			$sql = $pdo->prepare("SELECT * FROM tbl_pacientes WHERE id = :id");
			$sql->bindValue(":id",$id);
			$sql->execute();

			if($sql->rowCount() > 0){
				$array = $sql->fetch();

				
				}

			return $array;


		}

	public function getPacientes(){
		global $pdo;
		$array = array();

		$sql = $pdo->query("SELECT * FROM tbl_pacientes");
		$sql->execute();

		if($sql->rowCount() > 0){
			$array = $sql->fetchAll();

			
		}


		return $array;

	}


	public function cadastrarPaciente($nome, $data_nasc, $sexo, $cartao_sus, $data_cadastro, $contato1, $parentesco1, $id_usuario_cadastro, $cpf = "",  $rg = "", $bairro = "", $rua = "", $numero = "", $complemento = "", $email = "", $contato2 = "", $parentesco2 = "", $prontuario_old = "", $n_prontuario_old = ""){

		/*
		$nome, $data_nasc, $sexo, $cartao_sus, $data_cadastro, $contato1, $parentesco1, $id_usuario_cadastro, $cpf = "",  $rg = "", $bairro = "", $rua = "", $numero = "", $complemento = "", $email = "", $contato2 = "", $parentesco2 = "", $prontuario_old = "", $n_prontuario_old = ""

		*/


		global $pdo;

		/*
		echo $nome."<br>";
		echo $data_nasc."<br>";
		echo $sexo."<br>";
		echo $cpf."<br>";
		echo $rg."<br>";
		echo $bairro."<br>";
		echo $rua."<br>";
		echo $complemento."<br>";
		echo $numero."<br>";
		echo $email."<br>";
		echo $cartao_sus."<br>";
		echo $data_cadastro."<br>";
		echo $contato1."<br>";
		echo $parentesco1."<br>";
		echo $contato2."<br>";
		echo $parentesco2."<br>";
		echo $id_usuario_cadastro."<br>";
		print_r($prontuario_old)."<br>";
		echo $n_prontuario_old."<br>";


		*/






		// Verifica se há um paciente com o mesmo número de SUS
		$sql = $pdo->prepare("SELECT id, nome, data_nasc, cartao_sus FROM tbl_pacientes WHERE cartao_sus = :cartao_sus");
		$sql->bindValue(":cartao_sus", $cartao_sus);
		$sql->execute();


		if($sql->rowCount() > 0){

			$resultado = $sql->fetch();

		}


		// Se houver ele retorna os dados do paciente encontrado
		if(!empty($resultado)){

			return $resultado;
			exit;
			

		}

		// Depois ajustar para utilizar um único nome de variável, no caso $prontuario_old
		$_FILES['arquivo'] = $prontuario_old;


		// Se for enviado um arquivo ele entra no if
		if(!empty($_FILES['arquivo'])){



			// Verifica se há mais de um arquivo sendo enviado
			if(count($_FILES['arquivo']['tmp_name']) > 0){

				// Faz o loop na quantidade de arquivos que foram enviados
				for($q = 0; $q < count($_FILES['arquivo']['tmp_name']); $q++){

					// Monta o nome do arquivo gerando um hash
					$nomearquivo = md5($_FILES['arquivo']['name'][$q].time().rand(0,999).'.pdf');
					// Move o arquivo para a pasta prontuários_old
					move_uploaded_file($_FILES['arquivo']['tmp_name'][$q], 'prontuarios_old/'.$nomearquivo);

				}

			}

		}else{

			$nomearquivo = "";

		}
		



		/* 

			$sql = $pdo->prepare("INSERT INTO paciente SET nome = :nome, data_nasc = :data_nasc, sexo = :sexo, cpf = :cpf, rg = :rg, id_bairro = :bairro, id_rua = :rua, complemento = :complemento, numero = :numero, email = :email, cartao_sus = :cartao_sus, data_cadastro = :data_cadastro, contato1 = :contato1, parentesco1 = :parentesco1, contato2 = :contato2, parentesco2 = :parentesco2, id_usuario_cadastro = :id_usuario_cadastro, prontuario_old = :prontuario_old, n_prontuario_old = :n_prontuario_old");


		*/


		$sql = $pdo->prepare("INSERT INTO tbl_pacientes SET nome = :nome, data_nasc = :data_nasc, sexo = :sexo, cpf = :cpf, rg = :rg, id_bairro = :bairro, id_rua = :rua, complemento = :complemento, numero = :numero, email = :email, cartao_sus = :cartao_sus, data_cadastro = :data_cadastro, contato1 = :contato1, parentesco1 = :parentesco1, contato2 = :contato2, parentesco2 = :parentesco2, id_usuario_cadastro = :id_usuario_cadastro, prontuario_old = :prontuario_old, n_prontuario_old = :n_prontuario_old");

		$sql->bindValue(':nome',$nome);
		$sql->bindValue(':data_nasc',$data_nasc);
		$sql->bindValue(':sexo',$sexo);
		$sql->bindValue(':cpf',$cpf);
		$sql->bindValue(':rg',$rg);
		$sql->bindValue(':bairro',$bairro);
		$sql->bindValue(':rua',$rua);
		$sql->bindValue(':complemento',$complemento);
		$sql->bindValue(':numero',$numero);
		$sql->bindValue(':email',$email);
		$sql->bindValue(':cartao_sus',$cartao_sus);
		$sql->bindValue(':data_cadastro',$data_cadastro);
		$sql->bindValue(':contato1',$contato1);
		$sql->bindValue(':parentesco1',$parentesco1);
		$sql->bindValue(':contato2',$contato2);
		$sql->bindValue(':parentesco2',$parentesco2);
		$sql->bindValue(':id_usuario_cadastro',$id_usuario_cadastro);
		$sql->bindValue(':prontuario_old',$nomearquivo);
		$sql->bindValue(":n_prontuario_old", $n_prontuario_old);
		$sql->execute();

		return 1;


	}



	public function editarPaciente($nome, $data_nasc, $sexo, $cartao_sus, $data_cadastro, $contato1, $parentesco1, $id_usuario_cadastro, $id_paciente, $cpf = "",  $rg = "", $bairro = "", $rua = "", $numero = "", $complemento = "", $email = "", $contato2 = "", $parentesco2 = "", $prontuario_old = "", $n_prontuario_old = ""){


		global $pdo;

		/*
		echo $nome."<br>";
		echo $data_nasc."<br>";
		echo $sexo."<br>";
		echo $cpf."<br>";
		echo $rg."<br>";
		echo $bairro."<br>";
		echo $rua."<br>";
		echo $complemento."<br>";
		echo $numero."<br>";
		echo $email."<br>";
		echo $cartao_sus."<br>";
		echo $data_cadastro."<br>";
		echo $contato1."<br>";
		echo $parentesco1."<br>";
		echo $contato2."<br>";
		echo $parentesco2."<br>";
		echo $id_usuario_cadastro."<br>";
		echo $id_paciente."<br>";
		print_r($prontuario_old)."<br>";
		echo $n_prontuario_old."<br>";
		*/


	






		// Verifica se há um paciente com o mesmo número de SUS
		$sql = $pdo->prepare("SELECT id, nome, data_nasc, cartao_sus FROM tbl_pacientes WHERE cartao_sus = :cartao_sus AND id != :id");
		$sql->bindValue(":cartao_sus", $cartao_sus);
		$sql->bindValue(":id", $id_paciente);
		$sql->execute();


		if($sql->rowCount() > 0){

			$resultado = $sql->fetch();


		}


		// Se houver ele retorna os dados do paciente encontrado
		if(!empty($resultado)){


			return $resultado;
			exit;
			

		}




		// Depois ajustar para utilizar um único nome de variável, no caso $prontuario_old
		$_FILES['arquivo'] = $prontuario_old;


		// Se for enviado um arquivo ele entra no if
		if(!empty($_FILES['arquivo'])){



			// Busca se há um link de um prontuario antigo já lançado
			$sql = $pdo->prepare("SELECT prontuario_old FROM tbl_pacientes WHERE id = :id_paciente");
			$sql->bindValue(":id_paciente", $id_paciente);
			$sql->execute();
			$link = $sql->fetch();

			// Se houver um link ele apaga o arquivo na pasta prontuario_old;
			if($link['prontuario_old'] != NULL){


				$link = "prontuarios_old/".$link['prontuario_old'];

				unlink($link);
			}

			// Verifica se há mais de um arquivo sendo enviado
			if(count($_FILES['arquivo']['tmp_name']) > 0){

				// Faz o loop na quantidade de arquivos que foram enviados
				for($q = 0; $q < count($_FILES['arquivo']['tmp_name']); $q++){

					// Monta o nome do arquivo gerando um hash
					$nomearquivo = md5($_FILES['arquivo']['name'][$q].time().rand(0,999).'.pdf');
					// Move o arquivo para a pasta prontuários_old
					move_uploaded_file($_FILES['arquivo']['tmp_name'][$q], 'prontuarios_old/'.$nomearquivo);

				}



			}

		}else{

			$nomearquivo = "";

		}
		

		$sql = $pdo->prepare("UPDATE tbl_pacientes SET nome = :nome, data_nasc = :data_nasc, sexo = :sexo, cpf = :cpf, rg = :rg, id_bairro = :bairro, id_rua = :rua, complemento = :complemento, numero = :numero, email = :email, cartao_sus = :cartao_sus, data_cadastro = :data_cadastro, contato1 = :contato1, parentesco1 = :parentesco1, contato2 = :contato2, parentesco2 = :parentesco2, id_usuario_cadastro = :id_usuario_cadastro, prontuario_old = :prontuario_old, n_prontuario_old = :n_prontuario_old WHERE id = :id_paciente");

		$sql->bindValue(':nome',$nome);
		$sql->bindValue(':sexo',$sexo);
		$sql->bindValue(':data_nasc',$data_nasc);
		$sql->bindValue(':cpf',$cpf);
		$sql->bindValue(':rg',$rg);
		$sql->bindValue(':bairro',$bairro);
		$sql->bindValue(':rua',$rua);
		$sql->bindValue(':complemento',$complemento);
		$sql->bindValue(':numero',$numero);
		$sql->bindValue(':email',$email);
		$sql->bindValue(':cartao_sus',$cartao_sus);
		$sql->bindValue(':data_cadastro',$data_cadastro);
		$sql->bindValue(':contato1',$contato1);
		$sql->bindValue(':parentesco1',$parentesco1);
		$sql->bindValue(':contato2',$contato2);
		$sql->bindValue(':parentesco2',$parentesco2);
		$sql->bindValue(':id_usuario_cadastro',$id_usuario_cadastro);
		$sql->bindValue(':id_paciente',$id_paciente);
		$sql->bindValue(':prontuario_old',$nomearquivo);
		$sql->bindValue(":n_prontuario_old", $n_prontuario_old);
		$sql->execute();

		return 1;



	}

	public function excluirPaciente($id){


		global $pdo;


		$sql = $pdo->prepare("DELETE FROM tbl_pacientes WHERE id = :id");
		$sql->bindValue(":id",$id);
		$sql->execute();

		return 1;




	}

	public function ProdutoUso($id){

		global $pdo;

		$verificao1 = $pdo->prepare("SELECT id_produto FROM itemreqsaida WHERE id_produto = :id_produto");
		$verificao1->bindValue(":id_produto",$id);
		$verificao1->execute();

		if($verificao1->rowCount() > 0){

			return 1;
		}



		$verificao2 = $pdo->prepare("SELECT id_produto FROM itemreqentrada WHERE id_produto = :id_produto");
		$verificao2->bindValue(":id_produto",$id);
		$verificao2->execute();

		if($verificao2->rowCount() > 0){

			return 1;

		}






	}

		public function getNomeProdutos(){
		global $pdo;
		$array = array();

		$sql = $pdo->query("SELECT id, nome FROM produtos WHERE ativo = '1'");
		$sql->execute();

		if($sql->rowCount() > 0){
			$array = $sql->fetchAll();

			
		}

		return $array;

	} 

	public function getBairros(){

		global $pdo;
		$array = array();

		$sql = $pdo->query("SELECT id, nome FROM tbl_bairros");
		$sql->execute();

		if($sql->rowCount() > 0){
			$array = $sql->fetchAll();

			
		}

		return $array;

	}

		public function getRuas(){

		global $pdo;
		$array = array();

		$sql = $pdo->query("SELECT id, descricao FROM tbl_ruas");
		$sql->execute();

		if($sql->rowCount() > 0){
			$array = $sql->fetchAll();

			
		}

		return $array;

	}

	function pesquisarPaciente($nome_paciente = "", $nascimento_paciente = "", $id_paciente = ""){

		global $pdo;


		$array = "";

		if(!empty($id_paciente)){

		$sql = $pdo->prepare("SELECT id, nome, data_nasc FROM tbl_pacientes WHERE id = :id_paciente");
		$sql->bindValue(":id_paciente",$id_paciente);
		$sql->execute();

			if($sql->rowCount() > 0){
				
				$array = $sql->fetchAll();

			}

		return $array;
		exit;


		}

		if(!empty($nome_paciente)){

			$nome_paciente = "%".$nome_paciente."%";

		$sql = $pdo->prepare("SELECT id, nome, data_nasc FROM tbl_pacientes WHERE nome LIKE :nome_paciente");
		$sql->bindValue(":nome_paciente",$nome_paciente);
		$sql->execute();

			if($sql->rowCount() > 0){
				
				$array = $sql->fetchAll();

			}

		return $array;
		exit;


		}

		if(!empty($nascimento_paciente)){

		$sql = $pdo->prepare("SELECT id, nome, data_nasc FROM tbl_pacientes WHERE data_nasc = :data_nasc");
		$sql->bindValue(":data_nasc",$nascimento_paciente);
		$sql->execute();

			if($sql->rowCount() > 0){
				
				$array = $sql->fetchAll();

			}

		return $array;
		exit;


		}
	
	}

	
}


?>