<?php 
require_once 'header.php';
require_once 'aside.php';

?>


 <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <!-- Start: Topbar -->
      <header id="topbar" class="alt">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-icon">
              <a href="saida-materiais.php">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-active">
              <a href="saida-materiais.php">Cadastros</a>
            </li>
            <li class="crumb-trail">
              <a href="usuarios.php">Usuários</a>
            </li>
            <li class="crumb-trail">
              <a href="add-usuario.php">Adicionar usuário</a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">
        <!-- begin: .tray-center -->
        <div class="tray tray-center">
          <div class="mw1000 center-block">
            <!-- Begin: Admin Form -->
            <div class="admin-form">

                    <div class="row">
                     <?php 
                     
                     $u = new Usuarios();
                     if(isset($_POST['nome']) && !empty($_POST['nome'])){
                       $nome = addslashes($_POST['nome']);
                       $cpf = addslashes($_POST['cpf']);
                       $email = addslashes($_POST['email']);
                       $senha = $_POST['senha'];

                       $tipo = addslashes($_POST['tipo']);
                       $ativo = addslashes($_POST['ativo']);



                      if(!empty($nome) && !empty($email) && !empty($senha) && !empty($cpf)){

                        if($u->cadastrar($nome, $cpf, $email, $senha, $tipo, $ativo)){
                          ?>
                          <div class="alert alert-success alert-dismissable">
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <i class="fa fa-remove pr10"></i>
                            Usuário cadastrado com sucesso
                          </div>
                        </div>
                          <?php 


                        }else{
                          ?>
                       
                        <div class="alert alert-danger alert-dismissable">
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <i class="fa fa-remove pr10"></i>
                            Este usuário já existe
                          </div>
                        </div>
                          <?php
                            }
                        }


                      }else{

                        ?>
                        <div class="col-sm-12 pr30">
                          <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <i class="fa fa-remove pr10"></i>
                            Preecha todos os campos !
                          </div>
                        </div>
                        <?php 
                          }
                       ?>

                    </div>
                    <br>
                    <br>
                    <div class="admin-form theme-primary">
                      <div class="panel heading-border panel-primary">
                        <div class="panel-body bg-light">      
                          <div class="section-divider mb40" id="spy1">
                            <span>Cadastro de usuários</span>
                          </div>
                          <form method="POST">
                            <div class="row">
                          
                              <div class="col-md-4">
                                <div class="form-group">
                                  <label for="nome"><b>Nome:</b></label>
                                  <input type="text" name="nome" id="nome" class="form-control" required="">
                                </div>
                              </div>
                              <div class="col-md-2">
                                <div class="form-group">
                                  <label for="cpf"><b>CPF:</b></label>
                                  <input type="text" name="cpf" id="cpf" class="form-control">
                                </div>
                              </div>
                               <div class="col-md-3">
                                 <div class="form-group">
                                    <label for="email"><b>E-mail:</b></label>
                                    <input type="text" name="email" id="email" class="form-control" required>
                                  </div>
                               </div>
                              
                              <div class="col-md-2">
                                <div class="form-group">
                                  <label for="senha"><b>Senha:</b></label>
                                  <input type="password" name="senha" id="senha" class="form-control" required>
                                </div>
                              </div>
                              
                            </div><!-- fim da row -->
                            <div class="row">
                              <div class="col-md-2">
                                <div class="form-group">
                                  <label for="tipo"><b>Tipo</b></label>
                                  <select name="tipo" id="tipo" class="form-control">
                                    <option value="1">Requisitante</option>
                                    <option value="2">Atendente</option>
                                    <option value="3">Gerente</option>
                                    <option value="4">Administrador</option>
                                  </select>
                                </div>
                              </div>
                              <div class="col-md-2">
                                <div class="form-group">
                                  <label for="ativo"><b>Ativo</b></label>
                                  <select name="ativo" id="ativo" class="form-control">
                                    <option value="sim">Sim</option>
                                    <option value="nao">Não</option>
                                  </select>
                                </div>
                              </div>
                              
                            </div><!-- fim da row -->
                            <div class="row">
                              <div class="col-md-2">
                                <input type="submit" name="Cadastrar" class="btn btn-success" value="Cadastrar">
                              </div>
                            </div>
                          </form> 

            </div>
          </div>
        </div>
        <!-- end: .tray-center -->
      </section>
      <!-- End: Content -->
    </section>
  </div>
  <!-- End: Main -->

  <style>
  /* demo page styles */
  body { min-height: 2300px; }
  
  .content-header b,
  .admin-form .panel.heading-border:before,
  .admin-form .panel .heading-border:before {
    transition: all 0.7s ease;
  }
  /* responsive demo styles */
  @media (max-width: 800px) {
    .admin-form .panel-body { padding: 18px 12px; }
    .option-group .option { display: block; }
    .option-group .option + .option { margin-top: 8px; }
  }
  </style>
    

  <!-- BEGIN: PAGE SCRIPTS -->

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  jQuery(document).ready(function() {

    "use strict";

    // Init Theme Core    
    Core.init();

    // Form Switcher
    $('#form-switcher > button').on('click', function() {
      var btnData = $(this).data('form-layout');
      var btnActive = $('#form-elements-pane .admin-form.active');

      // Remove any existing animations and then fade current form out
      btnActive.removeClass('slideInUp').addClass('animated fadeOutRight animated-shorter');
      // When above exit animation ends remove leftover classes and animate the new form in
      btnActive.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
        btnActive.removeClass('active fadeOutRight animated-shorter');
        $('#' + btnData).addClass('active animated slideInUp animated-shorter')
      });
    });

    // Cache several DOM elements
    var pageHeader = $('.content-header').find('b');
    var adminForm = $('.admin-form');
    var options = adminForm.find('.option');
    var switches = adminForm.find('.switch');
    var buttons = adminForm.find('.button');
    var Panel = adminForm.find('.panel');

    // Form Skin Switcher
    $('#skin-switcher a').on('click', function() {
      var btnData = $(this).data('form-skin');

      $('#skin-switcher a').removeClass('item-active');
      $(this).addClass('item-active')

      adminForm.each(function(i, e) {
        var skins = 'theme-primary theme-info theme-success theme-warning theme-danger theme-alert theme-system theme-dark';
        var panelSkins = 'panel-primary panel-info panel-success panel-warning panel-danger panel-alert panel-system panel-dark';
        $(e).removeClass(skins).addClass('theme-' + btnData);
        Panel.removeClass(panelSkins).addClass('panel-' + btnData);
        pageHeader.removeClass().addClass('text-' + btnData);
      });

      $(options).each(function(i, e) {
        if ($(e).hasClass('block')) {
          $(e).removeClass().addClass('block mt15 option option-' + btnData);
        } else {
          $(e).removeClass().addClass('option option-' + btnData);
        }
      });
      $(switches).each(function(i, ele) {
        if ($(ele).hasClass('switch-round')) {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-round switch-' + btnData);
          } else {
            $(ele).removeClass().addClass('switch switch-round switch-' + btnData);
          }
        } else {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-' + btnData);
          } else {
            $(ele).removeClass().addClass('switch switch-' + btnData);
          }
        }

      });
      buttons.removeClass().addClass('button btn-' + btnData);
    });

    setTimeout(function() {
      adminForm.addClass('theme-primary');
      Panel.addClass('panel-primary');
      pageHeader.addClass('text-primary');

      $(options).each(function(i, e) {
        if ($(e).hasClass('block')) {
          $(e).removeClass().addClass('block mt15 option option-primary');
        } else {
          $(e).removeClass().addClass('option option-primary');
        }
      });
      $(switches).each(function(i, ele) {

        if ($(ele).hasClass('switch-round')) {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-round switch-primary');
          } else {
            $(ele).removeClass().addClass('switch switch-round switch-primary');
          }
        } else {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-primary');
          } else {
            $(ele).removeClass().addClass('switch switch-primary');
          }
        }
      });
      buttons.removeClass().addClass('button btn-primary');
    }, 800);

  });
  </script>
  <!-- END: PAGE SCRIPTS -->

</body>

</html>
