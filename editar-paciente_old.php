<?php 
require_once 'header.php';
require_once 'aside.php';
require_once 'classes/paciente.class.php';
?>

 <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <!-- Start: Topbar -->
      <header id="topbar" class="alt">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-icon">
              <a href="saida-materiais.php">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-active">
              <a href="saida-materiais.php">Cadastros</a>
            </li>
            <li class="crumb-trail">
              <a href="pacientes.php">Pacientes</a>
            </li>
            <li class="crumb-trail">
              <a href="add-produto.php">Adicionar paciente</a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">
        <!-- begin: .tray-center -->
        <div class="tray tray-center">
          <div class="mw1000 center-block">
            <!-- Begin: Admin Form -->
            <div class="admin-form">

                    <div class="row">
                     <?php 
                     
                     $p = new Pacientes();

                     if(isset($_POST['nome']) && !empty($_POST['nome'])){

                       $nome = addslashes($_POST['nome']);
                       $data_nasc = ($_POST['data_nasc']);
                       $nome_resp = ($_POST['nome_resp']);
                       $cartao_sus = ($_POST['cartao_sus']);
                       $data_cadastro = date('Y/m/d');
                       $contato1 = ($_POST['contato1']);
                       $parentesco1 = ($_POST['parentesco1']);
                       $contato2 = ($_POST['contato2']);
                       $parentesco2 = ($_POST['parentesco2']);
                       $id_usuario_cadastro = ($_SESSION['cLogin']);
                       $prontuario_old = $_FILES['arquivo'];








                      if(!empty($nome)){

                        if($p->cadastrarPaciente($nome, $data_nasc, $nome_resp, $cartao_sus, $data_cadastro, $contato1, $parentesco1, $contato2, $parentesco2, $id_usuario_cadastro, $prontuario_old)){
                          ?>
                          <div class="alert alert-success alert-dismissable">
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <i class="fa fa-remove pr10"></i>
                            Paciente cadastrado com sucesso !
                          </div>
                        </div>
                          <?php 


                        }else{
                          ?>
                       
                        <div class="alert alert-danger alert-dismissable">
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <i class="fa fa-remove pr10"></i>
                            O paciente já existe
                          </div>
                        </div>
                          <?php
                            }
                        }


                      }

                        ?>

                    </div>
                    <br>
                    <br>

                    <?php

                    if(isset($_GET['id']) && !empty($_GET['id'])){

                      $paciente = $p->getPaciente($_GET['id']);

                    }else{

                      ?>
                        <script type="text/javascript">window.location.href="pacientes.php"</script>
                      <?php
                    }


                    ?>
                    <div class="admin-form theme-primary">
                      <div class="panel heading-border panel-primary">
                        <div class="panel-body bg-light">      
                          <div class="section-divider mb40" id="spy1">
                            <span>Informações pessoais</span>
                          </div>
                          <form method="POST" enctype="multipart/form-data">
                            <div class="row" id="grid_itens">
                              <div class="col-md-2">
                                <div class="form-group">
                                  <label for="nome"><b>Id:</b></label>
                                  <input type="number" name="id" id="nome" class="form-control" required="" value="<?php echo $paciente['id']?>" readonly>
                                </div>
                              </div>
                              <div class="col-md-7">
                                <div class="form-group">
                                  <label for="data_baixa"><b>Nascimento</b></label>
                                  <input type="text" name="data_baixa" id="data_baixa maskedDate" maxlength="10" autocomplete="off" class="form-control date" required="" >
                                </div>
                              </div>
                              <div class="col-md-2">
                                <div class="form-group">
                                  <label for="data_nasc"><b>Nascimento:</b></label>
                                  <input type="text" name="data_nasc" id="data_nasc" class="form-control" required="" value="<?php echo $paciente['data_nasc']?>">
                                </div>
                              </div>
                            </div><!-- fim da row -->
                            <div class="row">
                              <div class="col-md-3">
                                <div class="form-group">
                                  <label for="sexo"><b>Sexo:</b></label>
                                  <input type="text" name="sexo" id="cartao_sus" class="form-control" required="" value="<?php echo $paciente['sexo']?>">
                                </div>
                              </div>
                              <div class="col-md-3">
                                <div class="form-group">
                                  <label for="cpf"><b>CPF:</b></label>
                                  <input type="text" name="cpdf" id="cartao_sus" class="form-control" required="" value="<?php echo $paciente['cpdf']?>">
                                </div>
                              </div>
                              <div class="col-md-3">
                                <div class="form-group">
                                  <label for="identidade"><b>Identidade:</b></label>
                                  <input type="text" name="identidade" id="cartao_sus" class="form-control" required="" value="<?php echo $paciente['identidade']?>">
                                </div>
                              </div>
                              <div class="col-md-3">
                                <div class="form-group">
                                  <label for="cartao_sus"><b>Cartão do SUS:</b></label>
                                  <input type="text" name="cartao_sus" id="cartao_sus" class="form-control" required="" value="<?php echo $paciente['cartao_sus']?>">
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-3">
                                <div class="form-group">
                                  <label for="nome_resp"><b>Bairro:</b></label>
                                  <input type="text" name="nome_resp" id="nome_resp" class="form-control" required="" value="<?php echo $paciente['nome_resp']; ?>">
                                </div>
                              </div>
                              <div class="col-md-4">
                                <div class="form-group">
                                  <label for="nome_resp"><b>Rua:</b></label>
                                  <input type="text" name="nome_resp" id="nome_resp" class="form-control" required="" value="<?php echo $paciente['nome_resp']; ?>">
                                </div>
                              </div>
                              <div class="col-md-4">
                                <div class="form-group">
                                  <label for="nome_resp"><b>Complemento:</b></label>
                                  <input type="text" name="nome_resp" id="nome_resp" class="form-control" required="" value="<?php echo $paciente['nome_resp']; ?>">
                                </div>
                              </div>
                              <div class="col-md-1">
                                <div class="form-group">
                                  <label for="nome_resp"><b>Número:</b></label>
                                  <input type="text" name="nome_resp" id="nome_resp" class="form-control" required="" value="<?php echo $paciente['nome_resp']; ?>">
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-4">
                                <div class="form-group">
                                  <label for="nome_resp"><b>E-mail:</b></label>
                                  <input type="text" name="nome_resp" id="nome_resp" class="form-control" required="" value="<?php echo $paciente['nome_resp']; ?>">
                                </div>
                              </div>
                              <div class="col-md-2">
                                <div class="form-group">
                                  <label for="contato1"><b>Contato 1:</b></label>
                                  <input type="text" name="contato1" id="contato1" class="form-control" required="" value="<?php echo $paciente['contato1']?>">
                                </div>
                              </div>
                              <div class="col-md-2">
                                <div class="form-group">
                                  <label for="parentesco1"><b>Parentesco 1:</b></label>
                                  <input type="text" name="parentesco1" id="parentesco1" class="form-control" required="" value="<?php echo $paciente['parentesco1']?>">
                                </div>
                              </div>
                              <div class="col-md-2">
                                <div class="form-group">
                                  <label for="contato2"><b>Contato 2:</b></label>
                                  <input type="text" name="contato2" id="contato2" class="form-control" required="" value="<?php echo $paciente['contato2']?>">
                                </div>
                              </div>
                              <div class="col-md-2">
                                <div class="form-group">
                                  <label for="paretesco2"><b>Parentesco 2:</b></label>
                                  <input type="text" name="parentesco2" id="paretesco2" class="form-control" required="" value="<?php echo $paciente['parentesco2']?>">
                                </div>
                              </div>
                            </div><!-- Fim da row -->
                            <div class="row">
                              <div class="col-md-12">
                                <div class="section-divider mb40" id="spy1">
                                  <span>Prontuário Digitalizado</span>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-6" style="margin-top: 25px;">
                                <div class="section">
                                  <label class="field prepend-icon append-button file">
                                    <span class="button btn-primary">Selecione o arquivo</span>
                                    <input type="file" class="gui-file" name="arquivo[]" id="arquivo[]" onchange="document.getElementById('uploader1').value = this.value;">
                                    <input type="text" class="gui-input" id="arquivo[]" placeholder="Por favor selecione os arquivos" multiple="">
                                    <label class="field-icon">
                                      <i class="fa fa-upload"></i>
                                    </label>
                                  </label>
                                </div>
                              </div>
                              <?php 

                              if(!empty($paciente['prontuario_old'])){

                              ?>

                              <div class="col-md-1" style="margin-top: 20px;">
                                <a href="prontuarios_old/<?php echo $paciente['prontuario_old']; ?>" target="_blank"><img src="assets/img/logos/logo_pdf.png" alt="prontuario_antigo"></a>
                              </div>
                              <div class="col-md-2">
                                <div class="form-group">
                                  <label for="n_prontuario_old"><b>Nº Prontuário:</b></label>
                                  <input type="text" name="n_prontuario_old" id="n_prontuario_old" class="form-control" value="<?php echo $paciente['n_prontuario_old']; ?>">
                                </div>
                              </div>

                              <?php

                              }


                              ?>

                            </div>
                            <div class="row">
                              <div class="col-md-2">
                                <input type="submit" name="Cadastrar" class="btn btn-success" value="Cadastrar" id="add-campo">
                              </div>
                            </div>
                          </form> 
                

            </div>
          </div>
        </div>
        <!-- end: .tray-center -->
      </section>
      <!-- End: Content -->
    </section>
  </div>
  <!-- End: Main -->

  <style>
  /* demo page styles */
  body { min-height: 2300px; }
  
  .content-header b,
  .admin-form .panel.heading-border:before,
  .admin-form .panel .heading-border:before {
    transition: all 0.7s ease;
  }
  /* responsive demo styles */
  @media (max-width: 800px) {
    .admin-form .panel-body { padding: 18px 12px; }
    .option-group .option { display: block; }
    .option-group .option + .option { margin-top: 8px; }
  }
  </style>
    

  <!-- BEGIN: PAGE SCRIPTS -->

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
    <!-- MaskedInput Plugin -->
  <script src="vendor/plugins/jquerymask/jquery.maskedinput.min.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  jQuery(document).ready(function() {

    "use strict";

    // Init Theme Core    
    Core.init();

    // Form Switcher
    $('#form-switcher > button').on('click', function() {
      var btnData = $(this).data('form-layout');
      var btnActive = $('#form-elements-pane .admin-form.active');

      // Remove any existing animations and then fade current form out
      btnActive.removeClass('slideInUp').addClass('animated fadeOutRight animated-shorter');
      // When above exit animation ends remove leftover classes and animate the new form in
      btnActive.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
        btnActive.removeClass('active fadeOutRight animated-shorter');
        $('#' + btnData).addClass('active animated slideInUp animated-shorter')
      });
    });

    // Cache several DOM elements
    var pageHeader = $('.content-header').find('b');
    var adminForm = $('.admin-form');
    var options = adminForm.find('.option');
    var switches = adminForm.find('.switch');
    var buttons = adminForm.find('.button');
    var Panel = adminForm.find('.panel');

    // Form Skin Switcher
    $('#skin-switcher a').on('click', function() {
      var btnData = $(this).data('form-skin');

      $('#skin-switcher a').removeClass('item-active');
      $(this).addClass('item-active')

      adminForm.each(function(i, e) {
        var skins = 'theme-primary theme-info theme-success theme-warning theme-danger theme-alert theme-system theme-dark';
        var panelSkins = 'panel-primary panel-info panel-success panel-warning panel-danger panel-alert panel-system panel-dark';
        $(e).removeClass(skins).addClass('theme-' + btnData);
        Panel.removeClass(panelSkins).addClass('panel-' + btnData);
        pageHeader.removeClass().addClass('text-' + btnData);
      });

      $(options).each(function(i, e) {
        if ($(e).hasClass('block')) {
          $(e).removeClass().addClass('block mt15 option option-' + btnData);
        } else {
          $(e).removeClass().addClass('option option-' + btnData);
        }
      });
      $(switches).each(function(i, ele) {
        if ($(ele).hasClass('switch-round')) {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-round switch-' + btnData);
          } else {
            $(ele).removeClass().addClass('switch switch-round switch-' + btnData);
          }
        } else {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-' + btnData);
          } else {
            $(ele).removeClass().addClass('switch switch-' + btnData);
          }
        }

      });
      buttons.removeClass().addClass('button btn-' + btnData);
    });

    setTimeout(function() {
      adminForm.addClass('theme-primary');
      Panel.addClass('panel-primary');
      pageHeader.addClass('text-primary');

      $(options).each(function(i, e) {
        if ($(e).hasClass('block')) {
          $(e).removeClass().addClass('block mt15 option option-primary');
        } else {
          $(e).removeClass().addClass('option option-primary');
        }
      });
      $(switches).each(function(i, ele) {

        if ($(ele).hasClass('switch-round')) {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-round switch-primary');
          } else {
            $(ele).removeClass().addClass('switch switch-round switch-primary');
          }
        } else {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-primary');
          } else {
            $(ele).removeClass().addClass('switch switch-primary');
          }
        }
      });
      buttons.removeClass().addClass('button btn-primary');
    }, 800);

  });



      // Init jQuery Time Spinner
    $("#spinner4").timespinner();

    // Init jQuery Masked inputs
    $('.date').mask('99/99/9999');
    $('.time').mask('99:99:99');
    $('.date_time').mask('99/99/9999 99:99:99');
    $('.zip').mask('99999-999');
    $('.phone').mask('(999) 999-9999');
    $('.phoneext').mask("(999) 999-9999 x99999");
    $(".money").mask("999,999,999.999");
    $(".product").mask("999.999.999.999");
    $(".tin").mask("99-9999999");
    $(".ssn").mask("999-99-9999");
    $(".ip").mask("9ZZ.9ZZ.9ZZ.9ZZ");
    $(".eyescript").mask("~9.99 ~9.99 999");
    $(".custom").mask("9.99.999.9999");
  </script>
  <!-- END: PAGE SCRIPTS -->

</body>

</html>
