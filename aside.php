<?php require_once 'header.php'; ?>
 <!-- Start: Sidebar -->
    <aside id="sidebar_left" class="nano nano-light sidebar-light affix">

      <!-- Start: Sidebar Left Content -->
      <div class="sidebar-left-content nano-content">

        <!-- Start: Sidebar Header -->
        <header class="sidebar-header">

          <!-- Sidebar Widget - Menu (slidedown) -->
          <div class="sidebar-widget menu-widget">
            <div class="row text-center mbn">
              <div class="col-xs-4">
                <a href="saida-materiais.php" class="text-primary" data-toggle="tooltip" data-placement="top" title="Dashboard">
                  <span class="glyphicon glyphicon-home"></span>
                </a>
              </div>
              <div class="col-xs-4">
                <a href="pages_messages.html" class="text-info" data-toggle="tooltip" data-placement="top" title="Messages">
                  <span class="glyphicon glyphicon-inbox"></span>
                </a>
              </div>
              <div class="col-xs-4">
                <a href="pages_profile.html" class="text-alert" data-toggle="tooltip" data-placement="top" title="Tasks">
                  <span class="glyphicon glyphicon-bell"></span>
                </a>
              </div>
              <div class="col-xs-4">
                <a href="pages_timeline.html" class="text-system" data-toggle="tooltip" data-placement="top" title="Activity">
                  <span class="fa fa-desktop"></span>
                </a>
              </div>
              <div class="col-xs-4">
                <a href="pages_profile.html" class="text-danger" data-toggle="tooltip" data-placement="top" title="Settings">
                  <span class="fa fa-gears"></span>
                </a>
              </div>
              <div class="col-xs-4">
                <a href="pages_gallery.html" class="text-warning" data-toggle="tooltip" data-placement="top" title="Cron Jobs">
                  <span class="fa fa-flask"></span>
                </a>
              </div>
            </div>
          </div>

          <!-- Sidebar Widget - Search (hidden) -->
          <div class="sidebar-widget search-widget hidden">
            <div class="input-group">
              <span class="input-group-addon">
                <i class="fa fa-search"></i>
              </span>
              <input type="text" id="sidebar-search" class="form-control" placeholder="Search...">
            </div>
          </div>

        </header>
        <!-- End: Sidebar Header -->

        <!-- Start: Sidebar Menu -->
        <ul class="nav sidebar-menu">
          <li class="sidebar-label pt36">Menu</li>
          <li>
            <a class="accordion-toggle" href="#">
              <span class="glyphicon glyphicon-plus"></span>
              <span class="sidebar-title">Cadastros</span>
              <span class="caret"></span>
            </a>
            <ul class="nav sub-nav">
              <li>
                <a href="pacientes.php">
                  <span class="glyphicon glyphicon-book"></span>Pacientes</a>
              </li>
            </ul>
          </li>
          <?php if($_SESSION['tipo'] != 1): ?>
          <li>
            <a class="accordion-toggle " href="#">
              <span class="imoon imoon-checkmark2"></span>
              <span class="sidebar-title">Procedimentos</span>
              <span class="caret"></span>
            </a>
            <ul class="nav sub-nav">
              
              <li>
                <a href="pesquisar-pacientes-recepcao.php">
                  <span class="imoon imoon-arrow-right"></span>Recepcao</a>
              </li>
              
              <li>
                <a href="saida-materiais.php">
                  <span class="imoon imoon-arrow-left"></span>Saída de materiais</a>
              </li>
            </ul>
          </li>
          <?php endif;?>
          <?php if($_SESSION['tipo'] != 1): ?>
          <li>
            <a class="accordion-toggle " href="#">
              <span class="glyphicon glyphicon-list-alt"></span>
              <span class="sidebar-title">Consultas</span>
              <span class="caret"></span>
            </a>
            <ul class="nav sub-nav">
              
              <li>
                <a href="consulta_saldo.php">
                  <span class="imoon imoon-numbered-list"></span>Saldo Materiais</a>
              </li>
            </ul>
          </li>
          <?php endif;?>
          <?php if($_SESSION['tipo'] != 1): ?>
          <li>
            <a class="accordion-toggle" href="#">
              <span class="fa fa-columns"></span>
              <span class="sidebar-title">Relatórios</span>
              <span class="caret"></span>
            </a>
            <ul class="nav sub-nav">
              
              <li>
                <a href="relatorio_fisico.php" >
                  <span class="fa fa fa-arrows-h"></span>
                  Físico
                </a>
              </li>
              
              <li>
                <a href="relatorio_financeiro.php" >
                  <span class="fa fa fa-arrows-h"></span>
                  Financeiro
                </a>
              </li>
              <li>
                <a href="relatorio_inventario.php">
                  <span class="fa fa fa-arrows-h"></span>
                  Físico - Financeiro
                </a>
              </li>
              <li>
                <a href="relatorio_ponto_pedido.php">
                  <span class="fa fa fa-arrows-h"></span>
                  Ponto de pedido
                </a>
              </li>
              <li>
                <a href="relatorio_total_entradas_saidas.php">
                  <span class="fa fa fa-arrows-h"></span>
                  Total de entradas e saídas <br><p style="padding-left: 25px"> por produto</p> 
                </a>
              </li>
              <li>
                <a href="relatorio_entradas_fornecedor.php">
                  <span class="fa fa fa-arrows-h"></span>
                  Entradas por fornecedor
                </a>
              </li>
              <li>
                <a href="relatorio_saidas_setor.php">
                  <span class="fa fa fa-arrows-h"></span>
                  Saídas por setor
                </a>
              </li>
              <li>
                <a href="relatorio_entradas_materiais_impressao.php">
                  <span class="fa fa fa-arrows-h"></span>
                  Entradas de materiais <p style="padding-left: 25px;">(Impressão)</p>
                </a>
              </li>
            </ul>
          </li>
              <!--
              <li>
                <a class="accordion-toggle" href="#">
                  <span class="fa fa-bullhorn"></span>
                  <span class="sidebar-title">Suporte</span>
                  <span class="caret"></span>
                </a>
                <ul class="nav sub-nav">
              <li>
                <a href="chamados.php" >
                  <span class="fa fa-ticket"></span>
                  Abrir chamados
                </a>
              </li>

                </ul>
              </li>
            -->
            </ul>
          </li>
          <?php endif;?>
        </ul>
        <!-- End: Sidebar Menu -->
      </div>
      <!-- End: Sidebar Left Content -->

    </aside>
    <!-- End: Sidebar -->

  
</html>
