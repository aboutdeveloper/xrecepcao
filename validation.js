			
			// VALIDACAO DO FORM DE CADASTRO DE USUARIO =================================================================================>>>>
			$(document).ready(function(){
				
				var frmusuarios = $("#frmusuarios");
				
				var sobrenome = $("#sobrenome");
				var sobrenomeInfo = $("#sobrenome_info");
				
				var nome = $("#nome");
				var nomeInfo = $("#nome_info");
				                             
				var chks = document.getElementsByName("sexo");
				var hasChecked = false;
				var sexoInfo = $("#sexo_info");
                                
				var email = $("#email");
				var emailInfo = $("#email_info");
                                
                                var cpf = $("#cpf");
				var cpfInfo = $("#cpf_info");
								
				var estado = $("#cod_estados");
				var estadoInfo = $("#estado_info");
				
				var cidade = $("#cod_cidades");
				var cidadeInfo = $("#cidade_info");
				
				var endereco = $("#endereco");
				var enderecoInfo = $("#endereco_info");
				
				var bairro = $("#bairro");
				var bairroInfo = $("#bairro_info");
				
				var telefone = $("#telefone");
				var telefoneInfo = $("#telefone_info");
				
				var departamento = $("#departamento");
				var departamentoInfo = $("#departamento_info");
				
				var cargoFuncao = $("#cargo_funcao");
				var cargoFuncaoInfo = $("#cargo_funcao_info");
				
				var categoriaUsuario = $("#categoria_usuario");
				var categoriaUsuarioInfo = $("#categoria_usuario_info");
				
				var usuario = $("#usuario");
				var usuarioInfo = $("#usuario_info");
				
				var pass1 = $("#senha");
				var pass1Info = $("#senha_info");
				
				var pass2 = $("#senha2");
				var pass2Info = $("#senha2_info");
								
				nome.blur(validateNome);
				sobrenome.blur(validateSobrenome);
                                cpf.blur(validateCpf);
				email.blur(validateEmail);
				estado.blur(validateEstado);
				cidade.blur(validateCidade);
				endereco.blur(validateEndereco);
				bairro.blur(validateBairro);
				telefone.blur(validateTelefone);
				departamento.blur(validateDepartamento);
				cargoFuncao.blur(validateCargoFuncao);
				categoriaUsuario.blur(validateCategoriaUsuario);
				usuario.blur(validateUsuario);
				pass1.blur(validatePass1);
				pass2.blur(validatePass2);
				
				nome.keyup(validateNome);
				sobrenome.keyup(validateSobrenome);
				email.keyup(validateEmail);
                                cpf.keyup(validateCpf);
				estado.keyup(validateEstado);
				cidade.keyup(validateCidade);
				endereco.keyup(validateEndereco);
				bairro.keyup(validateBairro);
				telefone.keyup(validateTelefone);
				departamento.keyup(validateDepartamento);
				cargoFuncao.keyup(validateCargoFuncao);
				categoriaUsuario.keyup(validateCategoriaUsuario);
				usuario.keyup(validateUsuario);
				pass1.keyup(validatePass1);
				pass2.keyup(validatePass2);

				frmusuarios.submit(function(){
					if(validateNome() & validateCpf() & validateSexo() & validateSobrenome() & validateEmail() & validateDepartamento() & validateCargoFuncao() & validateCategoriaUsuario() & validateUsuario() & validatePass1() & validatePass2() & validateEstado() & validateCidade() & validateEndereco() & validateBairro() & validateTelefone())
						return true
					else
						return false
				});
                                
                                
				function validateSexo(){
                                        for(var i = 0; i < chks.length; i++){ 
						if (chks[i].checked){ 
							hasChecked = true; 
							break; 
						} 
					}
                                        
					if(hasChecked == false){
						sexoInfo.text("Marque uma opção");
						sexoInfo.addClass("error");
						return false;
					}else{
						sexoInfo.text("ok");
						sexoInfo.removeClass("error");
						return true;
					}
				}

                                function validateCpf(){
					if(cpf.val().length < 5){
						cpf.addClass("error");
						cpfInfo.text("Digite seu CPF.");
						cpfInfo.addClass("error");
						return false;
					}else{
						cpf.removeClass("error");
						cpfInfo.text("ok");
						cpfInfo.removeClass("error");
						return true;
					}
				}

				function validateSobrenome(){
					if(sobrenome.val().length < 3){
						sobrenome.addClass("error");
						sobrenomeInfo.text("Digite seu Sobrenome (Mínimo de caracteres 3)");
						sobrenomeInfo.addClass("error");
						return false;
					}else{
						sobrenome.removeClass("error");
						sobrenomeInfo.text("ok");
						sobrenomeInfo.removeClass("error");
						return true;
					}
				}
				
				function validateNome(){
					if(nome.val().length < 5){
						nome.addClass("error");
						nomeInfo.text("Digite seu nome (Mínimo de caracteres 5)");
						nomeInfo.addClass("error");
						return false;
					}else{
						nome.removeClass("error");
						nomeInfo.text("ok");
						nomeInfo.removeClass("error");
						return true;
					}
				}
				
				function validateEstado(){
					if(estado.val() == 0){
						estado.addClass("error");
						estadoInfo.text("Selecione um Estado");
						estadoInfo.addClass("error");
						return false;
					}else{
						estado.removeClass("error");
						estadoInfo.text("ok");
						estadoInfo.removeClass("error");
						return true;
					}
				}
				
				function validateCidade(){
					if(cidade.val() == 0){
						cidade.addClass("error");
						cidadeInfo.text("Selecione uma Cidade");
						cidadeInfo.addClass("error");
						return false;
					}else{
						cidade.removeClass("error");
						cidadeInfo.text("ok");
						cidadeInfo.removeClass("error");
						return true;
					}
				}
				
				function validateEndereco(){
					if(endereco.val().length < 5){
						endereco.addClass("error");
						enderecoInfo.text("Digite seu Endereço (Mínimo de caracteres 5)");
						enderecoInfo.addClass("error");
						return false;
					}else{
						endereco.removeClass("error");
						enderecoInfo.text("ok");
						enderecoInfo.removeClass("error");
						return true;
					}
				}
				
				function validateBairro(){
					if(bairro.val().length < 3){
						bairro.addClass("error");
						bairroInfo.text("Digite seu Bairro (Mínimo de caracteres 3)");
						bairroInfo.addClass("error");
						return false;
					}else{
						bairro.removeClass("error");
						bairroInfo.text("ok");
						bairroInfo.removeClass("error");
						return true;
					}
				}
				
				function validateTelefone(){
					if(telefone.val().length < 5){
						telefone.addClass("error");
						telefoneInfo.text("Digite seu Telefone");
						telefoneInfo.addClass("error");
						return false;
					}else{
						telefone.removeClass("error");
						telefoneInfo.text("ok");
						telefoneInfo.removeClass("error");
						return true;
					}
				}
                                				
				function validateEmail(){
					var a = $("#email").val();
					var filter = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
					if(filter.test(a)){
						email.removeClass("error");
						emailInfo.text("ok");
						emailInfo.removeClass("error");
						return true;
					}else{
						email.addClass("error");
						emailInfo.text("Digite um E-mail válido!");
						emailInfo.addClass("error");
						return false;
					}
				}
				
				function validateDepartamento(){
					if(departamento.val().length < 5){
						departamento.addClass("error");
						departamentoInfo.text("Digite um Departamento (Mínimo de caracteres 5)");
						departamentoInfo.addClass("error");
						return false;
					}else{
						departamento.removeClass("error");
						departamentoInfo.text("ok");
						departamentoInfo.removeClass("error");
						return true;
					}
				}
				
				function validateCargoFuncao(){
					if(cargoFuncao.val().length < 5){
						cargoFuncao.addClass("error");
						cargoFuncaoInfo.text("Digite seu Cargo/Função (Mínino de caracteres 5)");
						cargoFuncaoInfo.addClass("error");
						return false;
					}else{
						cargoFuncao.removeClass("error");
						cargoFuncaoInfo.text("ok");
						cargoFuncaoInfo.removeClass("error");
						return true;
					}
				}
				
				function validateCategoriaUsuario(){
					if(categoriaUsuario.val()==0){
						categoriaUsuario.addClass("error");
						categoriaUsuarioInfo.text("Selecione uma Categoria de Usuário");
						categoriaUsuarioInfo.addClass("error");
						return false;
					}else{
						categoriaUsuario.removeClass("error");
						categoriaUsuarioInfo.text("ok");
						categoriaUsuarioInfo.removeClass("error");
						return true;
					}
				}

				function validateUsuario(){
					if(usuario.val().length < 5){
						usuario.addClass("error");
						usuarioInfo.text("Digite um nome de Usuário (Mínino de caracteres 5)");
						usuarioInfo.addClass("error");
						return false;
					}else{
						usuario.removeClass("error");
						usuarioInfo.text("ok");
						usuarioInfo.removeClass("error");
						return true;
					}
				}
				
				function validatePass1(){
					var a = $("#password1");
					var b = $("#password2");
			
					//it's NOT valid
					if(pass1.val().length <5){
						pass1.addClass("error");
						pass1Info.text("Digite pelo menos 5 caracteres: letras, números e '_'");
						pass1Info.addClass("error");
						return false;
					}
					//it's valid
					else{			
						pass1.removeClass("error");
						pass1Info.text("ok");
						pass1Info.removeClass("error");
						validatePass2();
						return true;
					}
				}
				
				function validatePass2(){
					var a = $("#password1");
					var b = $("#password2");
					//are NOT valid
					if( pass1.val() != pass2.val() ){
						pass2.addClass("error");
						pass2Info.text("Suas senhas não correspondem. Tente novamente.");
						pass2Info.addClass("error");
						return false;
					}
					//are valid
					else{
						pass2.removeClass("error");
						pass2Info.text("Repita sua senha");
						pass2Info.removeClass("error");
						return true;
					}
				}

			});
			
			// VALIDACAO DO FORM DE ALTERACAO DE USUARIO =================================================================================>>>>
			$(document).ready(function(){
				
				var frmusuarios2 = $("#frmusuariosupdate2");
				
				var nome = $("#nome");
				var nomeInfo = $("#nome_info");
				
				var cpf = $("#cpf");
				var cpfInfo = $("#cpf_info");
				
                                var chks = document.getElementsByName("sexo");
				var hasChecked = false;
				var sexoInfo = $("#sexo_info");
                                
				var sobrenome = $("#sobrenome");
				var sobrenomeInfo = $("#sobrenome_info");
				
				var email = $("#email");
				var emailInfo = $("#email_info");
				
				var endereco = $("#endereco");
				var enderecoInfo = $("#endereco_info");
				
				var bairro = $("#bairro");
				var bairroInfo = $("#bairro_info");
				
				var telefone = $("#telefone");
				var telefoneInfo = $("#telefone_info");
				
				var usuarioatualautenticacao = $("#usuario_atual_autenticacao");
				var usuarioatualautenticacaoInfo = $("#usuario_atual_autenticacao_info");
				
				var senhaatualautenticacao = $("#senha_atual_autenticacao");
				var senhaatualautenticacaoInfo = $("#senha_atual_autenticacao_info");

				nome.blur(validateNome);
				cpf.blur(validateCpf);
				sobrenome.blur(validateSobrenome);
				email.blur(validateEmail);
				endereco.blur(validateEndereco);
				bairro.blur(validateBairro);
				telefone.blur(validateTelefone);
				usuarioatualautenticacao.blur(validateUsuarioatualautenticacao);
				senhaatualautenticacao.blur(validateSenhaatualautenticacao);
				
				nome.keyup(validateNome);
				cpf.keyup(validateCpf);
				sobrenome.keyup(validateSobrenome);
				email.keyup(validateEmail);
				endereco.keyup(validateEndereco);
				bairro.keyup(validateBairro);
				telefone.keyup(validateTelefone);
				usuarioatualautenticacao.keyup(validateUsuarioatualautenticacao);
				senhaatualautenticacao.keyup(validateSenhaatualautenticacao);

				frmusuarios2.submit(function(){
					if(validateNome() & validateCpf() & validateSobrenome() & validateSexo() & validateEmail() & validateEndereco() & validateBairro() & validateTelefone() & validateUsuarioatualautenticacao() & validateSenhaatualautenticacao())
						return true
					else
						return false
				});
                                
                                function validateSexo(){
                                        for(var i = 0; i < chks.length; i++){ 
						if (chks[i].checked){ 
							hasChecked = true; 
							break; 
						} 
					}
					if(hasChecked == false){
						sexoInfo.text("Marque uma opção");
						sexoInfo.addClass("error");
						return false;
					}else{
						sexoInfo.text("ok");
						sexoInfo.removeClass("error");
						return true;
					}
				}
				
				function validateCpf(){
					if(cpf.val().length < 5){
						cpf.addClass("error");
						cpfInfo.text("Digite seu CPF.");
						cpfInfo.addClass("error");
						return false;
					}else{
						cpf.removeClass("error");
						cpfInfo.text("ok");
						cpfInfo.removeClass("error");
						return true;
					}
				}

				function validateSobrenome(){
					if(sobrenome.val().length < 3){
						sobrenome.addClass("error");
						sobrenomeInfo.text("Digite seu Sobrenome (Mínimo de caracteres 3)");
						sobrenomeInfo.addClass("error");
						return false;
					}else{
						sobrenome.removeClass("error");
						sobrenomeInfo.text("ok");
						sobrenomeInfo.removeClass("error");
						return true;
					}
				}
				
				function validateNome(){
					if(nome.val().length < 5){
						nome.addClass("error");
						nomeInfo.text("Digite seu nome (Mínimo de caracteres 5)");
						nomeInfo.addClass("error");
						return false;
					}else{
						nome.removeClass("error");
						nomeInfo.text("ok");
						nomeInfo.removeClass("error");
						return true;
					}
				}
				
				function validateEndereco(){
					if(endereco.val().length < 5){
						endereco.addClass("error");
						enderecoInfo.text("Digite seu Endereço (Mínimo de caracteres 5)");
						enderecoInfo.addClass("error");
						return false;
					}else{
						endereco.removeClass("error");
						enderecoInfo.text("ok");
						enderecoInfo.removeClass("error");
						return true;
					}
				}
				
				function validateBairro(){
					if(bairro.val().length < 3){
						bairro.addClass("error");
						bairroInfo.text("Digite seu Bairro (Mínimo de caracteres 3)");
						bairroInfo.addClass("error");
						return false;
					}else{
						bairro.removeClass("error");
						bairroInfo.text("ok");
						bairroInfo.removeClass("error");
						return true;
					}
				}
				
				function validateTelefone(){
					if(telefone.val().length < 5){
						telefone.addClass("error");
						telefoneInfo.text("Digite seu Telefone");
						telefoneInfo.addClass("error");
						return false;
					}else{
						telefone.removeClass("error");
						telefoneInfo.text("ok");
						telefoneInfo.removeClass("error");
						return true;
					}
				}
				
				function validateEmail(){
					var a = $("#email").val();
					var filter = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
					if(filter.test(a)){
						email.removeClass("error");
						emailInfo.text("ok");
						emailInfo.removeClass("error");
						return true;
					}else{
						email.addClass("error");
						emailInfo.text("Digite seu E-mail corretamente!");
						emailInfo.addClass("error");
						return false;
					}
				}

				function validateUsuarioatualautenticacao(){
					if(usuarioatualautenticacao.val().length < 3){
						usuarioatualautenticacao.addClass("error");
						usuarioatualautenticacaoInfo.text("Campo necessário!");
						usuarioatualautenticacaoInfo.addClass("error");
						return false;
					}else{
						usuarioatualautenticacao.removeClass("error");
						usuarioatualautenticacaoInfo.text("ok");
						usuarioatualautenticacaoInfo.removeClass("error");
						return true;
					}
				}
				
				function validateSenhaatualautenticacao(){
					if(senhaatualautenticacao.val().length < 3){
						senhaatualautenticacao.addClass("error");
						senhaatualautenticacaoInfo.text("Campo necessário!");
						senhaatualautenticacaoInfo.addClass("error");
						return false;
					}else{
						senhaatualautenticacao.removeClass("error");
						senhaatualautenticacaoInfo.text("ok");
						senhaatualautenticacaoInfo.removeClass("error");
						return true;
					}
				}

			});
			
			// VALIDACAO DO FORM DE ALTERACAO DE USUARIO =================================================================================>>>>
			$(document).ready(function(){
				
				var frmusuarios2 = $("#frmusuariosupdate");
				
				var nome = $("#nome");
				var nomeInfo = $("#nome_info");
				
				var sobrenome = $("#sobrenome");
				var sobrenomeInfo = $("#sobrenome_info");
				
                                var cpf = $("#cpf");
				var cpfInfo = $("#cpf_info");
                                
				var email = $("#email");
				var emailInfo = $("#email_info");
				                               
				var chks = document.getElementsByName("sexo");
				var hasChecked = false;
				var sexoInfo = $("#sexo_info");
                                
				var endereco = $("#endereco");
				var enderecoInfo = $("#endereco_info");
				
				var bairro = $("#bairro");
				var bairroInfo = $("#bairro_info");
				
				var telefone = $("#telefone");
				var telefoneInfo = $("#telefone_info");
				
				var departamento = $("#departamento");
				var departamentoInfo = $("#departamento_info");
				
				var cargoFuncao = $("#cargo_funcao");
				var cargoFuncaoInfo = $("#cargo_funcao_info");
				
				var categoriaUsuario = $("#categoria_usuario");
				var categoriaUsuarioInfo = $("#categoria_usuario_info");
				
				var usuarioatualautenticacao = $("#usuario_atual_autenticacao");
				var usuarioatualautenticacaoInfo = $("#usuario_atual_autenticacao_info");
				
				var senhaatualautenticacao = $("#senha_atual_autenticacao");
				var senhaatualautenticacaoInfo = $("#senha_atual_autenticacao_info");

				nome.blur(validateNome);
				sobrenome.blur(validateSobrenome);
				email.blur(validateEmail);
				cpf.blur(validateCpf);
				endereco.blur(validateEndereco);
				bairro.blur(validateBairro);
				telefone.blur(validateTelefone);
				departamento.blur(validateDepartamento);
				cargoFuncao.blur(validateCargoFuncao);
				categoriaUsuario.blur(validateCategoriaUsuario);
				usuarioatualautenticacao.blur(validateUsuarioatualautenticacao);
				senhaatualautenticacao.blur(validateSenhaatualautenticacao);
				
				nome.keyup(validateNome);
				sobrenome.keyup(validateSobrenome);
				email.keyup(validateEmail);
                                cpf.keyup(validateCpf);
				endereco.keyup(validateEndereco);
				bairro.keyup(validateBairro);
				telefone.keyup(validateTelefone);
				departamento.keyup(validateDepartamento);
				cargoFuncao.keyup(validateCargoFuncao);
				categoriaUsuario.keyup(validateCategoriaUsuario);
				usuarioatualautenticacao.keyup(validateUsuarioatualautenticacao);
				senhaatualautenticacao.keyup(validateSenhaatualautenticacao);

				frmusuarios2.submit(function(){
					if(validateNome() & validateSobrenome() & validateSexo() & validateCpf() & validateEmail() & validateDepartamento() & validateCargoFuncao() & validateCategoriaUsuario() & validateEndereco() & validateBairro() & validateTelefone() & validateUsuarioatualautenticacao() & validateSenhaatualautenticacao())
						return true
					else
						return false
				});
                                                                         
				function validateSexo(){
                                        for(var i = 0; i < chks.length; i++){ 
						if (chks[i].checked){ 
							hasChecked = true; 
							break; 
						} 
					}
                                        
					if(hasChecked == false){
						sexoInfo.text("Marque uma opção");
						sexoInfo.addClass("error");
						return false;
					}else{
						sexoInfo.text("ok");
						sexoInfo.removeClass("error");
						return true;
					}
				}
                                
                                function validateCpf(){
					if(cpf.val().length < 5){
						cpf.addClass("error");
						cpfInfo.text("Digite seu CPF.");
						cpfInfo.addClass("error");
						return false;
					}else{
						cpf.removeClass("error");
						cpfInfo.text("ok");
						cpfInfo.removeClass("error");
						return true;
					}
				}
                                
				function validateSobrenome(){
					if(sobrenome.val().length < 3){
						sobrenome.addClass("error");
						sobrenomeInfo.text("Digite seu Sobrenome (Mínimo de caracteres 3)");
						sobrenomeInfo.addClass("error");
						return false;
					}else{
						sobrenome.removeClass("error");
						sobrenomeInfo.text("ok");
						sobrenomeInfo.removeClass("error");
						return true;
					}
				}
				
				function validateNome(){
					if(nome.val().length < 5){
						nome.addClass("error");
						nomeInfo.text("Digite seu nome (Mínimo de caracteres 5)");
						nomeInfo.addClass("error");
						return false;
					}else{
						nome.removeClass("error");
						nomeInfo.text("ok");
						nomeInfo.removeClass("error");
						return true;
					}
				}
				
				function validateEndereco(){
					if(endereco.val().length < 5){
						endereco.addClass("error");
						enderecoInfo.text("Digite seu Endereço (Mínimo de caracteres 5)");
						enderecoInfo.addClass("error");
						return false;
					}else{
						endereco.removeClass("error");
						enderecoInfo.text("ok");
						enderecoInfo.removeClass("error");
						return true;
					}
				}
				
				function validateBairro(){
					if(bairro.val().length < 3){
						bairro.addClass("error");
						bairroInfo.text("Digite seu Bairro (Mínimo de caracteres 3)");
						bairroInfo.addClass("error");
						return false;
					}else{
						bairro.removeClass("error");
						bairroInfo.text("ok");
						bairroInfo.removeClass("error");
						return true;
					}
				}
				
				function validateTelefone(){
					if(telefone.val().length < 5){
						telefone.addClass("error");
						telefoneInfo.text("Digite seu Telefone");
						telefoneInfo.addClass("error");
						return false;
					}else{
						telefone.removeClass("error");
						telefoneInfo.text("ok");
						telefoneInfo.removeClass("error");
						return true;
					}
				}
				
				function validateEmail(){
					var a = $("#email").val();
					var filter = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
					if(filter.test(a)){
						email.removeClass("error");
						emailInfo.text("ok");
						emailInfo.removeClass("error");
						return true;
					}else{
						email.addClass("error");
						emailInfo.text("Digite seu E-mail corretamente!");
						emailInfo.addClass("error");
						return false;
					}
				}
				
				function validateDepartamento(){
					if(departamento.val().length < 5){
						departamento.addClass("error");
						departamentoInfo.text("Digite um Departamento (Mínimo de caracteres 5)");
						departamentoInfo.addClass("error");
						return false;
					}else{
						departamento.removeClass("error");
						departamentoInfo.text("ok");
						departamentoInfo.removeClass("error");
						return true;
					}
				}
				
				function validateCargoFuncao(){
					if(cargoFuncao.val().length < 5){
						cargoFuncao.addClass("error");
						cargoFuncaoInfo.text("Digite seu Cargo/Função (Mínino de caracteres 5)");
						cargoFuncaoInfo.addClass("error");
						return false;
					}else{
						cargoFuncao.removeClass("error");
						cargoFuncaoInfo.text("ok");
						cargoFuncaoInfo.removeClass("error");
						return true;
					}
				}
				
				function validateCategoriaUsuario(){
					if(categoriaUsuario.val()==0){
						categoriaUsuario.addClass("error");
						categoriaUsuarioInfo.text("Selecione uma Categoria de Usuário");
						categoriaUsuarioInfo.addClass("error");
						return false;
					}else{
						categoriaUsuario.removeClass("error");
						categoriaUsuarioInfo.text("ok");
						categoriaUsuarioInfo.removeClass("error");
						return true;
					}
				}

				function validateUsuarioatualautenticacao(){
					if(usuarioatualautenticacao.val().length < 3){
						usuarioatualautenticacao.addClass("error");
						usuarioatualautenticacaoInfo.text("Campo necessário!");
						usuarioatualautenticacaoInfo.addClass("error");
						return false;
					}else{
						usuarioatualautenticacao.removeClass("error");
						usuarioatualautenticacaoInfo.text("ok");
						usuarioatualautenticacaoInfo.removeClass("error");
						return true;
					}
				}
				
				function validateSenhaatualautenticacao(){
					if(senhaatualautenticacao.val().length < 3){
						senhaatualautenticacao.addClass("error");
						senhaatualautenticacaoInfo.text("Campo necessário!");
						senhaatualautenticacaoInfo.addClass("error");
						return false;
					}else{
						senhaatualautenticacao.removeClass("error");
						senhaatualautenticacaoInfo.text("ok");
						senhaatualautenticacaoInfo.removeClass("error");
						return true;
					}
				}

			});
			
			
			// VALIDACAO DO FORM DE CADASTRO DE ALVARA====================================================================================>>>
			$(document).ready(function(){
				// VARIAVEIS GLOBAIS
				var frmalvara = $("#frmalvara");
				// NOME DO PROPRIETARIO
				var nomeproprietario = $("#nome_proprietario");
				var nomeproprietarioInfo = $("#nome_proprietario_info");
				// PESSOA FISICA OU JURIDICA
				var chks = document.getElementsByName("pessoa");
				var hasChecked = false;
				var pessoaInfo = $("#pessoa_info");
				// AUTORIA
				var autoria = $("#autoria");
				var autoriaInfo = $("#autoria_info");
				// TIPO PROFISSIONAL - AUTORIA
				var chks2 = document.getElementsByName('tipo_profissional_autoria'); 
				var hasChecked2 = false; 
				var tipoprofissionalInfo = $("#tipo_profissional_autoria_info");
				// ENDERECO
				var endereco = $("#endereco");
				var enderecoInfo = $("#endereco_info");
				// BAIRRO
				var bairro = $("#bairro");
				var bairroInfo = $("#bairro_info");
				// FINALIDADE DE OBRA
				var finalidadeobra = $("#finalidade_obra");
				var finalidadeobraInfo = $("#finalidade_obra_info");
				// SITUACAO DO TERRENO
				var chks3 = document.getElementsByName('situacaoterreno'); 
				var hasChecked3 = false; 
				var situacaoTerrenoInfo = $("#situacao_terreno_info");
				// AREA DO TERRENO
				var areaterreno = $('#area_terreno');
				var areaterrenoInfo = $('#area_terreno_info');
				
				/*
					// PAVIMENTOS
					var pavimentos = $("#pavimentos");
					var pavimentosInfo = $("#pavimentos_info");
				
					// SITUACAO DO TERRENO
					var meioquadra = $("#meio_quadra");
					var esquina = $("#esquina");
					var situacaoterrenoInfo = $("#situacao_terreno_info");
					
					var chks3 = document.getElementsByName('resp_open'); 
					var hasChecked3 = false; 				
	
					var crearesponsaveltecnico = $("#crea_responsavel_tecnico");
					var crearesponsaveltecnicoInfo = $("#crea_responsavel_tecnico_info");
					
					var cauresponsaveltecnico = $("#cau_responsavel_tecnico");
					var cauresponsaveltecnicoInfo = $("#cau_responsavel_tecnico_info");
	
					var quadra = $("#quadra");
					var lote = $("#lote");
					var quadraloteInfo = $("#quadra_lote_info");
					
					var numeroalvara = $("#numero_alvara");
					var numeroalvaraInfo = $("#numero_alvara_info");
				*/				
				nomeproprietario.blur(validateNomeproprietario);
				autoria.blur(validateAutoria);
				endereco.blur(validateEndereco);
				bairro.blur(validateBairro);
				finalidadeobra.blur(validateFinalidadeobra);
				areaterreno.blur(validateAreaterreno);
				//pavimentos.blur(validatePavimentos);
				//responsaveltecnico.blur(ValidateResponsavelTecnico);
				//quadra.blur(validateQuadraLote);
				//lote.blur(validateQuadraLote);
				//numeroalvara.blur(validateNumeroalvara);
				nomeproprietario.keyup(validateNomeproprietario);
				autoria.keyup(validateAutoria);
				endereco.keyup(validateEndereco);
				bairro.keyup(validateBairro);
				finalidadeobra.keyup(validateFinalidadeobra);
				areaterreno.keyup(validateAreaterreno);
				//pavimentos.keyup(validatePavimentos);
				//responsaveltecnico.keyup(ValidateResponsavelTecnico);
				//quadra.keyup(validateQuadraLote);
				//lote.keyup(validateQuadraLote);
				//numeroalvara.keyup(validateNumeroalvara);
				frmalvara.submit(function(){
					if(validateNomeproprietario() & validatePessoa() & validateAutoria() & validateTipoprofissional() & validateEndereco() & validateBairro() & validateFinalidadeobra() & validateSituacaoTerreno() & validateAreaterreno())
						return true
					else
						return false
				});
				// PROPRIETARIO
				function validateNomeproprietario(){
					if(nomeproprietario.val().length < 5){
						nomeproprietario.addClass("error");
						nomeproprietarioInfo.text("Digite o nome do proprietário (mínino de caracteres 5)");
						nomeproprietarioInfo.addClass("error");
						return false;
					}else{
						nomeproprietario.removeClass("error");
						nomeproprietarioInfo.text("ok");
						nomeproprietarioInfo.removeClass("error");
						return true;
					}
				}
				// PESSOA FISICA OU JURIDICA DO PROPRIETARIO
				function validatePessoa(){
					for(var i = 0; i < chks.length; i++){ 
						if (chks[i].checked){ 
							hasChecked = true; 
							break; 
						} 
					} 
					if(hasChecked == false){
						pessoaInfo.text("Marque uma opção");
						pessoaInfo.addClass("error");
						return false;
					}else{
						pessoaInfo.text("ok");
						pessoaInfo.removeClass("error");
						return true;
					}
				}
				// AUTORIA 
				function validateAutoria(){
					if(autoria.val().length < 5){
						autoria.addClass("error");
						autoriaInfo.text("Digite o Autor (Mínino de caracteres 5)");
						autoriaInfo.addClass("error");
						return false;
					}else{
						autoria.removeClass("error");
						autoriaInfo.text("ok");
						autoriaInfo.removeClass("error");
						return true;
					}
				}
				// ENGENHEIRO OU ARQUITETO - AUTORIA
				function validateTipoprofissional(){
					for(var i = 0; i < chks2.length; i++){ 
						if (chks2[i].checked){ 
							hasChecked2 = true; 
							break; 
						} 
					} 
					if(hasChecked2 == false){
						tipoprofissionalInfo.text("Marque uma opção");
						tipoprofissionalInfo.addClass("error");
						return false;
					}else{
						tipoprofissionalInfo.text("ok");
						tipoprofissionalInfo.removeClass("error");
						return true;
					}
				}
				// ENDERECO
				function validateEndereco(){
					if(endereco.val().length < 5){
						endereco.addClass("error");
						enderecoInfo.text("Digite o Endereço (Mínino de caracteres 5)");
						enderecoInfo.addClass("error");
						return false;
					}else{
						endereco.removeClass("error");
						enderecoInfo.text("ok");
						enderecoInfo.removeClass("error");
						return true;
					}
				}
				// BAIRRO
				function validateBairro(){
					if(bairro.val().length < 5){
						bairro.addClass("error");
						bairroInfo.text("Digite o Bairro (Mínino de caracteres 5)");
						bairroInfo.addClass("error");
						return false;
					}else{
						bairro.removeClass("error");
						bairroInfo.text("ok");
						bairroInfo.removeClass("error");
						return true;
					}
				}
				// FINALIDADE DA OBRA
				function validateFinalidadeobra(){
					if(finalidadeobra.val()==0){
						finalidadeobra.addClass("error");
						finalidadeobraInfo.text("Selecione uma Finalidade de Obra");
						finalidadeobraInfo.addClass("error");
						return false;
					}else{
						finalidadeobra.removeClass("error");
						finalidadeobraInfo.text("ok");
						finalidadeobraInfo.removeClass("error");
						return true;
					}
				}	
				// SITUACAO DO TERRENO
				function validateSituacaoTerreno(){
					for(var i = 0; i < chks3.length; i++){ 
						if (chks3[i].checked){ 
							hasChecked3 = true; 
							break; 
						} 
					} 
					if(hasChecked3 == false){
						situacaoTerrenoInfo.text("Marque uma opção");
						situacaoTerrenoInfo.addClass("error");
						return false;
					}else{
						situacaoTerrenoInfo.text("ok");
						situacaoTerrenoInfo.removeClass("error");
						return true;
					}
				}
				// AREA DO TERRENO
				function validateAreaterreno(){
					if(areaterreno.val().length < 1){
						areaterreno.addClass("error");
						areaterrenoInfo.text("Digite a área do terreno");
						areaterrenoInfo.addClass("error");
						return false;
					}else{
						areaterreno.removeClass("error");
						areaterrenoInfo.text("ok");
						areaterrenoInfo.removeClass("error");
						return true;
					}
				}
				/*
				 * 
				 				
				  // AREA DE PAVIMENTO TERREO
					function validatePavimentos(){
						if(pavimentos.val().length < 1){
							pavimentos.addClass("error");
							pavimentosInfo.text("Digite um valor para a área do Pavimento Térreo");
							pavimentosInfo.addClass("error");
							return false;
						}else{
							pavimentos.removeClass("error");
							pavimentosInfo.text("ok");
							pavimentosInfo.removeClass("error");
							return true;
						}
					}
					// PESSOA FISICA OU JURIDICA DO PROPRIETARIO
					function respOpen(){
						for(var i = 0; i < chks3.length; i++){ 
							if (chks3[i].checked){ 
								hasChecked3 = true; 
								break; 
							} 
						} 
						if(hasChecked3 == false){
							return true;
						}else{
							
							// RESPONSAVEL TECNICO SE MARCADO
							function ValidateResponsavelTecnico(){
								if(responsaveltecnico.val().length < 5){
									responsaveltecnico.addClass("error");
									responsaveltecnicoInfo.text("Digite o nome do Responsável Técnico (mínino de caracteres 4)");
									responsaveltecnicoInfo.addClass("error");
									return false;
								}else{
									responsaveltecnico.removeClass("error");
									responsaveltecnicoInfo.text("ok");
									responsaveltecnicoInfo.removeClass("error");
									return true;
								}
							}
						}
					}
	
					// ENGENHEIRO OU ARQUITETO - RESPONSAVEL TECNICO
					function validateTipoprofissional2(){
						for(var i = 0; i < chks4.length; i++){ 
							if (chks4[i].checked){ 
								hasChecked4 = true; 
								break; 
							} 
						} 
						if(hasChecked4 == false){
							tipoprofissional2Info.text("Marque uma opção");
							tipoprofissional2Info.addClass("error");
							return false;
						}else{
							tipoprofissional2Info.text("ok");
							tipoprofissional2Info.removeClass("error");
							return true;
						}
					}
					
					function validateNumeroalvara(){
						if(numeroalvara.val().length < 1){
							numeroalvara.addClass("error");
							numeroalvaraInfo.text("Digite o número do alvará");
							numeroalvaraInfo.addClass("error");
							return false;
						}else{
							numeroalvara.removeClass("error");
							numeroalvaraInfo.text("ok");
							numeroalvaraInfo.removeClass("error");
							return true;
						}
					}
					
	
					
					function validateQuadraLote(){
						if(quadra.val().length < 1 || lote.val().length < 1){
							quadra.addClass("error");
							lote.addClass("error");
							quadraloteInfo.text("Digite a Quadra e o Lote");
							quadraloteInfo.addClass("error");
							return false;
						}else{
							quadra.removeClass("error");
							lote.removeClass("error");
							quadraloteInfo.text("ok");
							quadraloteInfo.removeClass("error");
							return true;
						}
					}
					
					function validatePavimentos(){
						if(pavimentos.val().length < 1){
							pavimentos.addClass("error");
							pavimentosInfo.text("Digite um valor para a &Aacute;rea do Pavimento");
							pavimentosInfo.addClass("error");
							return false;
						}else{
							pavimentos.removeClass("error");
							pavimentosInfo.text("ok");
							pavimentosInfo.removeClass("error");
							return true;
						}
					}
					
					function validateTipoprofissional(){
						
						for(var i = 0; i < chks3.length; i++){ 
							if (chks3[i].checked){ 
								hasChecked3 = true; 
								break; 
							} 
						} 
							
						if(hasChecked3 == false){
							tipoprofissionalInfo.text("Marque uma opção");
							tipoprofissionalInfo.addClass("error");
							return false;
						}else{
							tipoprofissionalInfo.text("ok");
							tipoprofissionalInfo.removeClass("error");
							return true;
						}
					}
				
				*/
				
			});

			// VALIDACAO DO FORM DE CADASTRO DE DESMEMBRAMENTO ======================================================================>>>>
			$(document).ready(function(){
				// VARIAVEIS GLOBAIS
				var frmdesmembramento = $("#frmdesmembramento");
				// NOME DO PROPRIETARIO
				var nomeproprietario = $("#nome_proprietario");
				var nomeproprietarioInfo = $("#nome_proprietario_info");
				// PESSOA FISICA OU JURIDICA
				var chks = document.getElementsByName("pessoa");
				var hasChecked = false;
				var pessoaInfo = $("#pessoa_info");
				// AUTORIA
				var autoria = $("#autoria");
				var autoriaInfo = $("#autoria_info");
				// TIPO PROFISSIONAL - AUTORIA
				var chks3 = document.getElementsByName('tipo_profissional_autoria'); 
				var hasChecked3 = false; 
				var tipoprofissionalInfo = $("#tipo_profissional_autoria_info");
				// ENDERECO
				var endereco = $("#endereco");
				var enderecoInfo = $("#endereco_info");
				// BAIRRO
				var bairro = $("#bairro");
				var bairroInfo = $("#bairro_info");
				// QUANTIDADE DE LOTES 1
				var qtlotes = $("#qt_lotes");
				var qtlotesInfo = $("#qt_lotes_info");
				// AREA TOTAL DO TERRENO
				var areatotalterreno = $("#area_total_terreno");
				var areatotalterrenoInfo = $("#area_total_terreno_info");
				// QUANTIDADE DE LOTES 2
				var qtlotes2 = $("#qt_lotes2");
				var qtlotes2Info = $("#qt_lotes2_info");
				// DESCRICAO DOS LOTES
				var desclotes = $("#desc_lotes");
				var desclotesInfo = $("#desc_lotes_info");
				// REQUERENTE
				var requerente = $("#requerente");
				var requerenteInfo = $("#requerente_info");
				// TELEFONE
				var telefone = $("#telefone");
				var telefoneInfo = $("#telefone_info");
								
				nomeproprietario.blur(validateNomeproprietario);
				autoria.blur(validateAutoria);
				endereco.blur(validateEndereco);
				bairro.blur(validateBairro);
				qtlotes.blur(validateQtlotes);
				areatotalterreno.blur(validateAreatotalterreno);
				qtlotes2.blur(validateQtlotes2);
				desclotes.blur(validateDesclotes);
				requerente.blur(validateRequerente);
				telefone.blur(validateTelefone);
				
				nomeproprietario.keyup(validateNomeproprietario);
				autoria.keyup(validateAutoria);
				endereco.keyup(validateEndereco);
				bairro.keyup(validateBairro);
				qtlotes.keyup(validateQtlotes);
				areatotalterreno.keyup(validateAreatotalterreno);
				qtlotes2.keyup(validateQtlotes2);
				desclotes.keyup(validateDesclotes);
				requerente.keyup(validateRequerente);
				telefone.keyup(validateTelefone);
				
				frmdesmembramento.submit(function(){
					if(validateNomeproprietario() & validatePessoa() & validateAutoria() & validateTipoprofissional() & validateEndereco() & validateBairro() & validateQtlotes() & validateAreatotalterreno() & validateQtlotes2() & validateDesclotes() & validateRequerente() & validateTelefone())
						return true
					else
						return false
				});

				//  
				function validateNomeproprietario(){
					if(nomeproprietario.val().length < 5){
						nomeproprietario.addClass("error");
						nomeproprietarioInfo.text("Digite o nome do proprietário (Mínino de caracteres 5)");
						nomeproprietarioInfo.addClass("error");
						return false;
					}else{
						nomeproprietario.removeClass("error");
						nomeproprietarioInfo.text("ok");
						nomeproprietarioInfo.removeClass("error");
						return true;
					}
				}
				
				function validateAutoria(){
					if(autoria.val().length < 5){
						autoria.addClass("error");
						autoriaInfo.text("Digite o Autor (Mínino de caracteres 5)");
						autoriaInfo.addClass("error");
						return false;
					}else{
						autoria.removeClass("error");
						autoriaInfo.text("ok");
						autoriaInfo.removeClass("error");
						return true;
					}
				}

				function validateEndereco(){
					if(endereco.val().length < 5){
						endereco.addClass("error");
						enderecoInfo.text("Digite o endereço (Mínino de caracteres 5)");
						enderecoInfo.addClass("error");
						return false;
					}else{
						endereco.removeClass("error");
						enderecoInfo.text("ok");
						enderecoInfo.removeClass("error");
						return true;
					}
				}
				
				function validateBairro(){
					if(bairro.val().length < 5){
						bairro.addClass("error");
						bairroInfo.text("Digite o Bairro (Mínino de caracteres 5)");
						bairroInfo.addClass("error");
						return false;
					}else{
						bairro.removeClass("error");
						bairroInfo.text("ok");
						bairroInfo.removeClass("error");
						return true;
					}
				}		
				
				function validatePessoa(){
					for(var i = 0; i < chks.length; i++){ 
						if (chks[i].checked){ 
							hasChecked = true; 
							break; 
						} 
					} 
					if(hasChecked == false){
						pessoaInfo.text("Marque uma opção");
						pessoaInfo.addClass("error");
						return false;
					}else{
						pessoaInfo.text("ok");
						pessoaInfo.removeClass("error");
						return true;
					}
				}
				
				
				function validateTipoprofissional(){
					for(var i = 0; i < chks3.length; i++){ 
						if (chks3[i].checked){ 
							hasChecked3 = true; 
							break; 
						} 
					} 
					if(hasChecked3 == false){
						tipoprofissionalInfo.text("Marque uma opção");
						tipoprofissionalInfo.addClass("error");
						return false;
					}else{
						tipoprofissionalInfo.text("ok");
						tipoprofissionalInfo.removeClass("error");
						return true;
					}
				}
				
				function validateQtlotes(){
					if(qtlotes.val().length < 1){
						qtlotes.addClass("error");
						qtlotesInfo.text("Digite uma Quantidade de Lotes");
						qtlotesInfo.addClass("error");
						return false;
					}else{
						qtlotes.removeClass("error");
						qtlotesInfo.text("ok");
						qtlotesInfo.removeClass("error");
						return true;
					}
				}
				
				function validateAreatotalterreno(){
					if(areatotalterreno.val().length < 1){
						areatotalterreno.addClass("error");
						areatotalterrenoInfo.text("Digite a área Total do Terreno");
						areatotalterrenoInfo.addClass("error");
						return false;
					}else{
						areatotalterreno.removeClass("error");
						areatotalterrenoInfo.text("ok");
						areatotalterrenoInfo.removeClass("error");
						return true;
					}
				}
				
				function validateQtlotes2(){
					if(qtlotes2.val().length < 1){
						qtlotes2.addClass("error");
						qtlotes2Info.text("Digite uma Quantidade de Lotes");
						qtlotes2Info.addClass("error");
						return false;
					}else{
						qtlotes2.removeClass("error");
						qtlotes2Info.text("ok");
						qtlotes2Info.removeClass("error");
						return true;
					}
				}
				
				function validateDesclotes(){
					if(desclotes.val().length < 1){
						desclotes.addClass("error");
						desclotesInfo.text("Digite a Descrição do(s) Lote(s)");
						desclotesInfo.addClass("error");
						return false;
					}else{
						desclotes.removeClass("error");
						desclotesInfo.text("ok");
						desclotesInfo.removeClass("error");
						return true;
					}
				}
				
				function validateRequerente(){
					if(requerente.val().length < 5){
						requerente.addClass("error");
						requerenteInfo.text("Digite o Requerente (Mínino de caracteres 5)");
						requerenteInfo.addClass("error");
						return false;
					}else{
						requerente.removeClass("error");
						requerenteInfo.text("ok");
						requerenteInfo.removeClass("error");
						return true;
					}
				}
				
				function validateTelefone(){
					if(telefone.val().length < 4){
						telefone.addClass("error");
						telefoneInfo.text("Digite um Telefone para contato");
						telefoneInfo.addClass("error");
						return false;
					}else{
						telefone.removeClass("error");
						telefoneInfo.text("ok");
						telefoneInfo.removeClass("error");
						return true;
					}
				}
				
			});
			
			// VALIDACAO DO FORM DE ALTERACAO DE USUARIOS ============================================================================>>>>
			$(document).ready(function(){
				
				// VARIAVEIS GLOBAIS
				var frmusuariosalt = $("#frmusuariosalt");
				
				var nome = $("#nome");
				var nomeInfo = $("#nome_info");
				
				var sobrenome = $("#sobrenome");
				var sobrenomeInfo = $("#sobrenome_info");
				
				var email = $("#email");
				var emailInfo = $("#email_info");
				
				var cpf = $("#cpf");
				var cpfInfo = $("#cpf_info");
				
				var telefone = $("#telefone");
				var telefoneInfo = $("#telefone_info");
				
				var tipoprofissional = $("#tipo_profissional");
				var tipoprofissionalInfo = $("#tipo_profissional_info");
				
				var dadosprofissional = $("#dados_profissional");
				var dadosprofissionalInfo = $("#dados_profissional_info");
				
				var usuarioatualautenticacao = $("#usuario_atual_autenticacao");
				var usuarioatualautenticacaoInfo = $("#usuario_atual_autenticacao_info");
				
				var senhaatualautenticacao = $("#senha_atual_autenticacao");
				var senhaatualautenticacaoInfo = $("#senha_atual_autenticacao_info");
								
				nome.blur(validateNome);
				sobrenome.blur(validateSobrenome);
				email.blur(validateEmail);
				cpf.blur(validateCpf);
				telefone.blur(validateTelefone);
				tipoprofissional.blur(validateTipoprofissional);
				dadosprofissional.blur(validateDadosprofissional);
				usuarioatualautenticacao.blur(validateUsuarioatualautenticacao);
				senhaatualautenticacao.blur(validateSenhaatualautenticacao);
				
				nome.keyup(validateNome);
				sobrenome.keyup(validateSobrenome);
				email.keyup(validateEmail);
				cpf.keyup(validateCpf);
				telefone.keyup(validateTelefone);
				tipoprofissional.keyup(validateTipoprofissional);
				dadosprofissional.keyup(validateDadosprofissional);
				usuarioatualautenticacao.keyup(validateUsuarioatualautenticacao);
				senhaatualautenticacao.keyup(validateSenhaatualautenticacao);

				frmusuariosalt.submit(function(){
					if(validateNome() & validateSobrenome() & validateEmail() & validateCpf() & validateTelefone() & validateTipoprofissional() & validateDadosprofissional() & validateUsuarioatualautenticacao() & validateSenhaatualautenticacao())
						return true
					else
						return false
				});

				function validateNome(){
					if(nome.val().length < 5){
						nome.addClass("error");
						nomeInfo.text("Preencha o campo Nome!");
						nomeInfo.addClass("error");
						return false;
					}else{
						nome.removeClass("error");
						nomeInfo.text("ok");
						nomeInfo.removeClass("error");
						return true;
					}
				}
				
				function validateSobrenome(){
					if(sobrenome.val().length < 5){
						sobrenome.addClass("error");
						sobrenomeInfo.text("Preencha o campo Sobrenome!");
						sobrenomeInfo.addClass("error");
						return false;
					}else{
						sobrenome.removeClass("error");
						sobrenomeInfo.text("ok");
						sobrenomeInfo.removeClass("error");
						return true;
					}
				}
				
				function validateEmail(){
					var a = $("#email").val();
					var filter = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
					if(filter.test(a)){
						email.removeClass("error");
						emailInfo.text("ok");
						emailInfo.removeClass("error");
						return true;
					}else{
						email.addClass("error");
						emailInfo.text("Digite seu E-mail corretamente!");
						emailInfo.addClass("error");
						return false;
					}
				}
				
				function validateCpf(){
					if(cpf.val().length < 5){
						cpf.addClass("error");
						cpfInfo.text("Preencha o campo CPF!");
						cpfInfo.addClass("error");
						return false;
					}else{
						cpf.removeClass("error");
						cpfInfo.text("ok");
						cpfInfo.removeClass("error");
						return true;
					}
				}
				
				function validateTelefone(){
					if(telefone.val().length < 5){
						telefone.addClass("error");
						telefoneInfo.text("Preencha o campo Telefone!");
						telefoneInfo.addClass("error");
						return false;
					}else{
						telefone.removeClass("error");
						telefoneInfo.text("ok");
						telefoneInfo.removeClass("error");
						return true;
					}
				}
				
				function validateTipoprofissional(){
					if(tipoprofissional.val() == 0){
						tipoprofissional.addClass("error");
						tipoprofissionalInfo.text("Preencha o seu tipo profissional!");
						tipoprofissionalInfo.addClass("error");
						return false;
					}else{
						tipoprofissional.removeClass("error");
						tipoprofissionalInfo.text("ok");
						tipoprofissionalInfo.removeClass("error");
						return true;
					}
				}
				
				function validateDadosprofissional(){
					if(dadosprofissional.val().length < 5){
						dadosprofissional.addClass("error");
						dadosprofissionalInfo.text("Obrigatório!");
						dadosprofissionalInfo.addClass("error");
						return false;
					}else{
						dadosprofissional.removeClass("error");
						dadosprofissionalInfo.text("ok");
						dadosprofissionalInfo.removeClass("error");
						return true;
					}
				}
				
				function validateUsuarioatualautenticacao(){
					if(usuarioatualautenticacao.val().length < 3){
						usuarioatualautenticacao.addClass("error");
						usuarioatualautenticacaoInfo.text("Campo necessário!");
						usuarioatualautenticacaoInfo.addClass("error");
						return false;
					}else{
						usuarioatualautenticacao.removeClass("error");
						usuarioatualautenticacaoInfo.text("ok");
						usuarioatualautenticacaoInfo.removeClass("error");
						return true;
					}
				}
				
				function validateSenhaatualautenticacao(){
					if(senhaatualautenticacao.val().length < 3){
						senhaatualautenticacao.addClass("error");
						senhaatualautenticacaoInfo.text("Campo necessário!");
						senhaatualautenticacaoInfo.addClass("error");
						return false;
					}else{
						senhaatualautenticacao.removeClass("error");
						senhaatualautenticacaoInfo.text("ok");
						senhaatualautenticacaoInfo.removeClass("error");
						return true;
					}
				}
		
			});
			
			
			
			// VALIDACAO DO FORM DE PESQUISA =======================================================================================>>>>
			$(document).ready(function(){
				
				// VARIAVEIS GLOBAIS
				var frmpesquisa = $("#frmpesquisa");
				var consulta = $("#consulta");
				var processo = $("#processo_busca");
				
				consulta.blur(validateConsulta);
				processo.blur(validateProcesso);
				consulta.keyup(validateConsulta);
				processo.keyup(validateProcesso);
				
				frmpesquisa.submit(function(){
					if(validateConsulta() & validateProcesso())
						return true
					else
						return false
				});

				function validateConsulta(){
					if(consulta.val().length < 2){
						consulta.addClass("error");
						return false;
					}else{
						consulta.removeClass("error");
						return true;
					}
				}
				
				function validateProcesso(){
					if(processo.val()==0){
						processo.addClass("error");
						return false;
					}else{
						processo.removeClass("error");
						return true;
					}
				}
				
			});
			
			// VALIDACAO DO FORM DE HABITESE==================================================================================================>>>>
			$(document).ready(function(){
				
				// VARIAVEIS GLOBAIS
				var frmhabitese = $("#frmhabitese");
				var numero = $("#numero");
				var cpf = $("#cpf2");
				
				numero.blur(validateNumero);
				cpf.blur(validateCpf);
				numero.keyup(validateNumero);
				cpf.keyup(validateCpf);
				
				frmhabitese.submit(function(){
					if(validateNumero() & validateCpf())
						return true
					else
						return false
				});

				function validateNumero(){
					if(numero.val().length < 3){
						numero.addClass("error");
						return false;
					}else{
						numero.removeClass("error");
						return true;
					}
				}
				
				function validateCpf(){
					if(cpf.val().length < 3){
						cpf.addClass("error");
						return false;
					}else{
						cpf.removeClass("error");
						return true;
					}
				}
				
			});
			
			
			
			// VALIDACAO DO FORM DE CADASTRO DE PROFISSIONAL =================================================================================>>>>
			$(document).ready(function(){
				
				var frmcadprofissional = $("#frmcadprofissional");
				var nome = $("#nome");
				var sobrenome = $("#sobrenome");
				var email = $("#email");
				var cpf = $("#cpf");
				var telefone = $("#telefone");
				var tipoprofissional = $("#tipo_profissional");
				var dadosprofissional = $("#dados_profissional");
				var usuario = $("#usuario");
				var pass1 = $("#senha");
				var pass2 = $("#senha2");
								
				nome.blur(validateNome);
				sobrenome.blur(validateSobrenome);
				email.blur(validateEmail);
				cpf.blur(validateCpf);
				telefone.blur(validateTelefone);
				tipoprofissional.blur(validateTipoprofissional);
				dadosprofissional.blur(validateDadosprofissional);
				usuario.blur(validateUsuario);
				pass1.blur(validatePass1);
				pass2.blur(validatePass2);
				
				nome.keyup(validateNome);
				sobrenome.keyup(validateSobrenome);
				email.keyup(validateEmail);
				cpf.keyup(validateCpf);
				telefone.keyup(validateTelefone);
				tipoprofissional.keyup(validateTipoprofissional);
				dadosprofissional.keyup(validateDadosprofissional);
				usuario.keyup(validateUsuario);
				pass1.keyup(validatePass1);
				pass2.keyup(validatePass2);

				frmcadprofissional.submit(function(){
					if(validateNome() & validateSobrenome() & validateEmail() & validateCpf() & validateTelefone() & validateTipoprofissional() & validateDadosprofissional() & validateUsuario() & validatePass1() & validatePass2())
						return true
					else
						return false
					
				});

				function validateNome(){
					if(nome.val().length < 2){
						$("#imgerro_nome").css("display","block");
						$("#imgsucess_nome").css("display","none");
						return false;
					}else{
						$("#imgerro_nome").css("display","none");
						$("#imgsucess_nome").css("display","block");
						return true;
					}
				}
				
				function validateSobrenome(){
					if(sobrenome.val().length < 5){
						$("#imgerro_sobrenome").css("display","block");
						$("#imgsucess_sobrenome").css("display","none");
						return false;
					}else{
						$("#imgerro_sobrenome").css("display","none");
						$("#imgsucess_sobrenome").css("display","block");
						return true;
					}
				}
				
				function validateEmail(){
					var a = $("#email").val();
					var filter = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
					if(filter.test(a) || email.val().length < 1){
						$("#imgerro_email").css("display","block");
						$("#imgsucess_email").css("display","none");
						return false;
					}else{
						$("#imgerro_email").css("display","none");
						$("#imgsucess_email").css("display","block");
						return true;
					}
				}
				
				function validateCpf(){
					if(cpf.val().length < 5){
						$("#imgerro_cpf").css("display","block");
						$("#imgsucess_cpf").css("display","none");
						return false;
					}else{
						$("#imgerro_cpf").css("display","none");
						$("#imgsucess_cpf").css("display","block");
						return true;
					}
				}
				
				function validateTelefone(){
					if(telefone.val().length < 5){
						$("#imgerro_telefone").css("display","block");
						$("#imgsucess_telefone").css("display","none");
						return false;
					}else{
						$("#imgerro_telefone").css("display","none");
						$("#imgsucess_telefone").css("display","block");
						return true;
					}
				}
				
				function validateTipoprofissional(){
					if(tipoprofissional.val()==0){
						$("#imgerro_tipo_profissional").css("display","block");
						$("#imgsucess_tipo_profissional").css("display","none");
						return false;
					}else{
						$("#imgerro_tipo_profissional").css("display","none");
						$("#imgsucess_tipo_profissional").css("display","block");
						return true;
					}
				}
				
				function validateDadosprofissional(){
					if(dadosprofissional.val().length < 5){
						$("#imgerro_dados_profissional").css("display","block");
						$("#imgsucess_dados_profissional").css("display","none");
						return false;
					}else{
						$("#imgerro_dados_profissional").css("display","none");
						$("#imgsucess_dados_profissional").css("display","block");
						return true;
					}
				}
				
				function validateUsuario(){
					if(usuario.val().length < 5){
						$("#imgerro_usuario").css("display","block");
						$("#imgsucess_usuario").css("display","none");
						return false;
					}else{
						$("#imgerro_usuario").css("display","none");
						$("#imgsucess_usuario").css("display","block");
						return true;
					}
				}
				
				function validatePass1(){
					var a = $("#password1");
					var b = $("#password2");
			
					//it's NOT valid
					if(pass1.val().length <5){
						$("#imgerro_senha").css("display","block");
						$("#imgsucess_senha").css("display","none");
						return false;
					}else{
						$("#imgerro_senha").css("display","none");
						$("#imgsucess_senha").css("display","block");
						return true;
					}
				}
				
				function validatePass2(){
					var a = $("#password1");
					var b = $("#password2");
					//are NOT valid
					if( pass1.val() != pass2.val() || pass2.val().length < 1){
						$("#imgerro_senha2").css("display","block");
						$("#imgsucess_senha2").css("display","none");
						return false;
					}else{
						$("#imgerro_senha2").css("display","none");
						$("#imgsucess_senha2").css("display","block");
						return true;
					}
				}

			});
			
			
			// VALIDACAO DO FORM DE CHECAGEM PRIMEIRA ANALISE DE DOCUMENTOS ENTREGUES==================================================================================================>>>>
			$(document).ready(function(){
				
				// VARIAVEIS GLOBAIS
				var frmchecagem1processo = $("#frmchecagem1processo");
				var chks11 = document.getElementsByName('valor_documentacao1'); 
				var chks12 = document.getElementsByName('valor_documentacao2'); 
				var chks13 = document.getElementsByName('valor_documentacao3'); 
				var chks14 = document.getElementsByName('valor_documentacao4'); 
				var chks15 = document.getElementsByName('valor_documentacao5');
				var chks16 = document.getElementsByName('valor_documentacao6');
				var chks17 = document.getElementsByName('valor_documentacao7');
				var chks18 = document.getElementsByName('valor_situacao_alvara');
				var hasChecked1All = false;
				var hasChecked2All = false;
				var hasChecked3All = false;
				var hasChecked4All = false;
				var hasChecked5All = false;
				var hasChecked6All = false;
				var hasChecked7All = false;
				var hasChecked8All = false;
				var valordocumentacao1Info = $("#valor_documentacao1_info");
				var valordocumentacao2Info = $("#valor_documentacao2_info");
				var valordocumentacao3Info = $("#valor_documentacao3_info");
				var valordocumentacao4Info = $("#valor_documentacao4_info");
				var valordocumentacao5Info = $("#valor_documentacao5_info");
				var valordocumentacao6Info = $("#valor_documentacao6_info");
				var valordocumentacao7Info = $("#valor_documentacao7_info");
				var valorsituacaoalvaraInfo = $("#valor_situacao_alvara_info");
				
				frmchecagem1processo.submit(function(){
					if(valorDocumentacao1() & valorDocumentacao2() & valorDocumentacao3() & valorDocumentacao4() & valorDocumentacao5() & valorDocumentacao6() & valorDocumentacao7() & valorSituacaoAlvara())
						return true
					else
						return false
				});

				// FUNCAO 1 PARA VALIDAR CHECKEBOXES DE ANALISE
				function valorDocumentacao1(){
					for(var i = 0; i < chks11.length; i++){ 
						if (chks11[i].checked){ 
							hasChecked1All = true; 
							break; 
						} 
					} 
						
					if(hasChecked1All == false){
						valordocumentacao1Info.text("Marque uma opção");
						valordocumentacao1Info.addClass("error");
						return false;
					}else{
						valordocumentacao1Info.text("ok");
						valordocumentacao1Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 2 PARA VALIDAR CHECKEBOXES DE ANALISE
				function valorDocumentacao2(){
					for(var i = 0; i < chks12.length; i++){ 
						if (chks12[i].checked){ 
							hasChecked2All = true; 
							break; 
						} 
					} 
						
					if(hasChecked2All == false){
						valordocumentacao2Info.text("Marque uma opção");
						valordocumentacao2Info.addClass("error");
						return false;
					}else{
						valordocumentacao2Info.text("ok");
						valordocumentacao2Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 3 PARA VALIDAR CHECKEBOXES DE ANALISE
				function valorDocumentacao3(){
					for(var i = 0; i < chks13.length; i++){ 
						if (chks13[i].checked){ 
							hasChecked3All = true; 
							break; 
						} 
					} 
						
					if(hasChecked3All == false){
						valordocumentacao3Info.text("Marque uma opção");
						valordocumentacao3Info.addClass("error");
						return false;
					}else{
						valordocumentacao3Info.text("ok");
						valordocumentacao3Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 4 PARA VALIDAR CHECKEBOXES DE ANALISE
				function valorDocumentacao4(){
					for(var i = 0; i < chks14.length; i++){ 
						if (chks14[i].checked){ 
							hasChecked4All = true; 
							break; 
						} 
					} 
						
					if(hasChecked4All == false){
						valordocumentacao4Info.text("Marque uma opção");
						valordocumentacao4Info.addClass("error");
						return false;
					}else{
						valordocumentacao4Info.text("ok");
						valordocumentacao4Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 5 PARA VALIDAR CHECKEBOXES DE ANALISE
				function valorDocumentacao5(){
					for(var i = 0; i < chks15.length; i++){ 
						if (chks15[i].checked){ 
							hasChecked5All = true; 
							break; 
						} 
					} 
						
					if(hasChecked5All == false){
						valordocumentacao5Info.text("Marque uma opção");
						valordocumentacao5Info.addClass("error");
						return false;
					}else{
						valordocumentacao5Info.text("ok");
						valordocumentacao5Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 6 PARA VALIDAR CHECKEBOXES DE ANALISE
				function valorDocumentacao6(){
					for(var i = 0; i < chks16.length; i++){ 
						if (chks16[i].checked){ 
							hasChecked6All = true; 
							break; 
						} 
					} 
						
					if(hasChecked6All == false){
						valordocumentacao6Info.text("Marque uma opção");
						valordocumentacao6Info.addClass("error");
						return false;
					}else{
						valordocumentacao6Info.text("ok");
						valordocumentacao6Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 7 PARA VALIDAR CHECKEBOXES DE ANALISE
				function valorDocumentacao7(){
					for(var i = 0; i < chks17.length; i++){ 
						if (chks17[i].checked){ 
							hasChecked7All = true; 
							break; 
						} 
					} 
						
					if(hasChecked7All == false){
						valordocumentacao7Info.text("Marque uma opção");
						valordocumentacao7Info.addClass("error");
						return false;
					}else{
						valordocumentacao7Info.text("ok");
						valordocumentacao7Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 8 PARA VALIDAR CHECKEBOXES DE ANALISE
				function valorSituacaoAlvara(){
					for(var i = 0; i < chks18.length; i++){ 
						if (chks18[i].checked){ 
							hasChecked8All = true; 
							break; 
						} 
					} 
						
					if(hasChecked8All == false){
						valorsituacaoalvaraInfo.text("Marque uma opção");
						valorsituacaoalvaraInfo.addClass("error");
						return false;
					}else{
						valorsituacaoalvaraInfo.text("ok");
						valorsituacaoalvaraInfo.removeClass("error");
						return true;
					}
				}

				
			});
			
			
			// VALIDACAO DO FORM DE ANALISE E CADASTRO DE PENDENCIA ============================================================================>>>>
			$(document).ready(function(){
				
				// VARIAVEIS GLOBAIS
				var frmanalisemaispendencia = $("#frmanalisemaispendencia");
				
				var chks1 = document.getElementsByName('valor_analise1'); 
				var valordocumentacao1Info = $("#valor_analise1_info");
				var hasChecked1All = false;
				var chks2 = document.getElementsByName('valor_analise2'); 
				var valordocumentacao2Info = $("#valor_analise2_info");
				var hasChecked2All = false;
				var chks3 = document.getElementsByName('valor_analise3'); 
				var valordocumentacao3Info = $("#valor_analise3_info");
				var hasChecked3All = false;
				var chks4 = document.getElementsByName('valor_analise4'); 
				var valordocumentacao4Info = $("#valor_analise4_info");
				var hasChecked4All = false;
				var chks5 = document.getElementsByName('valor_analise5'); 
				var valordocumentacao5Info = $("#valor_analise5_info");
				var hasChecked5All = false;
				var chks6 = document.getElementsByName('valor_analise6'); 
				var valordocumentacao6Info = $("#valor_analise6_info");
				var hasChecked6All = false;
				var chks7 = document.getElementsByName('valor_analise7'); 
				var valordocumentacao7Info = $("#valor_analise7_info");
				var hasChecked7All = false;
				var chks8 = document.getElementsByName('valor_analise8'); 
				var valordocumentacao8Info = $("#valor_analise8_info");
				var hasChecked8All = false;
				var chks9 = document.getElementsByName('valor_analise9'); 
				var valordocumentacao9Info = $("#valor_analise9_info");
				var hasChecked9All = false;
				var chks10 = document.getElementsByName('valor_analise10'); 
				var valordocumentacao10Info = $("#valor_analise10_info");
				var hasChecked10All = false;
				var chks11 = document.getElementsByName('valor_analise11'); 
				var valordocumentacao11Info = $("#valor_analise11_info");
				var hasChecked11All = false;
				var chks12 = document.getElementsByName('valor_analise12'); 
				var valordocumentacao12Info = $("#valor_analise12_info");
				var hasChecked12All = false;
				var chks13 = document.getElementsByName('valor_analise13'); 
				var valordocumentacao13Info = $("#valor_analise13_info");
				var hasChecked13All = false;
				var chks14 = document.getElementsByName('valor_analise14'); 
				var valordocumentacao14Info = $("#valor_analise14_info");
				var hasChecked14All = false;
				var chks15 = document.getElementsByName('valor_analise15'); 
				var valordocumentacao15Info = $("#valor_analise15_info");
				var hasChecked15All = false;
				var chks16 = document.getElementsByName('valor_analise16'); 
				var valordocumentacao16Info = $("#valor_analise16_info");
				var hasChecked16All = false;
				var chks17 = document.getElementsByName('valor_analise17'); 
				var valordocumentacao17Info = $("#valor_analise17_info");
				var hasChecked17All = false;
				var chks18 = document.getElementsByName('valor_analise18'); 
				var valordocumentacao18Info = $("#valor_analise18_info");
				var hasChecked18All = false;
				var chks19 = document.getElementsByName('valor_analise19'); 
				var valordocumentacao19Info = $("#valor_analise19_info");
				var hasChecked19All = false;
				var chks20 = document.getElementsByName('valor_analise20'); 
				var valordocumentacao20Info = $("#valor_analise20_info");
				var hasChecked20All = false;
				var chks21 = document.getElementsByName('valor_analise21'); 
				var valordocumentacao21Info = $("#valor_analise21_info");
				var hasChecked21All = false;
				var chks22 = document.getElementsByName('valor_analise22'); 
				var valordocumentacao22Info = $("#valor_analise22_info");
				var hasChecked22All = false;
				var chks23 = document.getElementsByName('valor_analise23'); 
				var valordocumentacao23Info = $("#valor_analise23_info");
				var hasChecked23All = false;
				var chks24 = document.getElementsByName('valor_analise24'); 
				var valordocumentacao24Info = $("#valor_analise24_info");
				var hasChecked24All = false;
				var chks25 = document.getElementsByName('valor_analise25'); 
				var valordocumentacao25Info = $("#valor_analise25_info");
				var hasChecked25All = false;
				var chks26 = document.getElementsByName('valor_analise26'); 
				var valordocumentacao26Info = $("#valor_analise26_info");
				var hasChecked26All = false;
				var chks27 = document.getElementsByName('valor_analise27'); 
				var valordocumentacao27Info = $("#valor_analise27_info");
				var hasChecked27All = false;
				var chks28 = document.getElementsByName('valor_analise28'); 
				var valordocumentacao28Info = $("#valor_analise28_info");
				var hasChecked28All = false;
				var chks29 = document.getElementsByName('valor_analise29'); 
				var valordocumentacao29Info = $("#valor_analise29_info");
				var hasChecked29All = false;
				var chks30 = document.getElementsByName('valor_analise30'); 
				var valordocumentacao30Info = $("#valor_analise30_info");
				var hasChecked30All = false;
				var chks31 = document.getElementsByName('valor_analise31'); 
				var valordocumentacao31Info = $("#valor_analise31_info");
				var hasChecked31All = false;
				var chks32 = document.getElementsByName('valor_analise32'); 
				var valordocumentacao32Info = $("#valor_analise32_info");
				var hasChecked32All = false;
				var chks33 = document.getElementsByName('valor_analise33'); 
				var valordocumentacao33Info = $("#valor_analise33_info");
				var hasChecked33All = false;
				var chks34 = document.getElementsByName('valor_analise34'); 
				var valordocumentacao34Info = $("#valor_analise34_info");
				var hasChecked34All = false;
				var chks35 = document.getElementsByName('valor_analise35'); 
				var valordocumentacao35Info = $("#valor_analise35_info");
				var hasChecked35All = false;
				var chks36 = document.getElementsByName('valor_analise36'); 
				var valordocumentacao36Info = $("#valor_analise36_info");
				var hasChecked36All = false;
				var chks37 = document.getElementsByName('valor_analise37'); 
				var valordocumentacao37Info = $("#valor_analise37_info");
				var hasChecked37All = false;
				var chks38 = document.getElementsByName('valor_analise38'); 
				var valordocumentacao38Info = $("#valor_analise38_info");
				var hasChecked38All = false;
				var chks39 = document.getElementsByName('valor_analise39'); 
				var valordocumentacao39Info = $("#valor_analise39_info");
				var hasChecked39All = false;
				var chks40 = document.getElementsByName('valor_analise40'); 
				var valordocumentacao40Info = $("#valor_analise40_info");
				var hasChecked40All = false;
				var chks41 = document.getElementsByName('valor_analise41'); 
				var valordocumentacao41Info = $("#valor_analise41_info");
				var hasChecked41All = false;
				var chks42 = document.getElementsByName('valor_analise42'); 
				var valordocumentacao42Info = $("#valor_analise42_info");
				var hasChecked42All = false;
				var chks43 = document.getElementsByName('valor_analise43'); 
				var valordocumentacao43Info = $("#valor_analise43_info");
				var hasChecked43All = false;
				var chks44 = document.getElementsByName('valor_analise44'); 
				var valordocumentacao44Info = $("#valor_analise44_info");
				var hasChecked44All = false;
				var chks45 = document.getElementsByName('valor_analise45'); 
				var valordocumentacao45Info = $("#valor_analise45_info");
				var hasChecked45All = false;
				var chks46 = document.getElementsByName('valor_analise46'); 
				var valordocumentacao46Info = $("#valor_analise46_info");
				var hasChecked46All = false;
				var chks47 = document.getElementsByName('valor_analise47'); 
				var valordocumentacao47Info = $("#valor_analise47_info");
				var hasChecked47All = false;
				
				var quantidadeanalise = $("#quantidade_analise");
				var quantidadeanaliseInfo = $("#quantidade_analise_info");
				
				var chks48 = document.getElementsByName('legenda'); 
				var legendaInfo = $("#legenda_info");
				var hasChecked48All = false;
				
				quantidadeanalise.blur(validateQuantidadeanalise);
				quantidadeanalise.keyup(validateQuantidadeanalise);

				frmanalisemaispendencia.submit(function(){
					if(valorAnalise1() & valorAnalise2() & valorAnalise3() & valorAnalise4() & valorAnalise5() & valorAnalise6() & valorAnalise7() & valorAnalise8() & valorAnalise9() & valorAnalise10() & valorAnalise11() & valorAnalise12() & valorAnalise13() & valorAnalise14() & valorAnalise15() & valorAnalise16() & valorAnalise17() & valorAnalise18() & valorAnalise19() & valorAnalise20() & valorAnalise21() & valorAnalise22() & valorAnalise23() & valorAnalise24() & valorAnalise25() & valorAnalise26() & valorAnalise27() & valorAnalise28() & valorAnalise29() & valorAnalise30() & valorAnalise31() & valorAnalise32() & valorAnalise33() & valorAnalise34() & valorAnalise35() & valorAnalise36() & valorAnalise37() & valorAnalise38() & valorAnalise39() & valorAnalise40() & valorAnalise41() & valorAnalise42() & valorAnalise43() & valorAnalise44() & valorAnalise45() & valorAnalise46() & valorAnalise47() & validateQuantidadeanalise() & valorAnalise48())  
						return true
					else
						return false
				});

				// FUNCAO 1 PARA VALIDAR CHECKEBOXES DE ANALISE PARTE 2
				function valorAnalise1(){
					for(var i = 0; i < chks1.length; i++){ 
						if (chks1[i].checked){ 
							hasChecked1All = true; 
							break; 
						} 
					} 
						
					if(hasChecked1All == false){
						valordocumentacao1Info.text("Marque uma opção");
						valordocumentacao1Info.addClass("error");
						return false;
					}else{
						valordocumentacao1Info.text("ok");
						valordocumentacao1Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 2 PARA VALIDAR CHECKEBOXES DE ANALISE PARTE 2
				function valorAnalise2(){
					for(var i = 0; i < chks2.length; i++){ 
						if (chks2[i].checked){ 
							hasChecked2All = true; 
							break; 
						} 
					} 
						
					if(hasChecked2All == false){
						valordocumentacao2Info.text("Marque uma opção");
						valordocumentacao2Info.addClass("error");
						return false;
					}else{
						valordocumentacao2Info.text("ok");
						valordocumentacao2Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 3 PARA VALIDAR CHECKEBOXES DE ANALISE PARTE 2
				function valorAnalise3(){
					for(var i = 0; i < chks3.length; i++){ 
						if (chks3[i].checked){ 
							hasChecked3All = true; 
							break; 
						} 
					} 
						
					if(hasChecked3All == false){
						valordocumentacao3Info.text("Marque uma opção");
						valordocumentacao3Info.addClass("error");
						return false;
					}else{
						valordocumentacao3Info.text("ok");
						valordocumentacao3Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 4 PARA VALIDAR CHECKEBOXES DE ANALISE PARTE 2
				function valorAnalise4(){
					for(var i = 0; i < chks4.length; i++){ 
						if (chks4[i].checked){ 
							hasChecked4All = true; 
							break; 
						} 
					} 
						
					if(hasChecked4All == false){
						valordocumentacao4Info.text("Marque uma opção");
						valordocumentacao4Info.addClass("error");
						return false;
					}else{
						valordocumentacao4Info.text("ok");
						valordocumentacao4Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 5 PARA VALIDAR CHECKEBOXES DE ANALISE PARTE 2
				function valorAnalise5(){
					for(var i = 0; i < chks5.length; i++){ 
						if (chks5[i].checked){ 
							hasChecked5All = true; 
							break; 
						} 
					} 
						
					if(hasChecked5All == false){
						valordocumentacao5Info.text("Marque uma opção");
						valordocumentacao5Info.addClass("error");
						return false;
					}else{
						valordocumentacao5Info.text("ok");
						valordocumentacao5Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 6 PARA VALIDAR CHECKEBOXES DE ANALISE PARTE 2
				function valorAnalise6(){
					for(var i = 0; i < chks6.length; i++){ 
						if (chks6[i].checked){ 
							hasChecked6All = true; 
							break; 
						} 
					} 
						
					if(hasChecked6All == false){
						valordocumentacao6Info.text("Marque uma opção");
						valordocumentacao6Info.addClass("error");
						return false;
					}else{
						valordocumentacao6Info.text("ok");
						valordocumentacao6Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 7 PARA VALIDAR CHECKEBOXES DE ANALISE PARTE 2
				function valorAnalise7(){
					for(var i = 0; i < chks7.length; i++){ 
						if (chks7[i].checked){ 
							hasChecked7All = true; 
							break; 
						} 
					} 
						
					if(hasChecked7All == false){
						valordocumentacao7Info.text("Marque uma opção");
						valordocumentacao7Info.addClass("error");
						return false;
					}else{
						valordocumentacao7Info.text("ok");
						valordocumentacao7Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 8 PARA VALIDAR CHECKEBOXES DE ANALISE PARTE 2
				function valorAnalise8(){
					for(var i = 0; i < chks8.length; i++){ 
						if (chks8[i].checked){ 
							hasChecked8All = true; 
							break; 
						} 
					} 
						
					if(hasChecked8All == false){
						valordocumentacao8Info.text("Marque uma opção");
						valordocumentacao8Info.addClass("error");
						return false;
					}else{
						valordocumentacao8Info.text("ok");
						valordocumentacao8Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 9 PARA VALIDAR CHECKEBOXES DE ANALISE PARTE 2
				function valorAnalise9(){
					for(var i = 0; i < chks9.length; i++){ 
						if (chks9[i].checked){ 
							hasChecked9All = true; 
							break; 
						} 
					} 
						
					if(hasChecked9All == false){
						valordocumentacao9Info.text("Marque uma opção");
						valordocumentacao9Info.addClass("error");
						return false;
					}else{
						valordocumentacao9Info.text("ok");
						valordocumentacao9Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 10 PARA VALIDAR CHECKEBOXES DE ANALISE PARTE 2
				function valorAnalise10(){
					for(var i = 0; i < chks10.length; i++){ 
						if (chks10[i].checked){ 
							hasChecked10All = true; 
							break; 
						} 
					} 
						
					if(hasChecked10All == false){
						valordocumentacao10Info.text("Marque uma opção");
						valordocumentacao10Info.addClass("error");
						return false;
					}else{
						valordocumentacao10Info.text("ok");
						valordocumentacao10Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 11 PARA VALIDAR CHECKEBOXES DE ANALISE PARTE 2
				function valorAnalise11(){
					for(var i = 0; i < chks11.length; i++){ 
						if (chks11[i].checked){ 
							hasChecked11All = true; 
							break; 
						} 
					} 
						
					if(hasChecked11All == false){
						valordocumentacao11Info.text("Marque uma opção");
						valordocumentacao11Info.addClass("error");
						return false;
					}else{
						valordocumentacao11Info.text("ok");
						valordocumentacao11Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 12 PARA VALIDAR CHECKEBOXES DE ANALISE PARTE 2
				function valorAnalise12(){
					for(var i = 0; i < chks12.length; i++){ 
						if (chks12[i].checked){ 
							hasChecked12All = true; 
							break; 
						} 
					} 
						
					if(hasChecked12All == false){
						valordocumentacao12Info.text("Marque uma opção");
						valordocumentacao12Info.addClass("error");
						return false;
					}else{
						valordocumentacao12Info.text("ok");
						valordocumentacao12Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 13 PARA VALIDAR CHECKEBOXES DE ANALISE PARTE 2
				function valorAnalise13(){
					for(var i = 0; i < chks13.length; i++){ 
						if (chks13[i].checked){ 
							hasChecked13All = true; 
							break; 
						} 
					} 
						
					if(hasChecked13All == false){
						valordocumentacao13Info.text("Marque uma opção");
						valordocumentacao13Info.addClass("error");
						return false;
					}else{
						valordocumentacao13Info.text("ok");
						valordocumentacao13Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 14 PARA VALIDAR CHECKEBOXES DE ANALISE PARTE 2
				function valorAnalise14(){
					for(var i = 0; i < chks14.length; i++){ 
						if (chks14[i].checked){ 
							hasChecked14All = true; 
							break; 
						} 
					} 
						
					if(hasChecked14All == false){
						valordocumentacao14Info.text("Marque uma opção");
						valordocumentacao14Info.addClass("error");
						return false;
					}else{
						valordocumentacao14Info.text("ok");
						valordocumentacao14Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 15 PARA VALIDAR CHECKEBOXES DE ANALISE PARTE 2
				function valorAnalise15(){
					for(var i = 0; i < chks15.length; i++){ 
						if (chks15[i].checked){ 
							hasChecked15All = true; 
							break; 
						} 
					} 
						
					if(hasChecked15All == false){
						valordocumentacao15Info.text("Marque uma opção");
						valordocumentacao15Info.addClass("error");
						return false;
					}else{
						valordocumentacao15Info.text("ok");
						valordocumentacao15Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 16 PARA VALIDAR CHECKEBOXES DE ANALISE PARTE 2
				function valorAnalise16(){
					for(var i = 0; i < chks16.length; i++){ 
						if (chks16[i].checked){ 
							hasChecked16All = true; 
							break; 
						} 
					} 
						
					if(hasChecked16All == false){
						valordocumentacao16Info.text("Marque uma opção");
						valordocumentacao16Info.addClass("error");
						return false;
					}else{
						valordocumentacao16Info.text("ok");
						valordocumentacao16Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 17 PARA VALIDAR CHECKEBOXES DE ANALISE PARTE 2
				function valorAnalise17(){
					for(var i = 0; i < chks17.length; i++){ 
						if (chks17[i].checked){ 
							hasChecked17All = true; 
							break; 
						} 
					} 
						
					if(hasChecked17All == false){
						valordocumentacao17Info.text("Marque uma opção");
						valordocumentacao17Info.addClass("error");
						return false;
					}else{
						valordocumentacao17Info.text("ok");
						valordocumentacao17Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 18 PARA VALIDAR CHECKEBOXES DE ANALISE PARTE 2
				function valorAnalise18(){
					for(var i = 0; i < chks18.length; i++){ 
						if (chks18[i].checked){ 
							hasChecked18All = true; 
							break; 
						} 
					} 
						
					if(hasChecked18All == false){
						valordocumentacao18Info.text("Marque uma opção");
						valordocumentacao18Info.addClass("error");
						return false;
					}else{
						valordocumentacao18Info.text("ok");
						valordocumentacao18Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 19 PARA VALIDAR CHECKEBOXES DE ANALISE PARTE 2
				function valorAnalise19(){
					for(var i = 0; i < chks19.length; i++){ 
						if (chks19[i].checked){ 
							hasChecked19All = true; 
							break; 
						} 
					} 
						
					if(hasChecked19All == false){
						valordocumentacao19Info.text("Marque uma opção");
						valordocumentacao19Info.addClass("error");
						return false;
					}else{
						valordocumentacao19Info.text("ok");
						valordocumentacao19Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 20 PARA VALIDAR CHECKEBOXES DE ANALISE PARTE 2
				function valorAnalise20(){
					for(var i = 0; i < chks20.length; i++){ 
						if (chks20[i].checked){ 
							hasChecked20All = true; 
							break; 
						} 
					} 
						
					if(hasChecked20All == false){
						valordocumentacao20Info.text("Marque uma opção");
						valordocumentacao20Info.addClass("error");
						return false;
					}else{
						valordocumentacao20Info.text("ok");
						valordocumentacao20Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 21 PARA VALIDAR CHECKEBOXES DE ANALISE PARTE 2
				function valorAnalise21(){
					for(var i = 0; i < chks21.length; i++){ 
						if (chks21[i].checked){ 
							hasChecked21All = true; 
							break; 
						} 
					} 
						
					if(hasChecked21All == false){
						valordocumentacao21Info.text("Marque uma opção");
						valordocumentacao21Info.addClass("error");
						return false;
					}else{
						valordocumentacao21Info.text("ok");
						valordocumentacao21Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 22 PARA VALIDAR CHECKEBOXES DE ANALISE PARTE 2
				function valorAnalise22(){
					for(var i = 0; i < chks22.length; i++){ 
						if (chks22[i].checked){ 
							hasChecked22All = true; 
							break; 
						} 
					} 
						
					if(hasChecked22All == false){
						valordocumentacao22Info.text("Marque uma opção");
						valordocumentacao22Info.addClass("error");
						return false;
					}else{
						valordocumentacao22Info.text("ok");
						valordocumentacao22Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 23 PARA VALIDAR CHECKEBOXES DE ANALISE PARTE 2
				function valorAnalise23(){
					for(var i = 0; i < chks23.length; i++){ 
						if (chks23[i].checked){ 
							hasChecked23All = true; 
							break; 
						} 
					} 
						
					if(hasChecked23All == false){
						valordocumentacao23Info.text("Marque uma opção");
						valordocumentacao23Info.addClass("error");
						return false;
					}else{
						valordocumentacao23Info.text("ok");
						valordocumentacao23Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 24 PARA VALIDAR CHECKEBOXES DE ANALISE PARTE 2
				function valorAnalise24(){
					for(var i = 0; i < chks24.length; i++){ 
						if (chks24[i].checked){ 
							hasChecked24All = true; 
							break; 
						} 
					} 
						
					if(hasChecked24All == false){
						valordocumentacao24Info.text("Marque uma opção");
						valordocumentacao24Info.addClass("error");
						return false;
					}else{
						valordocumentacao24Info.text("ok");
						valordocumentacao24Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 25 PARA VALIDAR CHECKEBOXES DE ANALISE PARTE 2
				function valorAnalise25(){
					for(var i = 0; i < chks25.length; i++){ 
						if (chks25[i].checked){ 
							hasChecked25All = true; 
							break; 
						} 
					} 
						
					if(hasChecked25All == false){
						valordocumentacao25Info.text("Marque uma opção");
						valordocumentacao25Info.addClass("error");
						return false;
					}else{
						valordocumentacao25Info.text("ok");
						valordocumentacao25Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 26 PARA VALIDAR CHECKEBOXES DE ANALISE PARTE 2
				function valorAnalise26(){
					for(var i = 0; i < chks26.length; i++){ 
						if (chks26[i].checked){ 
							hasChecked26All = true; 
							break; 
						} 
					} 
						
					if(hasChecked26All == false){
						valordocumentacao26Info.text("Marque uma opção");
						valordocumentacao26Info.addClass("error");
						return false;
					}else{
						valordocumentacao26Info.text("ok");
						valordocumentacao26Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 27 PARA VALIDAR CHECKEBOXES DE ANALISE PARTE 2
				function valorAnalise27(){
					for(var i = 0; i < chks27.length; i++){ 
						if (chks27[i].checked){ 
							hasChecked27All = true; 
							break; 
						} 
					} 
						
					if(hasChecked27All == false){
						valordocumentacao27Info.text("Marque uma opção");
						valordocumentacao27Info.addClass("error");
						return false;
					}else{
						valordocumentacao27Info.text("ok");
						valordocumentacao27Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 28 PARA VALIDAR CHECKEBOXES DE ANALISE PARTE 2
				function valorAnalise28(){
					for(var i = 0; i < chks28.length; i++){ 
						if (chks28[i].checked){ 
							hasChecked28All = true; 
							break; 
						}
					} 
						
					if(hasChecked28All == false){
						valordocumentacao28Info.text("Marque uma opção");
						valordocumentacao28Info.addClass("error");
						return false;
					}else{
						valordocumentacao28Info.text("ok");
						valordocumentacao28Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 29 PARA VALIDAR CHECKEBOXES DE ANALISE PARTE 2
				function valorAnalise29(){
					for(var i = 0; i < chks29.length; i++){ 
						if (chks29[i].checked){ 
							hasChecked29All = true; 
							break; 
						} 
					} 
						
					if(hasChecked29All == false){
						valordocumentacao29Info.text("Marque uma opção");
						valordocumentacao29Info.addClass("error");
						return false;
					}else{
						valordocumentacao29Info.text("ok");
						valordocumentacao29Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 30 PARA VALIDAR CHECKEBOXES DE ANALISE PARTE 2
				function valorAnalise30(){
					for(var i = 0; i < chks30.length; i++){ 
						if (chks30[i].checked){ 
							hasChecked30All = true; 
							break; 
						} 
					} 
						
					if(hasChecked30All == false){
						valordocumentacao30Info.text("Marque uma opção");
						valordocumentacao30Info.addClass("error");
						return false;
					}else{
						valordocumentacao30Info.text("ok");
						valordocumentacao30Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 31 PARA VALIDAR CHECKEBOXES DE ANALISE PARTE 2
				function valorAnalise31(){
					for(var i = 0; i < chks31.length; i++){ 
						if (chks31[i].checked){ 
							hasChecked31All = true; 
							break; 
						} 
					} 
						
					if(hasChecked31All == false){
						valordocumentacao31Info.text("Marque uma opção");
						valordocumentacao31Info.addClass("error");
						return false;
					}else{
						valordocumentacao31Info.text("ok");
						valordocumentacao31Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 32 PARA VALIDAR CHECKEBOXES DE ANALISE PARTE 2
				function valorAnalise32(){
					for(var i = 0; i < chks32.length; i++){ 
						if (chks32[i].checked){ 
							hasChecked32All = true; 
							break; 
						} 
					} 
						
					if(hasChecked32All == false){
						valordocumentacao32Info.text("Marque uma opção");
						valordocumentacao32Info.addClass("error");
						return false;
					}else{
						valordocumentacao32Info.text("ok");
						valordocumentacao32Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 33 PARA VALIDAR CHECKEBOXES DE ANALISE PARTE 2
				function valorAnalise33(){
					for(var i = 0; i < chks33.length; i++){ 
						if (chks33[i].checked){ 
							hasChecked33All = true; 
							break; 
						} 
					} 
						
					if(hasChecked33All == false){
						valordocumentacao33Info.text("Marque uma opção");
						valordocumentacao33Info.addClass("error");
						return false;
					}else{
						valordocumentacao33Info.text("ok");
						valordocumentacao33Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 34 PARA VALIDAR CHECKEBOXES DE ANALISE PARTE 2
				function valorAnalise34(){
					for(var i = 0; i < chks34.length; i++){ 
						if (chks34[i].checked){ 
							hasChecked34All = true; 
							break; 
						} 
					} 
						
					if(hasChecked34All == false){
						valordocumentacao34Info.text("Marque uma opção");
						valordocumentacao34Info.addClass("error");
						return false;
					}else{
						valordocumentacao34Info.text("ok");
						valordocumentacao34Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 35 PARA VALIDAR CHECKEBOXES DE ANALISE PARTE 2
				function valorAnalise35(){
					for(var i = 0; i < chks35.length; i++){ 
						if (chks35[i].checked){ 
							hasChecked35All = true; 
							break; 
						} 
					} 
						
					if(hasChecked35All == false){
						valordocumentacao35Info.text("Marque uma opção");
						valordocumentacao35Info.addClass("error");
						return false;
					}else{
						valordocumentacao35Info.text("ok");
						valordocumentacao35Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 36 PARA VALIDAR CHECKEBOXES DE ANALISE PARTE 2
				function valorAnalise36(){
					for(var i = 0; i < chks36.length; i++){ 
						if (chks36[i].checked){ 
							hasChecked36All = true; 
							break; 
						} 
					} 
						
					if(hasChecked36All == false){
						valordocumentacao36Info.text("Marque uma opção");
						valordocumentacao36Info.addClass("error");
						return false;
					}else{
						valordocumentacao36Info.text("ok");
						valordocumentacao36Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 37 PARA VALIDAR CHECKEBOXES DE ANALISE PARTE 2
				function valorAnalise37(){
					for(var i = 0; i < chks37.length; i++){ 
						if (chks37[i].checked){ 
							hasChecked37All = true; 
							break; 
						} 
					} 
						
					if(hasChecked37All == false){
						valordocumentacao37Info.text("Marque uma opção");
						valordocumentacao37Info.addClass("error");
						return false;
					}else{
						valordocumentacao37Info.text("ok");
						valordocumentacao37Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 38 PARA VALIDAR CHECKEBOXES DE ANALISE PARTE 2
				function valorAnalise38(){
					for(var i = 0; i < chks38.length; i++){ 
						if (chks38[i].checked){ 
							hasChecked38All = true; 
							break; 
						} 
					} 
						
					if(hasChecked38All == false){
						valordocumentacao38Info.text("Marque uma opção");
						valordocumentacao38Info.addClass("error");
						return false;
					}else{
						valordocumentacao38Info.text("ok");
						valordocumentacao38Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 39 PARA VALIDAR CHECKEBOXES DE ANALISE PARTE 2
				function valorAnalise39(){
					for(var i = 0; i < chks39.length; i++){ 
						if (chks39[i].checked){ 
							hasChecked39All = true; 
							break; 
						} 
					} 
						
					if(hasChecked39All == false){
						valordocumentacao39Info.text("Marque uma opção");
						valordocumentacao39Info.addClass("error");
						return false;
					}else{
						valordocumentacao39Info.text("ok");
						valordocumentacao39Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 40 PARA VALIDAR CHECKEBOXES DE ANALISE PARTE 2
				function valorAnalise40(){
					for(var i = 0; i < chks40.length; i++){ 
						if (chks40[i].checked){ 
							hasChecked40All = true; 
							break; 
						} 
					} 
						
					if(hasChecked40All == false){
						valordocumentacao40Info.text("Marque uma opção");
						valordocumentacao40Info.addClass("error");
						return false;
					}else{
						valordocumentacao40Info.text("ok");
						valordocumentacao40Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 41 PARA VALIDAR CHECKEBOXES DE ANALISE PARTE 2
				function valorAnalise41(){
					for(var i = 0; i < chks41.length; i++){ 
						if (chks41[i].checked){ 
							hasChecked41All = true; 
							break; 
						} 
					} 
						
					if(hasChecked41All == false){
						valordocumentacao41Info.text("Marque uma opção");
						valordocumentacao41Info.addClass("error");
						return false;
					}else{
						valordocumentacao41Info.text("ok");
						valordocumentacao41Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 42 PARA VALIDAR CHECKEBOXES DE ANALISE PARTE 2
				function valorAnalise42(){
					for(var i = 0; i < chks42.length; i++){ 
						if (chks42[i].checked){ 
							hasChecked42All = true; 
							break; 
						} 
					} 
						
					if(hasChecked42All == false){
						valordocumentacao42Info.text("Marque uma opção");
						valordocumentacao42Info.addClass("error");
						return false;
					}else{
						valordocumentacao42Info.text("ok");
						valordocumentacao42Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 43 PARA VALIDAR CHECKEBOXES DE ANALISE PARTE 2
				function valorAnalise43(){
					for(var i = 0; i < chks43.length; i++){ 
						if (chks43[i].checked){ 
							hasChecked43All = true; 
							break; 
						} 
					} 
						
					if(hasChecked43All == false){
						valordocumentacao43Info.text("Marque uma opção");
						valordocumentacao43Info.addClass("error");
						return false;
					}else{
						valordocumentacao43Info.text("ok");
						valordocumentacao43Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 44 PARA VALIDAR CHECKEBOXES DE ANALISE PARTE 2
				function valorAnalise44(){
					for(var i = 0; i < chks44.length; i++){ 
						if (chks44[i].checked){ 
							hasChecked44All = true; 
							break; 
						} 
					} 
						
					if(hasChecked44All == false){
						valordocumentacao44Info.text("Marque uma opção");
						valordocumentacao44Info.addClass("error");
						return false;
					}else{
						valordocumentacao44Info.text("ok");
						valordocumentacao44Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 45 PARA VALIDAR CHECKEBOXES DE ANALISE PARTE 2
				function valorAnalise45(){
					for(var i = 0; i < chks45.length; i++){ 
						if (chks45[i].checked){ 
							hasChecked45All = true; 
							break; 
						} 
					} 
						
					if(hasChecked45All == false){
						valordocumentacao45Info.text("Marque uma opção");
						valordocumentacao45Info.addClass("error");
						return false;
					}else{
						valordocumentacao45Info.text("ok");
						valordocumentacao45Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 46 PARA VALIDAR CHECKEBOXES DE ANALISE PARTE 2
				function valorAnalise46(){
					for(var i = 0; i < chks46.length; i++){ 
						if (chks46[i].checked){ 
							hasChecked46All = true; 
							break; 
						} 
					} 
						
					if(hasChecked46All == false){
						valordocumentacao46Info.text("Marque uma opção");
						valordocumentacao46Info.addClass("error");
						return false;
					}else{
						valordocumentacao46Info.text("ok");
						valordocumentacao46Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 47 PARA VALIDAR CHECKEBOXES DE ANALISE PARTE 2
				function valorAnalise47(){
					for(var i = 0; i < chks47.length; i++){ 
						if (chks47[i].checked){ 
							hasChecked47All = true; 
							break; 
						} 
					} 
						
					if(hasChecked47All == false){
						valordocumentacao47Info.text("Marque uma opção");
						valordocumentacao47Info.addClass("error");
						return false;
					}else{
						valordocumentacao47Info.text("ok");
						valordocumentacao47Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO VALIDAR CAMPO DE SELECT DE ANALISE PARTE 2
				function validateQuantidadeanalise(){
					if(quantidadeanalise.val()==0){
						quantidadeanalise.addClass("error");
						quantidadeanaliseInfo.text("Selecione uma opção");
						quantidadeanaliseInfo.addClass("error");
						return false;
					}else{
						quantidadeanalise.removeClass("error");
						quantidadeanaliseInfo.text("ok");
						quantidadeanaliseInfo.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 48 PARA VALIDAR CHECKEBOXES DE ANALISE PARTE 2
				function valorAnalise48(){
					for(var i = 0; i < chks48.length; i++){ 
						if (chks48[i].checked){ 
							hasChecked48All = true; 
							break; 
						} 
					} 
						
					if(hasChecked48All == false){
						legendaInfo.text("Marque uma opção");
						legendaInfo.addClass("error");
						return false;
					}else{
						legendaInfo.text("ok");
						legendaInfo.removeClass("error");
						return true;
					}
				}
				
			});
			
			
			
			// VALIDACAO DO FORM DE IMPRESSAO, FINALIZACAO E ENTREGA AO REQUERENTE =======================================================================================>>>>
			$(document).ready(function(){
				
				// VARIAVEIS GLOBAIS
				var frmprint = $("#frmprint");
				var dataEntregueAlvara = $("#data_entregue_alvara");
				
				dataEntregueAlvara.blur(validateDataEntregueAlvara);
				dataEntregueAlvara.keyup(validateDataEntregueAlvara);
				
				frmprint.submit(function(){
					if(validateDataEntregueAlvara())
						return true
					else
						return false
				});

				function validateDataEntregueAlvara(){
					if(dataEntregueAlvara.val().length < 10){
						dataEntregueAlvara.addClass("error");
						return false;
					}else{
						dataEntregueAlvara.removeClass("error");
						return true;
					}
				}
				
			});

			
			// VALIDACAO DO FORM DE UPLOAD DE FOTOS	 =======================================================================================>>>>
			$(document).ready(function(){
				
				// VARIAVEIS GLOBAIS
				var frmupload = $("#frmupload");
				var observacaoFiscal = $("#observacaofiscal");
				var observacaoFiscalInfo = $("#observacaofiscal_info");
				var chks = document.getElementsByName('valor_situacao_alvara'); 
				var valorSituacaoAlvaraInfo = $("#valor_situacao_alvara_info");
				var hasChecked = false;
				
				observacaoFiscal.blur(validateObservacaoFiscal);
				observacaoFiscal.keyup(validateObservacaoFiscal);
				
				frmupload.submit(function(){
					if(validateObservacaoFiscal() & SituacaoAlvara())
						return true
					else
						return false
				});
				
				function validateObservacaoFiscal(){
					if(observacaoFiscal.val().length < 5){
						observacaoFiscal.addClass("error");
						observacaoFiscalInfo.text("Digite pelo menos 5 caracteres.");
						observacaoFiscalInfo.addClass("error");
						return false;
					}else{
						observacaoFiscal.removeClass("error");
						observacaoFiscalInfo.text("ok");
						observacaoFiscalInfo.removeClass("error");
						return true;
					}
				}
				
				function SituacaoAlvara(){
					for(var i = 0; i < chks.length; i++){ 
						if (chks[i].checked){ 
							hasChecked = true; 
							break; 
						} 
					} 
						
					if(hasChecked == false){
						valorSituacaoAlvaraInfo.text("Marque a opção");
						valorSituacaoAlvaraInfo.addClass("error");
						return false;
					}else{
						valorSituacaoAlvaraInfo.text("ok");
						valorSituacaoAlvaraInfo.removeClass("error");
						return true;
					}
				}
				
			});
			
			// VALIDACAO DO FORM DE CHECAGEM PRIMEIRA ANALISE DE DOCUMENTOS ENTREGUES DESMEMBRAMENTOS==================================================================================================>>>>
			$(document).ready(function(){
				
				// VARIAVEIS GLOBAIS
				var frmchecagem2processo = $("#frmchecagem2processo");
				var chks11 = document.getElementsByName('valor_documentacao1'); 
				var chks12 = document.getElementsByName('valor_documentacao2'); 
				var chks13 = document.getElementsByName('valor_documentacao3'); 
				var chks14 = document.getElementsByName('valor_documentacao4'); 
				var chks15 = document.getElementsByName('valor_documentacao5');
				var chks16 = document.getElementsByName('valor_documentacao6');
				var chks17 = document.getElementsByName('valor_documentacao7');
				var chks18 = document.getElementsByName('valor_situacao_desmembramento');
				var chks19 = document.getElementsByName('valor_documentacao8');
				var hasChecked1All = false;
				var hasChecked2All = false;
				var hasChecked3All = false;
				var hasChecked4All = false;
				var hasChecked5All = false;
				var hasChecked6All = false;
				var hasChecked7All = false;
				var hasChecked8All = false;
				var hasChecked9All = false;
				var valordocumentacao1Info = $("#valor_documentacao1_info");
				var valordocumentacao2Info = $("#valor_documentacao2_info");
				var valordocumentacao3Info = $("#valor_documentacao3_info");
				var valordocumentacao4Info = $("#valor_documentacao4_info");
				var valordocumentacao5Info = $("#valor_documentacao5_info");
				var valordocumentacao6Info = $("#valor_documentacao6_info");
				var valordocumentacao7Info = $("#valor_documentacao7_info");
				var valordocumentacao8Info = $("#valor_documentacao8_info");
				var valorsituacaoalvaraInfo = $("#valor_situacao_desmembramento_info");
				
				frmchecagem2processo.submit(function(){
					if(valorDocumentacao1() & valorDocumentacao2() & valorDocumentacao3() & valorDocumentacao4() & valorDocumentacao5() & valorDocumentacao6() & valorDocumentacao7() & valorDocumentacao8() & valorSituacaoAlvara())
						return true
					else
						return false
				});

				// FUNCAO 1 PARA VALIDAR CHECKEBOXES DE ANALISE
				function valorDocumentacao1(){
					for(var i = 0; i < chks11.length; i++){ 
						if (chks11[i].checked){ 
							hasChecked1All = true; 
							break; 
						} 
					} 
						
					if(hasChecked1All == false){
						valordocumentacao1Info.text("Marque uma opção");
						valordocumentacao1Info.addClass("error");
						return false;
					}else{
						valordocumentacao1Info.text("ok");
						valordocumentacao1Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 2 PARA VALIDAR CHECKEBOXES DE ANALISE
				function valorDocumentacao2(){
					for(var i = 0; i < chks12.length; i++){ 
						if (chks12[i].checked){ 
							hasChecked2All = true; 
							break; 
						} 
					} 
						
					if(hasChecked2All == false){
						valordocumentacao2Info.text("Marque uma opção");
						valordocumentacao2Info.addClass("error");
						return false;
					}else{
						valordocumentacao2Info.text("ok");
						valordocumentacao2Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 3 PARA VALIDAR CHECKEBOXES DE ANALISE
				function valorDocumentacao3(){
					for(var i = 0; i < chks13.length; i++){ 
						if (chks13[i].checked){ 
							hasChecked3All = true; 
							break; 
						} 
					} 
						
					if(hasChecked3All == false){
						valordocumentacao3Info.text("Marque uma opção");
						valordocumentacao3Info.addClass("error");
						return false;
					}else{
						valordocumentacao3Info.text("ok");
						valordocumentacao3Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 4 PARA VALIDAR CHECKEBOXES DE ANALISE
				function valorDocumentacao4(){
					for(var i = 0; i < chks14.length; i++){ 
						if (chks14[i].checked){ 
							hasChecked4All = true; 
							break; 
						} 
					} 
						
					if(hasChecked4All == false){
						valordocumentacao4Info.text("Marque uma opção");
						valordocumentacao4Info.addClass("error");
						return false;
					}else{
						valordocumentacao4Info.text("ok");
						valordocumentacao4Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 5 PARA VALIDAR CHECKEBOXES DE ANALISE
				function valorDocumentacao5(){
					for(var i = 0; i < chks15.length; i++){ 
						if (chks15[i].checked){ 
							hasChecked5All = true; 
							break; 
						} 
					} 
						
					if(hasChecked5All == false){
						valordocumentacao5Info.text("Marque uma opção");
						valordocumentacao5Info.addClass("error");
						return false;
					}else{
						valordocumentacao5Info.text("ok");
						valordocumentacao5Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 6 PARA VALIDAR CHECKEBOXES DE ANALISE
				function valorDocumentacao6(){
					for(var i = 0; i < chks16.length; i++){ 
						if (chks16[i].checked){ 
							hasChecked6All = true; 
							break; 
						} 
					} 
						
					if(hasChecked6All == false){
						valordocumentacao6Info.text("Marque uma opção");
						valordocumentacao6Info.addClass("error");
						return false;
					}else{
						valordocumentacao6Info.text("ok");
						valordocumentacao6Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 7 PARA VALIDAR CHECKEBOXES DE ANALISE
				function valorDocumentacao7(){
					for(var i = 0; i < chks17.length; i++){ 
						if (chks17[i].checked){ 
							hasChecked7All = true; 
							break; 
						} 
					} 
						
					if(hasChecked7All == false){
						valordocumentacao7Info.text("Marque uma opção");
						valordocumentacao7Info.addClass("error");
						return false;
					}else{
						valordocumentacao7Info.text("ok");
						valordocumentacao7Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 7 PARA VALIDAR CHECKEBOXES DE ANALISE
				function valorDocumentacao8(){
					for(var i = 0; i < chks19.length; i++){ 
						if (chks19[i].checked){ 
							hasChecked9All = true; 
							break; 
						} 
					} 
						
					if(hasChecked9All == false){
						valordocumentacao8Info.text("Marque uma opção");
						valordocumentacao8Info.addClass("error");
						return false;
					}else{
						valordocumentacao8Info.text("ok");
						valordocumentacao8Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 8 PARA VALIDAR CHECKEBOXES DE ANALISE
				function valorSituacaoAlvara(){
					for(var i = 0; i < chks18.length; i++){ 
						if (chks18[i].checked){ 
							hasChecked8All = true; 
							break; 
						} 
					} 
						
					if(hasChecked8All == false){
						valorsituacaoalvaraInfo.text("Marque uma opção");
						valorsituacaoalvaraInfo.addClass("error");
						return false;
					}else{
						valorsituacaoalvaraInfo.text("ok");
						valorsituacaoalvaraInfo.removeClass("error");
						return true;
					}
				}

				
			});
			
			
			// VALIDACAO DO FORM DE  ANALISE / APROVACAO DE PROCESSOS / DESMEMBRAMENTOS==================================================================================================>>>>
			$(document).ready(function(){
				
				// VARIAVEIS GLOBAIS
				var frmanaliseprocesso = $("#frmanaliseprocesso");
				var chks1 = document.getElementsByName('valor_analise1'); 
				var chks2 = document.getElementsByName('valor_analise2'); 
				var chks3 = document.getElementsByName('valor_analise3'); 
				var chks4 = document.getElementsByName('legenda');
				
				var hasChecked1All = false;
				var hasChecked2All = false;
				var hasChecked3All = false;
				var hasChecked4All = false;

				var valorAnalise1Info = $("#valor_analise1_info");
				var valorAnalise2Info = $("#valor_analise2_info");
				var valorAnalise3Info = $("#valor_analise3_info");
				var legendaInfo = $("#legenda_info");
								
				frmanaliseprocesso.submit(function(){
					if(valorAnalise1() & valorAnalise2() & valorAnalise3() & legenda())
						return true
					else
						return false
				});

				// FUNCAO 1 PARA VALIDAR CHECKEBOXES DE ANALISE
				function valorAnalise1(){
					for(var i = 0; i < chks1.length; i++){ 
						if (chks1[i].checked){ 
							hasChecked1All = true; 
							break; 
						} 
					} 
						
					if(hasChecked1All == false){
						valorAnalise1Info.text("Marque uma opção");
						valorAnalise1Info.addClass("error");
						return false;
					}else{
						valorAnalise1Info.text("ok");
						valorAnalise1Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 2 PARA VALIDAR CHECKEBOXES DE ANALISE
				function valorAnalise2(){
					for(var i = 0; i < chks2.length; i++){ 
						if (chks2[i].checked){ 
							hasChecked2All = true; 
							break; 
						} 
					} 
						
					if(hasChecked2All == false){
						valorAnalise2Info.text("Marque uma opção");
						valorAnalise2Info.addClass("error");
						return false;
					}else{
						valorAnalise2Info.text("ok");
						valorAnalise2Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 3 PARA VALIDAR CHECKEBOXES DE ANALISE
				function valorAnalise3(){
					for(var i = 0; i < chks3.length; i++){ 
						if (chks3[i].checked){ 
							hasChecked3All = true; 
							break; 
						} 
					} 
						
					if(hasChecked3All == false){
						valorAnalise3Info.text("Marque uma opção");
						valorAnalise3Info.addClass("error");
						return false;
					}else{
						valorAnalise3Info.text("ok");
						valorAnalise3Info.removeClass("error");
						return true;
					}
				}
				
				// FUNCAO 4 PARA VALIDAR CHECKEBOXES DE ANALISE
				function legenda(){
					for(var i = 0; i < chks4.length; i++){ 
						if (chks4[i].checked){ 
							hasChecked4All = true; 
							break; 
						} 
					} 
						
					if(hasChecked4All == false){
						legendaInfo.text("Marque uma opção");
						legendaInfo.addClass("error");
						return false;
					}else{
						legendaInfo.text("ok");
						legendaInfo.removeClass("error");
						return true;
					}
				}
				
			});
			
			
			// VALIDACAO DO FORM DE ALTERACAO DE PROCESSO====================================================================================>>>
			$(document).ready(function(){
				// VARIAVEIS GLOBAIS
				var frmalterar = $("#frmalterar");
				// CAMPO
				var campo = $("#campoaltsproc");
				var campoInfo = $("#campoaltsproc_info");
				// VALOR NOVO
				var atual = $("#dado_atual");
				var atualInfo = $("#dado_atual_info");
			
				campo.blur(validateCampo);

				atual.keyup(validateAtual);

				frmalterar.submit(function(){
					if(validateCampo() & validateAtual())
						return true
					else
						return false
				});
				//  CAMPO
				function validateCampo(){
					if(campo.val().length < 5){
						campo.addClass("error");
						campoInfo.text("Digite um campo desejado");
						campoInfo.addClass("error");
						return false;
					}else{
						campo.removeClass("error");
						campoInfo.text("ok");
						campoInfo.removeClass("error");
						return true;
					}
				}
				// VALOR NOVO
				function validateAtual(){
					if(atual.val().length < 2){
						atual.addClass("error");
						atualInfo.text("Digite o novo valor a conter o campo solicitado");
						atualInfo.addClass("error");
						return false;
					}else{
						atual.removeClass("error");
						atualInfo.text("ok");
						atualInfo.removeClass("error");
						return true;
					}
				}
			});
			
			
			
		// VALIDACAO DO FORM DE CADASTRO DE ALVARA====================================================================================>>>
			$(document).ready(function(){
				// VARIAVEIS GLOBAIS
				var frmcondominio = $("#frmcondominio");
				// NOME DO PROPRIETARIO
				var nomeproprietario = $("#nome_proprietario");
				var nomeproprietarioInfo = $("#nome_proprietario_info");
				// PESSOA FISICA OU JURIDICA
				var chks = document.getElementsByName("pessoa");
				var hasChecked = false;
				var pessoaInfo = $("#pessoa_info");
				// AUTORIA
				var autoria = $("#autoria");
				var autoriaInfo = $("#autoria_info");
				// TIPO PROFISSIONAL - AUTORIA
				var chks2 = document.getElementsByName('tipo_profissional_autoria'); 
				var hasChecked2 = false; 
				var tipoprofissionalInfo = $("#tipo_profissional_autoria_info");
				// ENDERECO
				var endereco = $("#endereco");
				var enderecoInfo = $("#endereco_info");
				// BAIRRO
				var bairro = $("#bairro");
				var bairroInfo = $("#bairro_info");
				// FINALIDADE DE OBRA
				var finalidadeobra = $("#finalidade_obra");
				var finalidadeobraInfo = $("#finalidade_obra_info");
				// SITUACAO DO TERRENO
				var chks3 = document.getElementsByName('situacaoterreno'); 
				var hasChecked3 = false; 
				var situacaoTerrenoInfo = $("#situacao_terreno_info");
				// AREA DO TERRENO
				var areaterreno = $('#area_terreno');
				var areaterrenoInfo = $('#area_terreno_info');
				
                                											
				nomeproprietario.blur(validateNomeproprietario);
				autoria.blur(validateAutoria);
				endereco.blur(validateEndereco);
				bairro.blur(validateBairro);
				finalidadeobra.blur(validateFinalidadeobra);
				areaterreno.blur(validateAreaterreno);
                                
				nomeproprietario.keyup(validateNomeproprietario);
				autoria.keyup(validateAutoria);
				endereco.keyup(validateEndereco);
				bairro.keyup(validateBairro);
				finalidadeobra.keyup(validateFinalidadeobra);
				areaterreno.keyup(validateAreaterreno);
                                
                                
				frmcondominio.submit(function(){
					if(validateNomeproprietario() & validatePessoa() & validateAutoria() & validateTipoprofissional() & validateEndereco() & validateBairro() & validateFinalidadeobra() & validateSituacaoTerreno() & validateAreaterreno())
						return true
					else
						return false
				});
                                
				// PROPRIETARIO
				function validateNomeproprietario(){
					if(nomeproprietario.val().length < 5){
						nomeproprietario.addClass("error");
						nomeproprietarioInfo.text("Digite o nome do proprietário (mínino de caracteres 5)");
						nomeproprietarioInfo.addClass("error");
						return false;
					}else{
						nomeproprietario.removeClass("error");
						nomeproprietarioInfo.text("ok");
						nomeproprietarioInfo.removeClass("error");
						return true;
					}
				}
				// PESSOA FISICA OU JURIDICA DO PROPRIETARIO
				function validatePessoa(){
					for(var i = 0; i < chks.length; i++){ 
						if (chks[i].checked){ 
							hasChecked = true; 
							break; 
						} 
					} 
					if(hasChecked == false){
						pessoaInfo.text("Marque uma opção");
						pessoaInfo.addClass("error");
						return false;
					}else{
						pessoaInfo.text("ok");
						pessoaInfo.removeClass("error");
						return true;
					}
				}
				// AUTORIA 
				function validateAutoria(){
					if(autoria.val().length < 5){
						autoria.addClass("error");
						autoriaInfo.text("Digite o Autor (Mínino de caracteres 5)");
						autoriaInfo.addClass("error");
						return false;
					}else{
						autoria.removeClass("error");
						autoriaInfo.text("ok");
						autoriaInfo.removeClass("error");
						return true;
					}
				}
				// ENGENHEIRO OU ARQUITETO - AUTORIA
				function validateTipoprofissional(){
					for(var i = 0; i < chks2.length; i++){ 
						if (chks2[i].checked){ 
							hasChecked2 = true; 
							break; 
						} 
					} 
					if(hasChecked2 == false){
						tipoprofissionalInfo.text("Marque uma opção");
						tipoprofissionalInfo.addClass("error");
						return false;
					}else{
						tipoprofissionalInfo.text("ok");
						tipoprofissionalInfo.removeClass("error");
						return true;
					}
				}
				// ENDERECO
				function validateEndereco(){
					if(endereco.val().length < 5){
						endereco.addClass("error");
						enderecoInfo.text("Digite o Endereço (Mínino de caracteres 5)");
						enderecoInfo.addClass("error");
						return false;
					}else{
						endereco.removeClass("error");
						enderecoInfo.text("ok");
						enderecoInfo.removeClass("error");
						return true;
					}
				}
				// BAIRRO
				function validateBairro(){
					if(bairro.val().length < 5){
						bairro.addClass("error");
						bairroInfo.text("Digite o Bairro (Mínino de caracteres 5)");
						bairroInfo.addClass("error");
						return false;
					}else{
						bairro.removeClass("error");
						bairroInfo.text("ok");
						bairroInfo.removeClass("error");
						return true;
					}
				}
				// FINALIDADE DA OBRA
				function validateFinalidadeobra(){
					if(finalidadeobra.val()==0){
						finalidadeobra.addClass("error");
						finalidadeobraInfo.text("Selecione uma Finalidade de Obra");
						finalidadeobraInfo.addClass("error");
						return false;
					}else{
						finalidadeobra.removeClass("error");
						finalidadeobraInfo.text("ok");
						finalidadeobraInfo.removeClass("error");
						return true;
					}
				}	
				// SITUACAO DO TERRENO
				function validateSituacaoTerreno(){
					for(var i = 0; i < chks3.length; i++){ 
						if (chks3[i].checked){ 
							hasChecked3 = true; 
							break; 
						} 
					} 
					if(hasChecked3 == false){
						situacaoTerrenoInfo.text("Marque uma opção");
						situacaoTerrenoInfo.addClass("error");
						return false;
					}else{
						situacaoTerrenoInfo.text("ok");
						situacaoTerrenoInfo.removeClass("error");
						return true;
					}
				}
				// AREA DO TERRENO
				function validateAreaterreno(){
					if(areaterreno.val().length < 1){
						areaterreno.addClass("error");
						areaterrenoInfo.text("Digite a área do terreno");
						areaterrenoInfo.addClass("error");
						return false;
					}else{
						areaterreno.removeClass("error");
						areaterrenoInfo.text("ok");
						areaterrenoInfo.removeClass("error");
						return true;
					}
				}
								
			});