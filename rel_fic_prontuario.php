﻿<?php
require_once 'config.php';
require_once 'classes/materiais.class.php';

$m = new Materiais();


if(isset($_GET['id']) && !empty($_GET['id'])){


    $info = $m->getSaidaMateriais($_GET['id']);


    $produtosReqSaida = $m->getItensReqSaidaImp($_GET['id']);


}


?>

<style type="text/css">
	body, div{

		
	}

		@media print{
			.botao-imprimir{
				display: none;
			}
		}

		.botao-imprimir{
			margin-top: 30px;
			width: 62px;
			height: 16px;
			cursor: pointer;
			font-family: Arial, Helvetica, sans-serif;
			font-size: 14px;
			color: #050505;
			padding: 10px 20px;
			background: -webkit-gradient(
			linear, left top, left bottom,
			from(#ffffff),
			color-stop(0.50, #c7d95f),
			color-stop(0.50, #add136),
			to(#6d8000));
		background: linear-gradient(
			top,
			#ffffff 0%,
			#c7d95f 50%,
			#add136 50%,
			#6d8000);
		border-radius: 14px;
		border: 1px solid #6d8000;
		box-shadow:
			0px 1px 3px rgba(000,000,000,0.5),
			inset 0px 0px 2px rgba(255,255,255,1);
		text-shadow:
			0px -1px 0px rgba(000,000,000,0.2),
			0px 1px 0px rgba(255,255,255,0.4);


	}
	
</style>

<!-- Botão imprimir -->

<div align="center" id="funcoes"  style="margin-left: 580px; display: inline-block; margin-top: 36px; margin-bottom: 20px;" >
		<a class="botao-imprimir" onclick="document.getElementById('funcoes').style.display = 'none'; window.print();"><img border="0">Imprimir</a>
	</div>
	<div id="funcoes"  align="center" style="display: inline-block; margin-top: 36px; " >
		<a class="botao-imprimir" onclick="document.getElementById('funcoes').style.display = 'none';"  href="saida_material.php" ><img border="0">Voltar</a>
	<br>
	</div>
 <!-- onload="window.print();" -->
<body style="font-family:arial;" onload="window.print();" >
		<div id="funcoes" align="center" style="display: block;">
				<br>
					<img src="assets/img/logos/logoplem.jpeg">
				<br><hr width="800">
		</div>
		
		<table align="center" border="0" cellpadding="0" cellspacing="0" width="800">
			<td  style="font-size:14px;">
				<!-- <h2 align="center">PREFEITURA MUN. DE LUIS EDUARDO MAGALHAES</h2> -->
				<b>Endereço: </b>RUA OCTOGONAL, 684<br/>
				<b>Bairro: </b>JARDIM IMPERIAL<br/>
				<b>Cidade: </b>Luís Eduardo Magalhães - Bahia<br/>
				<b>CEP: </b>47.850-000
			</td>
		</table>


		<hr width="800"></br>

		<table >
			<div id="funcoes" align="center" style="display: block;">
				<h2 style="color: #299">FICHA DE ATENDIMENTO AMBULATÓRIAL</h2>
			</div>
		</table></br>


		<div  align="center" class="container">
			


			<!-- INÍCIO - FICHA DE ATENDIMENTO  -->

			
				
			<table style="border: 1px solid black; border-spacing: 0;" class="bordered striped centered">
			<tr>
				<td>
					<h3 align="left">Paciente: José Ribeiro da Silva</h3>
				</td>
			</tr>

				<thead >
				
					<tr style="border: 1px solid black;">
						<td colspan="6"  align="center" style="background: #D5D8DC"><strong>Unidade Prestadora</strong></td>
					</tr>
				</thead>
				<tbody>
					<td >
						Unidade: <b>SMS POLICLÍNICA MUNICIPAL DE SAUDE</b></br>
						Endereço: <b>Av. Caramuru </b></br>
						Bairro: <b>São Coutinho</b></br>
						Cidade: <b>Luis Eduardo Magalhães-Ba</b></br>
						CNES: <b>6298354</b>
					</td>

					<td style="vertical-align: top;">
						&nbsp;&nbsp;&nbsp;&nbsp;Data e Hora: <b>27/05/2020 11:00</b></br>
						&nbsp;&nbsp;&nbsp;&nbsp;Estado: <b>Bahia</b>
					</td></br>					
				
				</tbody>

				<thead >
					<tr style="border: 1px solid black;">
						<td colspan="6" align="center" style="background: #D5D8DC"><strong>Identificação do Paciente</strong></td>
						
					</tr>
				</thead>
				<tbody>
					<td style="border-top: 1px solid black; border-bottom:1px solid #999; padding-left:30px; padding:5px;">
						CGS: <b>43111 - AGACY JOAO DA SILVA</b></br>
						Endereço:<b>Rua Pau Brasil Qd 95 Lt 19</b></br>
						Bairro:<b>Jardim das Acacias 1ª Etapa</b></br>
						Cidade:<b>Luis Eduardo Magalhães</b></br>
						Telefone:<b>77 9.9846-1416</b></br>
						Mãe: <b>MARIA DIOMIRA GAMA</b></br>
						Nascimento: <b>22/01/1971</b>
					</td>
					<td colspan="6" style= "border-top: 1px solid black; border-bottom:1px solid #999; padding-left:30px; padding:5px;vertical-align:top;">
					&nbsp;&nbsp;&nbsp;&nbsp;Nome do pai: <b> Homem de Ferro</b></br>
					&nbsp;&nbsp;&nbsp;&nbsp;Nome da mãe:<b> Shyrra Bolden</b></br>
					&nbsp;&nbsp;&nbsp;&nbsp;Contato:<b> 77 9.9898-0099 (Juliano Soet)</b></br>
					</td>
				</tbody>

				<thead >
					<tr style="border: 1px solid black;">
						<td colspan="6" align="center" style="background: #D5D8DC"><strong>Identificação do Médico</strong></td>						
					</tr>
				</thead>
				<tbody>
					<tr>
						<td style="vertical-align:top;">
							Nome: <b> Lucas Mirt</b></br>
							CRM: <b>020103</b></br>
							Contato: <b>77 8.0102-9655</b>
						</td>
						<td >
						&nbsp;&nbsp;&nbsp;&nbsp;Atuação: <b>02154</b></br>
						&nbsp;&nbsp;&nbsp;&nbsp;Especialidade: <b>Ortopedia</b></br>	
						&nbsp;&nbsp;&nbsp;&nbsp;Registro CRG: <b>6522</b>
						</td>
					</tr>
					
				</tbody>

				<thead >
					<tr style="border: 1px solid black;">
						<td colspan="4" align="center" style="background: #D5D8DC; border-top: 1px solid black;"><strong>Atendimento</strong></td>						
					</tr>
				</thead>
				<tbody>
				<tr  >
					<td style="border: 1px solid black; vertical-align:top;">Pressão:    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; / </td>
					<td style="border: 1px solid black;vertical-align:top;"> Peso:  </td>
					<td style="border: 1px solid black;vertical-align:top;"> Altura:        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
					<td style="border: 1px solid black;vertical-align:top;"> Temperatuda:   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>				
				</tr>
				<tr  >
					<td style="border: 1px solid black; vertical-align:top;">Gestante: &nbsp;&nbsp;&nbsp;&nbsp; [ &nbsp;&nbsp; ] SIM &nbsp;&nbsp;&nbsp;&nbsp; [ &nbsp;&nbsp; ] NÃO   </td>
					<td colspan="3" style="border: 1px solid black;vertical-align:top;">Glicemia:  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; [ &nbsp;&nbsp; ] Jejum &nbsp;&nbsp;&nbsp;&nbsp;   [ &nbsp;&nbsp; ] Pós Prandial</td>
				</tr>
				</tbody>

				<thead >
					<tr style="border: 1px solid black;">
						<td colspan="6" align="center" style="background: #D5D8DC; border-top: 1px solid black;"><strong>Diagnosticado</strong></td>						
					</tr>
				</thead>
				<tr>
					<td style="padding:30px;"></td>
				</tr>
				

				<thead width="100%"; >
									
					<tr align="center"  >						
						<td  style="border: 1px solid black; background: #D5D8DC; border-top: 1px solid black;"><strong>Anamnese</strong></td>
						<td style="border: 1px solid black; background: #D5D8DC; border-top: 1px solid black;"><strong>CID</strong></td>
						<td colspan="3" style="border: 1px solid black; background: #D5D8DC; border-top: 1px solid black;"><strong>Procedimento</strong></td>
					</tr>
					<tr>						
						<td rowspan="5"  style="border: 1px solid black; "> </td>												
					</tr>					
					
					<tr>
						<td align="left"  colspan="1" style="border: 1px solid black; padding: 15px;">  </td>
						<td align="left"  colspan="2" style="border: 1px solid black; padding: 15px;">  </td>
					</tr>
					<tr>
						<td align="left"  colspan="1" style="border: 1px solid black; padding: 15px;">  </td>
						<td align="left"  colspan="2" style="border: 1px solid black; padding: 15px;">  </td>
					</tr>
					<tr>
						<td align="left"  colspan="1" style="border: 1px solid black; padding: 15px;">  </td>
						<td align="left"  colspan="2" style="border: 1px solid black; padding: 15px;">  </td>
					</tr>
					<tr>
						<td align="left"  colspan="1" style="border: 1px solid black; padding: 15px;">  </td>
						<td align="left"  colspan="2" style="border: 1px solid black; padding: 15px;">  </td>
					</tr>
					
					<tr>
						<td align="center" rowspan="4" colspan="1" style="border: 1px solid black; padding:20px;">Assinatura e Carimbo  </td>
						
					</tr>
						 <td align="center" rowspan="" colspan="3" style="border: 1px solid black; padding:10px;">Data de Atendimento: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   _____/_______/__________      </td>
					</tr>
					<tr>
						 <td align="center" rowspan="" colspan="3" style="border: 1px solid black; padding:10px;">Hora de Atendimento:  &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   <b>:</b>     </td>
					</tr>
						
						<td align="left"  colspan="3" style="border: 1px solid black; padding: 10px;">Assinatura Paciente/Responsável:  </td>
					</tr>
						
				</thead>
				<tbody>
				

				</tbody>

			</table>
		</div>

</body>
</html>
		
	
	
</body>
</html>