﻿<!DOCTYPE html>
<html>

<head>
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <title>X-almoxariafdo</title>
  <meta name="keywords" content="HTML5 Bootstrap 3 Admin Template UI Theme" />
  <meta name="description" content="TemplateMonster - A Responsive HTML5 Admin UI Framework">
  <meta name="author" content="TemplateMonster">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Font CSS (Via CDN) -->
  <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'>

  <!-- Theme CSS -->
  <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">

  <!-- Admin Forms CSS -->
  <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">

  <!-- Favicon -->
  <link rel="shortcut icon" href="assets/img/favicon.ico">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
   <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
   <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
   <![endif]-->
</head>

<body class="external-page sb-l-c sb-r-c">

  <!-- Start: Main -->
  <div id="main" class="animated fadeIn">

    <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- begin canvas animation bg -->
      <div id="canvas-wrapper">
        <canvas id="demo-canvas"></canvas>
      </div>

      <!-- Begin: Content -->
      <section id="content">

        <div class="admin-form theme-info" id="login1">

          <div class="row mb15 table-layout">

            <div class="col-xs-6 va-m pln">
              <a href="dashboard.html" title="Return to Dashboard">
                <img src="assets/img/logos/logotipo_vale.jpeg" title="TemplateMonster Logo" class="img-responsive ">
              </a>
            </div>
          </div>

          <div class="panel panel-info mt10 br-n">
            <!-- end .form-header section -->
              <div class="panel-body bg-light p30">
                <div class="row">
                  <?php 
                  require_once  'config.php';
                  require_once 'classes/usuario.class.php';


                  $u = new Usuarios();

                  if(isset($_POST['email']) && !empty($_POST['email'])){
                    $email = addslashes($_POST['email']);
                    $senha = addslashes($_POST['senha']);




                    if($u->login($email, $senha)){


                      


                      ?>
                      <script type="text/javascript">window.location.href="saida-materiais.php"</script>
                      <?php 

                    }else{
                      ?>
                      <div class="col-sm-12 pr30">
                        <div class="alert alert-danger alert-dismissable">
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                          <i class="fa fa-remove pr10"></i>
                          E-mail e/ou senha
                          <a href="#" class="alert-link">estão incorretos</a> 
                        </div>
                      </div>
                      
                      <?php    

                    }

                  }

                    ?>
                    
                  <form method="POST"><!-- início do form -->

					
                      <div class="section">
                        <label for="email" class="field-label text-muted fs18 mb10">E-mail</label>
                        <label for="email" class="field prepend-icon">
                          <input type="text" name="email" id="email" class="gui-input" placeholder="Informe seu e-mail">
                          <label for="email" class="field-icon">
                            <i class="fa fa-user"></i>
                          </label>
                        </label>
                      </div>

                      <div class="section">
                        <label for="senha" class="field-label text-muted fs18 mb10">Senha</label>
                        <label for="senha" class="field prepend-icon">
                          <input type="password" name="senha" id="senha" class="gui-input" placeholder="Informe a senha">
                          <label for="password" class="field-icon">
                            <i class="fa fa-lock"></i>
                          </label>
                        </label>
                      </div>
                      <input type="submit" class="button  btn-primary mr10 pull-left" value="Entrar">
					
                  </form>
                    

      </section>
      <!-- End: Content -->

    </section>
    <!-- End: Content-Wrapper -->

  </div>
  <!-- End: Main -->

  <!-- BEGIN: PAGE SCRIPTS -->

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- CanvasBG Plugin(creates mousehover effect) -->
  <script src="vendor/plugins/canvasbg/canvasbg.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>

  <!-- Page Javascript -->
  <script type="text/javascript">
  jQuery(document).ready(function() {

    "use strict";

    // Init Theme Core      
    Core.init();

    // Init CanvasBG and pass target starting location
    CanvasBG.init({
      Loc: {
        x: window.innerWidth / 2,
        y: window.innerHeight / 3.3
      },
    });

  });
  </script>

  <!-- END: PAGE SCRIPTS -->

</body>

</html>
