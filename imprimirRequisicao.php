﻿<?php
require_once 'config.php';
require_once 'classes/materiais.class.php';

$m = new Materiais();


if(isset($_GET['id']) && !empty($_GET['id'])){


    $info = $m->getSaidaMateriais($_GET['id']);


    $produtosReqSaida = $m->getItensReqSaidaImp($_GET['id']);


}


?>



<body style="font-family:arial;" onload="window.print();">
	<!-- Botão imprimir -->
	<div id="funcoes" align="center" style="display: block; " >
	<br>
	<a href="#"><img border="0" title="Imprimir"></a>
	<br>
	</div>
	<!-- Início do topo do cabecalho -->
  <table align="center" border="0" cellpadding="0" cellspacing="0" width="800">
	<tr>

		<td colspan="3" width="500" style="padding-left:10px;padding-bottom:10px;">
			<table border="0" cellpadding="10">
	<td width="100" height="100">
		<img src="/assets/img/logos/logo-camara.jpg">
	</td>
	<td width="320" style="font-size:14px;">
		<h2>CAMARA MUNICIPAL DE LUIS EDUARDO MAGALHAES</h2>
		<b>Endereço: </b>RUA OCTOGONAL, 684<br />
		<b>Bairro: </b>JARDIM IMPERIAL<br />
		<b>Cidade: </b>Luís Eduardo Magalhães - Bahia<br />
		<b>CEP: </b>47850-000<br /><br />
			</td>
	<td align="center" width="380">
		<h1>Requisição - <?php echo $info[0]['id']; ?></h1>
	</td>
</table>		
</td>
</tr>
<!-- Fim do topo do cabecalho -->
	<tr>
		<td colspan="3" style="padding:5px;">
			<br />
			<b>Data Saída:</b>	
			<?php             
             	$data_velha = $info[0]['data_baixa'];
                $data_br = date('d-m-Y', strtotime($data_velha));
                $data_baixa = str_replace('-', '/', $data_br);
               echo $data_baixa;

                                   ?><br />

		</td>
	</tr>
	<tr>
		<td colspan="2" style="vertical-align:top;padding:5px;"><b>Requisitado por: </b><?php echo $info[0]['usuario']; ?></td>
	</tr>
	<tr>
		<td colspan="2" style="vertical-align:top;padding:5px;"><b>Departamento Origem:</b> ALMOXARIFADO</td>
	</tr>
	<tr>
		<td colspan="2" style="vertical-align:top;padding:5px;"><b>Departamento Destino:</b>
			<?php echo $info[0]['nome_departamento']; ?>	</td>
	</tr>
	<tr>
		<td colspan="3">
			<table>
				<tr>
					<td width="800" style="background:#E8E8E8;border-top:1px solid #999;border-bottom:1px solid #999;font-size:18px;padding-left:30px;padding:5px;">
						<b>OBSERVAÇÕES</b>
					</td>
				</tr>
				<tr>
					<td><?php echo $info[0]['obs']; ?></td>
					
				</tr>
				<tr>
					<td  style="padding:10px;"></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<table cellpadding="5" cellspacing="0">
									<tr style="font-weight:bold;">
						<td style="border-top:1px solid #999;border-bottom:1px solid #999;" width="50">Código</td>
						<td style="border-top:1px solid #999;border-bottom:1px solid #999;" width="400">Produto</td>
						<td style="border-top:1px solid #999;border-bottom:1px solid #999;" width="200">Valor</td>
						<td style="border-top:1px solid #999;border-bottom:1px solid #999;">Qtd</td>
					</tr>
					<?php $totalProdutos = 0; ?>
					<?php foreach ($produtosReqSaida as $itemProduto): ?>


					<tr>
						<td><?php echo $itemProduto['id'];?></td>
						<td><?php echo $itemProduto['nome_produto'];?></td>
						<td>R$ 0,00</td>
						<td><b style="font-size:20px;"><?php $totalProdutos  = $totalProdutos + $qtdProduto = $itemProduto['qtd_produto']; echo $itemProduto['qtd_produto']; ?></b></td>
					</tr>
				<?php endforeach; ?>
					<tr>
					<td style="border-top:1px solid black;" width="400">
						
					</td>
					<td align="right" colspan="5" style="border-top:1px solid black;">
						<table cellpadding="5">
							<tr>
								<td></td>
								<td></td>
								<td style="font-weight:bold;">Total Produtos: <?php echo $totalProdutos; ?></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<!-- 
			<table align="center" style="color:#444;font-size:12px;padding-top:20px;">
			    <tr>
			        <td>Gerado por Meu Controle - Câmara LEM
			        	<!--Powered by Uemerson A. Santana - <a target="_blank" href="https://www.linkedin.com/in/uemerson-a-santana-9a5004184/">Linkedin</a> | <a href="mailto:uemerson@icloud.com">E-mail</a>
			        </td>
			    </tr>

			</table>	
			-->	
		</td>
	</tr>
</table>
	<table align="center" style="color:#444;font-size:12px;padding-top:20px;">
		<tr>
			<td align="center" width="300" style="border-top: 2px solid;"><strong><h3><?php echo $info[0]['usuario']; ?></h3></strong></td>
		</tr>

	</table>	
</body>
</html>
		
	</table>
	
</body>
</html>