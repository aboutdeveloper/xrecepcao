<?php 
require_once 'header.php';
require_once 'aside.php';
require_once 'classes/chamados.class.php';

$c = new Chamados();
?>

 <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <!-- Start: Topbar -->
      <header id="topbar" class="alt">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-icon">
              <a href="saida-materiais.php">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-active">
              <a href="">Suporte</a>
            </li>
            <li class="crumb-trail">
              <a href="chamados.php">Abrir chamados</a>
            </li>
            <li class="crumb-trail">
              <a href="add-chamado.php">Visualizar chamado</a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">
        <!-- begin: .tray-center -->
        <div class="tray tray-center">
          <div class="mw1000 center-block">
            <!-- Begin: Admin Form -->
            <div class="admin-form">
                    <div class="admin-form theme-primary">
                      <div class="panel heading-border panel-primary">
                        <div class="panel-body bg-light">      
                          <div class="section-divider mb40" id="spy1">
                            <span>Visualizar chamado</span>
                          </div>
                          <?php 

                          if(isset($_GET['id_chamado']) && !empty($_GET['id_chamado'])){
                          $chamado = $c->getChamado($_GET['id_chamado']);



                          }else{

                            ?>
                            <script type="text/javascript">window.location.hef="chamados.php"</script>

                            <?php

                          }




                          ?>
                            <div class="row">
                              <div class="col-md-1">
                                <div class="form-group">
                                  <label for="nome"><b>ID</b></label>
                                  <input type="text" id="nome" class="form-control" disabled="" value="<?php echo $chamado['id'];?>">
                                </div>
                              </div>
                              <div class="col-md-3">
                                <div class="form-group">
                                  <label for="nome"><b>Data de abertura</b></label>
                                  <input type="text" id="nome" class="form-control" value="<?php echo date('d/m/Y',strtotime($chamado['data_abertura']));?>" disabled>
                                </div>
                              </div>
                              <div class="col-md-3">
                                <div class="form-group">
                                  <label for="nome"><b>Data de fechamento</b></label>
                                  <input type="text"  id="nome" class="form-control" value="<?php echo date('d/m/Y',strtotime($chamado['data_fechamento']));?>" disabled>
                                </div>
                              </div>
                              <div class="col-md-3">
                                <div class="form-group">
                                  <label for="nome"><b>Tipo de chamado</b></label>
                                  <input type="text"  id="nome" class="form-control" disabled=""  value="<?php 
                                    switch ($chamado['situacao']) {
                                      case 1:
                                          echo "Atendida";
                                          break;
                                      case 2:
                                          echo "Não Atendida";
                                          break; 

                                       } 
                                   ?>">
                                </div>
                              </div>
                              <div class="col-md-2">
                                <div class="form-group">
                                  <label for="nome"><b>Situação</b></label>
                                  <input type="text"  id="nome" class="form-control" disabled="">
                                </div>
                              </div>
 
                              
                            </div><!-- fim da row -->
                            <div class="row">
                              <div class="col-md-5">
                                <div class="form-group">
                                  <label for="nome"><b>Usuário</b></label>
                                  <input type="text"  id="nome" class="form-control" disabled="" value="<?php echo $chamado['nome_usuario'];?>">
                                </div>
                              </div>
                                                           
                            </div>
                            <div class="row">
                              <div class="col-md-12">
                                <div class="section"><strong>Chamado</strong>
                                  <label class="field prepend-icon">
                                    <textarea class="gui-textarea" id="comment" name="comment" placeholder="Informe sua dúvida, problema, solicitação, críticas ou elogios" disabled=""><?php echo $chamado['chamado'];?></textarea>
                                    <label for="comment" class="field-icon">
                                      <i class="fa fa-comments"></i>
                                    </label>
                                  </label>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-12">
                                <div class="section"><strong>Resposta</strong>
                                  <label class="field prepend-icon">
                                    <textarea class="gui-textarea" id="comment" name="comment" placeholder="Informe sua dúvida, problema, solicitação, críticas ou elogios" disabled=""><?php echo $chamado['resposta'];?></textarea>
                                    <label for="comment" class="field-icon">
                                      <i class="fa fa-comments"></i>
                                    </label>
                                  </label>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-12">
                                <div class="section"><strong>Resolucao</strong>
                                  <label class="field prepend-icon">
                                    <textarea class="gui-textarea" id="comment" name="comment" placeholder="Informe sua dúvida, problema, solicitação, críticas ou elogios" disabled=""><?php echo $chamado['resolucao'];?></textarea>
                                    <label for="comment" class="field-icon">
                                      <i class="fa fa-comments"></i>
                                    </label>
                                  </label>
                                </div>
                              </div>
                            </div>
            </div>
          </div>
        </div>
        <!-- end: .tray-center -->
      </section>
      <!-- End: Content -->
    </section>
  </div>
  <!-- End: Main -->

  <style>
  /* demo page styles */
  body { min-height: 2300px; }
  
  .content-header b,
  .admin-form .panel.heading-border:before,
  .admin-form .panel .heading-border:before {
    transition: all 0.7s ease;
  }
  /* responsive demo styles */
  @media (max-width: 800px) {
    .admin-form .panel-body { padding: 18px 12px; }
    .option-group .option { display: block; }
    .option-group .option + .option { margin-top: 8px; }
  }
  </style>
    

  <!-- BEGIN: PAGE SCRIPTS -->

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  jQuery(document).ready(function() {

    "use strict";

    // Init Theme Core    
    Core.init();

    // Form Switcher
    $('#form-switcher > button').on('click', function() {
      var btnData = $(this).data('form-layout');
      var btnActive = $('#form-elements-pane .admin-form.active');

      // Remove any existing animations and then fade current form out
      btnActive.removeClass('slideInUp').addClass('animated fadeOutRight animated-shorter');
      // When above exit animation ends remove leftover classes and animate the new form in
      btnActive.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
        btnActive.removeClass('active fadeOutRight animated-shorter');
        $('#' + btnData).addClass('active animated slideInUp animated-shorter')
      });
    });

    // Cache several DOM elements
    var pageHeader = $('.content-header').find('b');
    var adminForm = $('.admin-form');
    var options = adminForm.find('.option');
    var switches = adminForm.find('.switch');
    var buttons = adminForm.find('.button');
    var Panel = adminForm.find('.panel');

    // Form Skin Switcher
    $('#skin-switcher a').on('click', function() {
      var btnData = $(this).data('form-skin');

      $('#skin-switcher a').removeClass('item-active');
      $(this).addClass('item-active')

      adminForm.each(function(i, e) {
        var skins = 'theme-primary theme-info theme-success theme-warning theme-danger theme-alert theme-system theme-dark';
        var panelSkins = 'panel-primary panel-info panel-success panel-warning panel-danger panel-alert panel-system panel-dark';
        $(e).removeClass(skins).addClass('theme-' + btnData);
        Panel.removeClass(panelSkins).addClass('panel-' + btnData);
        pageHeader.removeClass().addClass('text-' + btnData);
      });

      $(options).each(function(i, e) {
        if ($(e).hasClass('block')) {
          $(e).removeClass().addClass('block mt15 option option-' + btnData);
        } else {
          $(e).removeClass().addClass('option option-' + btnData);
        }
      });
      $(switches).each(function(i, ele) {
        if ($(ele).hasClass('switch-round')) {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-round switch-' + btnData);
          } else {
            $(ele).removeClass().addClass('switch switch-round switch-' + btnData);
          }
        } else {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-' + btnData);
          } else {
            $(ele).removeClass().addClass('switch switch-' + btnData);
          }
        }

      });
      buttons.removeClass().addClass('button btn-' + btnData);
    });

    setTimeout(function() {
      adminForm.addClass('theme-primary');
      Panel.addClass('panel-primary');
      pageHeader.addClass('text-primary');

      $(options).each(function(i, e) {
        if ($(e).hasClass('block')) {
          $(e).removeClass().addClass('block mt15 option option-primary');
        } else {
          $(e).removeClass().addClass('option option-primary');
        }
      });
      $(switches).each(function(i, ele) {

        if ($(ele).hasClass('switch-round')) {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-round switch-primary');
          } else {
            $(ele).removeClass().addClass('switch switch-round switch-primary');
          }
        } else {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-primary');
          } else {
            $(ele).removeClass().addClass('switch switch-primary');
          }
        }
      });
      buttons.removeClass().addClass('button btn-primary');
    }, 800);

  });
  </script>
  <!-- END: PAGE SCRIPTS -->

</body>

</html>
