<?php 
require_once 'header.php';
require_once 'aside.php';
require_once 'classes/paciente.class.php';
require_once 'classes/funcoes.class.php';


$p = new Paciente();
$funcoes = new Funcoes();
?>


 <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <!-- Start: Topbar -->
      <header id="topbar" class="alt">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-icon">
              <a href="saida-materiais.php">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-active">
              <a href="">Cadastros</a>
            </li>
            <li class="crumb-trail">
              <a href="add-paciente.php">Pacientes</a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">
        <!-- begin: .tray-center -->
        <div class="tray tray-center">
          <div class="mw1000 center-block">
            <!-- Begin: Admin Form -->
             <div class="admin-form">

                    <div class="row">
                     <?php 
                     
                     // Início - Recebe POST
                    if(isset($_POST['nome']) && !empty($_POST['nome'])){



                      if(!empty($_POST['nome']) && !empty($_POST['data_nasc']) && !empty($_POST['sexo']) && !empty($_POST['contato1']) && !empty($_POST['parentesco1'])){

                         $nome = addslashes($_POST['nome']);
                         $data_nasc = $funcoes->entradaData($_POST['data_nasc']); 
                         $sexo = ($_POST['sexo']);
                         $cpf = str_replace('.', '', $_POST['cpf']);
                         $rg  = str_replace('.', '', $_POST['rg']);
                         $bairro = ($_POST['bairro']);
                         $rua = ($_POST['rua']);
                         $complemento = ($_POST['complemento']);
                         $numero = ($_POST['numero']);
                         $email = ($_POST['email']);
                         $cartao_sus  = str_replace('.', '', $_POST['cartao_sus']);
                         $data_cadastro = date('Y-m-d');
                         $contato1 = $funcoes->entradaTelefone($_POST['contato1'] );
                         $parentesco1 = ($_POST['parentesco1']);
                         $contato2 = $funcoes->entradaTelefone($_POST['contato2'] );
                         $parentesco2 = ($_POST['parentesco2']);
                         $prontuario_old = ($_FILES['arquivo']);
                         $n_prontuario_old = ($_POST['n_prontuario_old']);
                         $id_usuario_cadastro = ($_SESSION['cLogin']);
                         $id_paciente = ($_POST['id_paciente']);



                          $retorno = $p->editarPaciente($nome, $data_nasc, $sexo, $cartao_sus, $data_cadastro, $contato1, $parentesco1, $id_usuario_cadastro, $id_paciente, $cpf,  $rg, $bairro, $rua, $numero ,$complemento , $email, $contato2, $parentesco2, $prontuario_old, $n_prontuario_old);


                      if($retorno == true){
                          ?>
                          <div class="alert alert-success alert-dismissable">
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <i class="fa fa-remove pr10"></i>
                            Paciente editado com sucesso !
                          </div>
                        </div>
                          <?php 


                        }else{


                          ?>
                       
                        <div class="alert alert-danger alert-dismissable">
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            
                            <h4>Foi encontrado um <strong>outro paciente com este mesmo número do cartão do SUS</strong> e por isso não é possivel editar o paciente selecionado. O outro paciente possui a seguintes informações: </h4>
                            <?php 

                            echo "ID: ".$retorno['id']."<br>";
                            echo "Nome: ".$retorno['nome']."<br>";
                            echo "Data de nascimento: ".$retorno['data_nasc'] = $funcoes->saidaData($retorno['data_nasc']); 
                            echo "<br>";
                            echo "Cartão SUS: ".$retorno['cartao_sus']."<br>";

                            ?>


                            <br>
                            <br>
                            <h4>Deseja abrir o cadastro do paciente ? </h4>
                          </div>
                        </div>
            
                        <?php 
                          
                           }

                        ?>
            


                      <?php
                      
                      }else{
                      
                      ?>
                        <div class="alert alert-danger alert-dismissable">
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> 
                            <h5><strong>Preencha todos os campos obrigatórios</strong></h5>
                          </div>
                        </div>


                      <?php

                      }// Fim - If preechimento de campos

                    }// Fim - Recebe POST

                      ?>

                    </div>
                    <br>
                    <br>
                    <?php

                    if(isset($_GET['id']) && !empty($_GET['id'])){

                      $paciente = $p->getPaciente($_GET['id']);


                    }else{

                      ?>
                        <script type="text/javascript">window.location.href="pacientes.php"</script>
                      <?php
                    }


                    ?>
                    <div class="admin-form theme-primary">
                      <div class="panel heading-border panel-primary">
                        <div class="panel-body bg-light">      
                          <div class="section-divider mb40" id="spy1">
                            <span>Informações pessoais</span>
                          </div>
                          <form method="POST" enctype="multipart/form-data" id="form-editar-paciente">
                            <div class="row" id="grid_itens">
                              <div class="col-md-2">
                                <div class="form-group">
                                  <label for="nome"><b>Id:</b></label>
                                  <input type="number" name="id_paciente" id="id_paciente" class="form-control"  value="<?php echo $paciente['id']?>" readonly>
                                </div>
                              </div>
                              <div class="col-md-7">
                                <div class="form-group">
                                  <label for="nome"><b>Nome:</b></label>
                                  <input type="text" name="nome" id="nome" class="form-control" value="<?php echo $paciente['nome']?>" >
                                </div>
                              </div>
                              <div class="col-md-2">
                                <div class="form-group">
                                  <label for="data_nasc"><b>Nascimento</b></label>
                                  <input type="text" name="data_nasc" id="maskedDate" maxlength="10" autocomplete="off" class="form-control date"  value="<?php echo $paciente['data_nasc'] = $funcoes->saidaData($paciente['data_nasc'])?>">
                                </div>
                              </div>
                            </div><!-- fim da row -->
                            <div class="row">
                              <div class="col-md-2">
                                <div class="form-group">
                                  <label for="tipo"><b>Sexo</b></label>
                                  <select name="sexo" id="sexo" class="form-control">
                                    <option value="masc" <?php echo ($paciente['sexo'] == 'masc')?'selected="selected"':'';?>>Masculino</option>
                                    <option value="fem" <?php echo ($paciente['sexo'] == 'fem')?'selected="selected"':'';?>>Feminino</option>
                                  </select>
                                </div>
                              </div>
                              <div class="col-md-3">
                                <div class="form-group">
                                  <label for="cpf"><b>CPF:</b></label>
                                  <input type="text" name="cpf" class="form-control cpf" value="<?php echo $paciente['cpf']?>">
                                </div>
                              </div>
                              <div class="col-md-3">
                                <div class="form-group">
                                  <label for="rg"><b>RG:</b></label>
                                  <input type="text" name="rg" id="rg" class="form-control rg" value="<?php echo $paciente['rg']?>">
                                </div>
                              </div>
                              <div class="col-md-3">
                                <div class="form-group">
                                  <label for="cartao_sus"><b>Cartão do SUS:</b></label>
                                  <input type="text" name="cartao_sus" id="cartao_sus" class="form-control cartao_sus"  value="<?php echo $paciente['cartao_sus']?>">
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-4">
                                <div class="form-group">
                                  <label for="complemento"><b>Bairro:</b></label>
                                  <input type="text" name="bairro" id="bairro" class="form-control"  value="<?php echo $paciente['id_bairro']; ?>">
                                </div>
                              </div>
                              <div class="col-md-4">
                                <div class="form-group">
                                  <label for="complemento"><b>Endereco:</b></label>
                                  <input type="text" name="rua" id="rua" class="form-control"  value="<?php echo $paciente['id_rua']; ?>">
                                </div>
                              </div>
                              <div class="col-md-3">
                                <div class="form-group">
                                  <label for="complemento"><b>Complemento:</b></label>
                                  <input type="text" name="complemento" id="complemento" class="form-control"  value="<?php echo $paciente['complemento']; ?>">
                                </div>
                              </div>
                              <div class="col-md-1">
                                <div class="form-group">
                                  <label for="numero"><b>Número:</b></label>
                                  <input type="text" name="numero" id="numero" class="form-control" value="<?php echo $paciente['numero']; ?>">
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-4">
                                <div class="form-group">
                                  <label for="email"><b>E-mail:</b></label>
                                  <input type="email" name="email" id="email" class="form-control"  value="<?php echo $paciente['email']; ?>">
                                </div>
                              </div>
                              <div class="col-md-2">
                                <div class="form-group">
                                  <label for="contato1"><b>Contato 1:</b></label>
                                  <input type="text" name="contato1" id="contato1" class="form-control celular"  value="<?php echo $paciente['contato1']?>">
                                </div>
                              </div>
                              <div class="col-md-2">
                                <div class="form-group">
                                  <label for="parentesco1"><b>Parentesco 1:</b></label>
                                  <input type="text" name="parentesco1" id="parentesco1" class="form-control"  value="<?php echo $paciente['parentesco1']?>">
                                </div>
                              </div>
                              <div class="col-md-2">
                                <div class="form-group">
                                  <label for="contato2"><b>Contato 2:</b></label>
                                  <input type="text" name="contato2" id="contato2" class="form-control celular" value="<?php echo $paciente['contato2']?>">
                                </div>
                              </div>
                              <div class="col-md-2">
                                <div class="form-group">
                                  <label for="paretesco2"><b>Parentesco 2:</b></label>
                                  <input type="text" name="parentesco2" id="paretesco2" class="form-control"  value="<?php echo $paciente['parentesco2']?>">
                                </div>
                              </div>
                            </div><!-- Fim da row -->
                            <div class="row">
                              <div class="col-md-12">
                                <div class="section-divider mb40" id="spy1">
                                  <span>Prontuário Digitalizado</span>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-6" style="margin-top: 25px;">
                                <div class="section">
                                  <label class="field prepend-icon append-button file">
                                    <span class="button btn-primary">Selecione o arquivo</span>
                                    <input type="file" class="gui-file" name="arquivo[]" id="arquivo[]" onchange="document.getElementById('uploader1').value = this.value;">
                                    <input type="text" class="gui-input" id="arquivo[]" placeholder="Por favor selecione os arquivos">
                                    <label class="field-icon">
                                      <i class="fa fa-upload"></i>
                                    </label>
                                  </label>
                                </div>
                              </div>
                              <?php 

                              if(!empty($paciente['prontuario_old'])){

                              ?>

                              <div class="col-md-1" style="margin-top: 20px;">
                                <a id="prontuario_old" href="prontuarios_old/<?php echo $paciente['prontuario_old']; ?>" target="_blank"><img src="assets/img/logos/logo_pdf.png" alt="prontuario_antigo"></a>
                              </div>

                              <?php

                              }

                              ?>
                            <div class="col-md-2">
                              <div class="form-group">
                                <label for="n_prontuario_old"><b>Nº Prontuário:</b></label>
                                <input type="text" name="n_prontuario_old" id="n_prontuario_old" class="form-control" value="<?php echo $paciente['n_prontuario_old']; ?>">
                              </div>
                            </div>
                            </div>
                            <div class="row">
                              <div class="col-md-2">
                                <input type="submit" name="Editar" class="btn btn-success" value="Cadastrar">
                              </div>
                            

                            </div>
                          </form>




                

            </div>
          </div>
        </div>
        <!-- end: .tray-center -->
      </section>
      <!-- End: Content -->
    </section>
  </div>
  <!-- End: Main -->

  <style>
  /* demo page styles */
  body { min-height: 2300px; }
  
  .content-header b,
  .admin-form .panel.heading-border:before,
  .admin-form .panel .heading-border:before {
    transition: all 0.7s ease;
  }
  /* responsive demo styles */
  @media (max-width: 800px) {
    .admin-form .panel-body { padding: 18px 12px; }
    .option-group .option { display: block; }
    .option-group .option + .option { margin-top: 8px; }
  }
  </style>
    

  <!-- BEGIN: PAGE SCRIPTS -->

  <style>
  /* demo page styles */
  body { min-height: 2300px; }
  
  .content-header b,
  .admin-form .panel.heading-border:before,
  .admin-form .panel .heading-border:before {
    transition: all 0.7s ease;
  }
  /* responsive demo styles */
  @media (max-width: 800px) {
    .admin-form .panel-body { padding: 18px 12px; }
    .option-group .option { display: block; }
    .option-group .option + .option { margin-top: 8px; }
  }
  </style>
    
<!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- Time/Date Plugin Dependencies -->
  <script src="vendor/plugins/globalize/globalize.min.js"></script>
  <script src="vendor/plugins/moment/moment.min.js"></script>

  <!-- BS Dual Listbox Plugin -->
  <script src="vendor/plugins/duallistbox/jquery.bootstrap-duallistbox.min.js"></script>

  <!-- Bootstrap Maxlength plugin -->
  <script src="vendor/plugins/maxlength/bootstrap-maxlength.min.js"></script>

  <!-- Select2 Plugin Plugin -->
  <script src="vendor/plugins/select2/select2.min.js"></script>

  <!-- Typeahead Plugin -->
  <script src="vendor/plugins/typeahead/typeahead.bundle.min.js"></script>

  <!-- TagManager Plugin -->
  <script src="vendor/plugins/tagmanager/tagmanager.js"></script>

  <!-- DateRange Plugin -->
  <script src="vendor/plugins/daterange/daterangepicker.min.js"></script>

  <!-- DateTime Plugin -->
  <script src="vendor/plugins/datepicker/js/bootstrap-datetimepicker.min.js"></script>

  <!-- BS Colorpicker Plugin -->
  <script src="vendor/plugins/colorpicker/js/bootstrap-colorpicker.min.js"></script>

  <!-- MaskedInput Plugin -->
  <script src="vendor/plugins/jquerymask/jquery.maskedinput.min.js"></script>

      <!-- Datatables -->
  <script src="vendor/plugins/datatables/media/js/jquery.dataTables.js"></script>

  <!-- Datatables Tabletools addon -->
  <script src="vendor/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>

  <!-- Datatables ColReorder addon -->
  <script src="vendor/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>

  <!-- Datatables Bootstrap Modifications  -->
  <script src="vendor/plugins/datatables/media/js/dataTables.bootstrap.js"></script>



  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript" src="assets/js/functions.js"></script>
  <script type="text/javascript" src="assets/js/scripts.js"></script>
  <script type="text/javascript">
  jQuery(document).ready(function() {



    "use strict";

    // Init Theme Core    
    Core.init();



    // Init Select2 - Basic Single
    $(".cidades").select2();


    $('#entrada-materiais').DataTable();

    // Init Select2 - Contextuals (via html classes)
    $(".select2-primary").select2(); // select2 contextual - primary
    $(".select2-success").select2(); // select2 contextual - success
    $(".select2-info").select2();    // select2 contextual - info
    $(".select2-warning").select2(); // select2 contextual - warning  

    // Init Bootstrap Maxlength Plugin
    $('input[maxlength]').maxlength({
      threshold: 15,
      placement: "right"
    });

    // Dual List Plugin Init
    var demo1 = $('.demo1').bootstrapDualListbox({
      nonSelectedListLabel: 'Options',
      selectedListLabel: 'Selected',
      preserveSelectionOnMove: 'moved',
      moveOnSelect: true,
      nonSelectedFilter: 'ion ([7-9]|[1][0-2])'
    });

    $("#demoform").submit(function() {
      alert("Options Selected: " + $('.demo1').val());
      return false;
    });

    // Init Twitter Typeahead.js
    var substringMatcher = function(strs) {
      return function findMatches(q, cb) {
        var matches, substrRegex;

        // an array that will be populated with substring matches
        matches = [];

        // regex used to determine if a string contains the substring `q`
        substrRegex = new RegExp(q, 'i');

        // iterate through the pool of strings and for any string that
        // contains the substring `q`, add it to the `matches` array
        $.each(strs, function(i, str) {
          if (substrRegex.test(str)) {
            // the typeahead jQuery plugin expects suggestions to a
            // JavaScript object, refer to typeahead docs for more info
            matches.push({
              value: str
            });
          }
        });

        cb(matches);
      };
    };

    var states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California',
      'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii',
      'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana',
      'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota',
      'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire',
      'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota',
      'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island',
      'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont',
      'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'
    ];

    // Init Typeahead Plugin with state aray
    $('.typeahead').typeahead({
      hint: true,
      highlight: true,
      minLength: 1
    }, {
      name: 'states',
      displayKey: 'value',
      source: substringMatcher(states)
    });

    // DateRange plugin options
    var rangeOptions = {
      ranges: {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
        'Last 7 Days': [moment().subtract('days', 6), moment()],
        'Last 30 Days': [moment().subtract('days', 29), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
      },
      startDate: moment().subtract('days', 29),
      endDate: moment()
    }

    // Init DateRange plugin
    $('#daterangepicker1').daterangepicker();

    // Init DateRange plugin
    $('#daterangepicker2').daterangepicker(
      rangeOptions,
      function(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      }
    );

    // Init DateRange plugin
    $('#inline-daterange').daterangepicker(
      rangeOptions,
      function(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      }
    );

    // Init DateTimepicker - fields
    $('#datetimepicker1').datetimepicker();
    $('#datetimepicker2').datetimepicker();

    // Init DateTimepicker - inline + range detection
    $('#datetimepicker3').datetimepicker({
      defaultDate: "9/4/2014",
      inline: true,
    });

    // Init DateTimepicker - fields + Date disabled (only time picker)
    $('#datetimepicker5').datetimepicker({
      defaultDate: "9/25/2014",
      pickDate: false,
    });
    // Init DateTimepicker - fields + Date disabled (only time picker)
    $('#datetimepicker6').datetimepicker({
      defaultDate: "9/25/2014",
      pickDate: false,
    });
    // Init DateTimepicker - inline + Date disabled (only time picker)
    $('#datetimepicker7').datetimepicker({
      defaultDate: "9/25/2014",
      pickDate: false,
      inline: true
    });

    // Init Colorpicker plugin
    $('#demo_apidemo').colorpicker({
      color: bgPrimary
    });
    $('.demo-auto').colorpicker();

    // Init jQuery Tags Manager 
    $(".tm-input").tagsManager({
      tagsContainer: '.tags',
      prefilled: ["Miley Cyrus", "Apple", "A Long Tag", "Na uh"],
      tagClass: 'tm-tag-info',
    });

    // Init Boostrap Multiselects
    $('#multiselect1').multiselect();
    $('#multiselect2').multiselect({
      includeSelectAllOption: true
    });
    $('#multiselect3').multiselect();
    $('#multiselect4').multiselect({
      enableFiltering: true,
    });
    $('#multiselect5').multiselect({
      buttonClass: 'multiselect dropdown-toggle btn btn-primary'
    });
    $('#multiselect6').multiselect({
      buttonClass: 'multiselect dropdown-toggle btn btn-info'
    });
    $('#multiselect7').multiselect({
      buttonClass: 'multiselect dropdown-toggle btn btn-success'
    });
    $('#multiselect8').multiselect({
      buttonClass: 'multiselect dropdown-toggle btn btn-warning'
    });

    // Init jQuery spinner init - default
    $("#spinner1").spinner();

    // Init jQuery spinner init - currency 
    $("#spinner2").spinner({
      min: 5,
      max: 2500,
      step: 25,
      start: 1000,
      //numberFormat: "C"
    });

    // Init jQuery spinner init - decimal  
    $("#spinner3").spinner({
      step: 0.01,
      numberFormat: "n"
    });

    // jQuery Time Spinner settings
    $.widget("ui.timespinner", $.ui.spinner, {
      options: {
        // seconds
        step: 60 * 1000,
        // hours
        page: 60
      },
      _parse: function(value) {
        if (typeof value === "string") {
          // already a timestamp
          if (Number(value) == value) {
            return Number(value);
          }
          return +Globalize.parseDate(value);
        }
        return value;
      },

      _format: function(value) {
        return Globalize.format(new Date(value), "t");
      }
    });

    // Init jQuery Time Spinner
    $("#spinner4").timespinner();

    // Init jQuery Masked inputs
    $('.date').mask('99/99/9999');
    $('.celular').mask('(99)99999-9999');
    $('.cartao_sus').mask('999.9999.9999.9999');



/*
      $(".qtd1,.custo1").on('keyup', function() {



        var qtd1 = parseFloat($('.qtd1').val()) || 0;
        var custo1 = parseFloat($('.custo1').val()) || 0;

        var totalproduto = qtd1 * custo1;

        $('.resultado1').val(totalproduto);
      });
          
          
*/
          




  });
  </script>

</body>

</html>
