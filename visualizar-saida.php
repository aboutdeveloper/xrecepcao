<?php 
require_once 'header.php';
require_once 'aside.php';
require_once 'classes/produto.class.php';
require_once 'classes/materiais.class.php';
require_once 'classes/departamentos.class.php';

$p = new Produtos();
$d = new  Departamentos();
$m = new Materiais();

?>


 <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <!-- Start: Topbar -->
      <header id="topbar" class="alt">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-icon">
              <a href="saida-materiais.php">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-active">
              <a href="saida-materiais.php">Procedimentos</a>
            </li>
            <li class="crumb-trail">
              <a href="saida-materiais.php">Saída de materiais</a>
            </li>
            <li class="crumb-trail">
              <a href="add-entrada.php">Visualizar Saída</a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">
        <!-- begin: .tray-center -->
        <div class="tray tray-center">
          <div class="mw1000 center-block">
            <!-- Begin: Admin Form -->
            <div class="admin-form">
              <div class="admin-form">
                    <?php 

                    if(isset($_GET['id']) && !empty($_GET['id'])){

                      $info = $m->getSaidaMateriais($_GET['id']);

                      $produtosReqSaida = $m->getItensReqSaida($_GET['id']);






                    }else{

                      ?>
                      <script type="text/javascript">window.location.href="entrada-materiais.php"</script>

                      <?php
                    }

                    ?>
                    <?php

                      if(isset($_GET['faturado']) && !empty($_GET['faturado'])){

                      ?>

                              <div class="alert alert-success alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                  <i class="fa fa-remove pr10"></i>
                                 <strong> Requisição faturada com sucesso !</strong>
                              </div>
                      <?php

                      }

                      ?>


                    <div class="admin-form theme-primary">
                      <div class="panel heading-border panel-primary">
                        <div class="panel-body bg-light">      
                          <div class="section-divider mb40" id="spy1">
                            <span>Saída de materiais</span>
                          </div>  
                          <form method="POST">
                            <div class="row">
                              <div class="col-md-2">
                                <div class="form-group">
                                  <label for="id"><b>ID</b></label>
                                  <input type="text" disabled="" class ="form-control" value="<?php 
                                  echo $info[0]['id'];
                                   ?>" >
                                </div>
                              </div>
                              <div class="col-md-2">
                                <div class="form-group">
                                  <label for="data_requisicao"><b>Data da entrada:</b></label>
                                  <input type="text" name="data_requisicao" id="data_baixa maskedDate" maxlength="10" autocomplete="off" class="form-control date"  disabled="" value="<?php 
                                  
                                  $data_velha = $info[0]['data_baixa'];
                                  $data_br = date('d-m-Y', strtotime($data_velha));
                                  $data_baixa = str_replace('-', '/', $data_br);
                                  echo $data_baixa;


                                   ?>" >
                                </div>
                              </div>
                              <div class="col-md-6">
                                <label for="departamento"><b>Departamento:</b></label>
                                <div class="form-group">
                                  <select name="id_departamento" id="id_departamento" class="cidades form-control"  disabled="">
                                  <?php 

                                    $departamentos = $d->getNomeDepartamentos();
                                    foreach($departamentos as $departamento): ?>
                                      <option value="<?php echo $departamento['id'];?>" <?php echo ($info[0]['id_departamento'] == $departamento['id'])?'selected="selected"':'';?>><?php echo $departamento['nome'];?></option>
                                  <?php endforeach; ?>
                                  </select>
                                </div> 
                              </div>
                              <div class="col-md-2">
                                <div class="form-group">
                                  <label for="data_requisicao"><b>Situação</b></label>
                                  <input type="text" disabled="" class ="form-control" value="<?php 
                                    switch ($info[0]['situacao']) {
                                      case 1:
                                          echo "Atendida";
                                          break;
                                      case 2:
                                          echo "Não Atendida";
                                          break; 

                                       } 
                                   ?>" >
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-md-12">
                                  <p><strong>Observação:</strong></p>
                                  <div class="section">
                                    <label class="field prepend-icon">
                                      <textarea class="gui-textarea" id="observarcao" name="observacao"><?php echo $info[0]['obs']; ?></textarea>
                                      <label for="observarcao" class="field-icon">
                                        <i class="fa fa-comments"></i>
                                      </label>
                                      <span class="input-footer">
                                        
                                    </span></label>
                                  </div>
                                </div>
                              </div>
                            <div class="row">
                              <table class="table table-striped" id="#formulario">
                                <thead>
                                  <tr class="dark">
                                    <th></th>
                                    <th>Produto</th>
                                    <th>Quantidade</th>
                                    <th></th>
                                    <th></th>
                                  </tr>
                                </thead>

                                <tbody id="grid_itens">
                                  <?php  $i = 1; foreach  ($produtosReqSaida as $itemProduto): ?>
                                  <tr class="campo">
                                    <td>
                                        <div class="btn-group">
                                        <button type="button" class="btn btn-default">
                                          <i class="glyphicon glyphicon-trash"></i>
                                        </button>
                                        </div>
                                    </td>                                 
                                    <td>
                                      <div class="form-group">
                                      <select name="produto[<?php echo $i ;?>][id]" id="select_pai" class="cidades form-control "  disabled="">
                                      <?php 
                                        
                                        $produtos = $p->getNomeProdutos();

                                        foreach($produtos as $produto): ?>
                                         <option value="<?php echo $produto['id'];?>"<?php echo ($itemProduto['id_produto'] == $produto['id'])?'selected="selected"':'';?>><?php echo $produto['nome'];?></option>
                                        <?php endforeach; ?>
                                      </select>
                                      </div> 
                                    </td>
                                    <td class="col-md-1">
                                      <input type="text" name="produto[<?php echo $i ;?>][qtd]" class="form-control qtd1 soma" required=""   value="<?php echo $itemProduto['qtd_produto']; ?>"  disabled="">
                                    </td>  
                                  </tr>
                                <?php $i++?>
                                <?php  endforeach; ?>
                                </tbody>
                              </table>
                            </div>
                            <br>
                            <div class="row">
                              <div class="col-md-2">
                                <input type="button" class="btn btn-info" value="Adicionar item" id="">
                              </div>
                            </div>
                            <br>
                            <br>
                            <div class="row">
                              <div class="panel-footer clearfix p10 ph15">

                                  <a href="imprimirRequisicao.php?id=<?php echo $info[0]['id'];?>" class="btn btn-system" type="button">
                                  Imprimir Requisição

                                  </a>
                                <?php if($_SESSION['tipo'] != 1): ?>
                                  <?php if($info[0]['situacao'] == 2): ?>
                                    <a href="faturarRequisicao.php?id=<?php echo $info[0]['id'];?>" class="btn btn-success pull-right" type="button">
                                    
                                    Faturar Requisção

                                    </a>
                                  <?php endif;?>
                                <?php endif;?>
                              </div>
                            </div>
                          </form> 
                

            </div>
          </div>
        </div>
        <!-- end: .tray-center -->
      </section>
      <!-- End: Content -->
    </section>
  </div>
  <!-- End: Main -->

  <style>
  /* demo page styles */
  body { min-height: 2300px; }
  
  .content-header b,
  .admin-form .panel.heading-border:before,
  .admin-form .panel .heading-border:before {
    transition: all 0.7s ease;
  }
  /* responsive demo styles */
  @media (max-width: 800px) {
    .admin-form .panel-body { padding: 18px 12px; }
    .option-group .option { display: block; }
    .option-group .option + .option { margin-top: 8px; }
  }
  </style>
    

  <!-- BEGIN: PAGE SCRIPTS -->

  <style>
  /* demo page styles */
  body { min-height: 2300px; }
  
  .content-header b,
  .admin-form .panel.heading-border:before,
  .admin-form .panel .heading-border:before {
    transition: all 0.7s ease;
  }
  /* responsive demo styles */
  @media (max-width: 800px) {
    .admin-form .panel-body { padding: 18px 12px; }
    .option-group .option { display: block; }
    .option-group .option + .option { margin-top: 8px; }
  }
  </style>
    
<!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- Time/Date Plugin Dependencies -->
  <script src="vendor/plugins/globalize/globalize.min.js"></script>
  <script src="vendor/plugins/moment/moment.min.js"></script>

  <!-- BS Dual Listbox Plugin -->
  <script src="vendor/plugins/duallistbox/jquery.bootstrap-duallistbox.min.js"></script>

  <!-- Bootstrap Maxlength plugin -->
  <script src="vendor/plugins/maxlength/bootstrap-maxlength.min.js"></script>

  <!-- Select2 Plugin Plugin -->
  <script src="vendor/plugins/select2/select2.min.js"></script>

  <!-- Typeahead Plugin -->
  <script src="vendor/plugins/typeahead/typeahead.bundle.min.js"></script>

  <!-- TagManager Plugin -->
  <script src="vendor/plugins/tagmanager/tagmanager.js"></script>

  <!-- DateRange Plugin -->
  <script src="vendor/plugins/daterange/daterangepicker.min.js"></script>

  <!-- DateTime Plugin -->
  <script src="vendor/plugins/datepicker/js/bootstrap-datetimepicker.min.js"></script>

  <!-- BS Colorpicker Plugin -->
  <script src="vendor/plugins/colorpicker/js/bootstrap-colorpicker.min.js"></script>

  <!-- MaskedInput Plugin -->
  <script src="vendor/plugins/jquerymask/jquery.maskedinput.min.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript" src="assets/js/campos_dinamicos_editar_saida_materiais.js"></script>
  <script type="text/javascript">
  jQuery(document).ready(function() {



    "use strict";

    // Init Theme Core    
    Core.init();



    // Init Select2 - Basic Single
    $(".cidades").select2();




    // Init Select2 - Contextuals (via html classes)
    $(".select2-primary").select2(); // select2 contextual - primary
    $(".select2-success").select2(); // select2 contextual - success
    $(".select2-info").select2();    // select2 contextual - info
    $(".select2-warning").select2(); // select2 contextual - warning  

    // Init Bootstrap Maxlength Plugin
    $('input[maxlength]').maxlength({
      threshold: 15,
      placement: "right"
    });

    // Dual List Plugin Init
    var demo1 = $('.demo1').bootstrapDualListbox({
      nonSelectedListLabel: 'Options',
      selectedListLabel: 'Selected',
      preserveSelectionOnMove: 'moved',
      moveOnSelect: true,
      nonSelectedFilter: 'ion ([7-9]|[1][0-2])'
    });

    $("#demoform").submit(function() {
      alert("Options Selected: " + $('.demo1').val());
      return false;
    });

    // Init Twitter Typeahead.js
    var substringMatcher = function(strs) {
      return function findMatches(q, cb) {
        var matches, substrRegex;

        // an array that will be populated with substring matches
        matches = [];

        // regex used to determine if a string contains the substring `q`
        substrRegex = new RegExp(q, 'i');

        // iterate through the pool of strings and for any string that
        // contains the substring `q`, add it to the `matches` array
        $.each(strs, function(i, str) {
          if (substrRegex.test(str)) {
            // the typeahead jQuery plugin expects suggestions to a
            // JavaScript object, refer to typeahead docs for more info
            matches.push({
              value: str
            });
          }
        });

        cb(matches);
      };
    };

    var states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California',
      'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii',
      'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana',
      'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota',
      'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire',
      'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota',
      'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island',
      'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont',
      'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'
    ];

    // Init Typeahead Plugin with state aray
    $('.typeahead').typeahead({
      hint: true,
      highlight: true,
      minLength: 1
    }, {
      name: 'states',
      displayKey: 'value',
      source: substringMatcher(states)
    });

    // DateRange plugin options
    var rangeOptions = {
      ranges: {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
        'Last 7 Days': [moment().subtract('days', 6), moment()],
        'Last 30 Days': [moment().subtract('days', 29), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
      },
      startDate: moment().subtract('days', 29),
      endDate: moment()
    }

    // Init DateRange plugin
    $('#daterangepicker1').daterangepicker();

    // Init DateRange plugin
    $('#daterangepicker2').daterangepicker(
      rangeOptions,
      function(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      }
    );

    // Init DateRange plugin
    $('#inline-daterange').daterangepicker(
      rangeOptions,
      function(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      }
    );

    // Init DateTimepicker - fields
    $('#datetimepicker1').datetimepicker();
    $('#datetimepicker2').datetimepicker();

    // Init DateTimepicker - inline + range detection
    $('#datetimepicker3').datetimepicker({
      defaultDate: "9/4/2014",
      inline: true,
    });

    // Init DateTimepicker - fields + Date disabled (only time picker)
    $('#datetimepicker5').datetimepicker({
      defaultDate: "9/25/2014",
      pickDate: false,
    });
    // Init DateTimepicker - fields + Date disabled (only time picker)
    $('#datetimepicker6').datetimepicker({
      defaultDate: "9/25/2014",
      pickDate: false,
    });
    // Init DateTimepicker - inline + Date disabled (only time picker)
    $('#datetimepicker7').datetimepicker({
      defaultDate: "9/25/2014",
      pickDate: false,
      inline: true
    });

    // Init Colorpicker plugin
    $('#demo_apidemo').colorpicker({
      color: bgPrimary
    });
    $('.demo-auto').colorpicker();

    // Init jQuery Tags Manager 
    $(".tm-input").tagsManager({
      tagsContainer: '.tags',
      prefilled: ["Miley Cyrus", "Apple", "A Long Tag", "Na uh"],
      tagClass: 'tm-tag-info',
    });

    // Init Boostrap Multiselects
    $('#multiselect1').multiselect();
    $('#multiselect2').multiselect({
      includeSelectAllOption: true
    });
    $('#multiselect3').multiselect();
    $('#multiselect4').multiselect({
      enableFiltering: true,
    });
    $('#multiselect5').multiselect({
      buttonClass: 'multiselect dropdown-toggle btn btn-primary'
    });
    $('#multiselect6').multiselect({
      buttonClass: 'multiselect dropdown-toggle btn btn-info'
    });
    $('#multiselect7').multiselect({
      buttonClass: 'multiselect dropdown-toggle btn btn-success'
    });
    $('#multiselect8').multiselect({
      buttonClass: 'multiselect dropdown-toggle btn btn-warning'
    });

    // Init jQuery spinner init - default
    $("#spinner1").spinner();

    // Init jQuery spinner init - currency 
    $("#spinner2").spinner({
      min: 5,
      max: 2500,
      step: 25,
      start: 1000,
      //numberFormat: "C"
    });

    // Init jQuery spinner init - decimal  
    $("#spinner3").spinner({
      step: 0.01,
      numberFormat: "n"
    });

    // jQuery Time Spinner settings
    $.widget("ui.timespinner", $.ui.spinner, {
      options: {
        // seconds
        step: 60 * 1000,
        // hours
        page: 60
      },
      _parse: function(value) {
        if (typeof value === "string") {
          // already a timestamp
          if (Number(value) == value) {
            return Number(value);
          }
          return +Globalize.parseDate(value);
        }
        return value;
      },

      _format: function(value) {
        return Globalize.format(new Date(value), "t");
      }
    });

    // Init jQuery Time Spinner
    $("#spinner4").timespinner();

    // Init jQuery Masked inputs
    $('.date').mask('99/99/9999');
    $('.time').mask('99:99:99');
    $('.date_time').mask('99/99/9999 99:99:99');
    $('.zip').mask('99999-999');
    $('.phone').mask('(999) 999-9999');
    $('.phoneext').mask("(999) 999-9999 x99999");
    $(".money").mask("999,999,999.999");
    $(".product").mask("999.999.999.999");
    $(".tin").mask("99-9999999");
    $(".ssn").mask("999-99-9999");
    $(".ip").mask("9ZZ.9ZZ.9ZZ.9ZZ");
    $(".eyescript").mask("~9.99 ~9.99 999");
    $(".custom").mask("9.99.999.9999");

   $(document).on('click', '.btn-apagar', function() {
        $(this).closest('.campo').remove();
      }); 
     
/*
      $(".qtd1,.custo1").on('keyup', function() {



        var qtd1 = parseFloat($('.qtd1').val()) || 0;
        var custo1 = parseFloat($('.custo1').val()) || 0;

        var totalproduto = qtd1 * custo1;

        $('.resultado1').val(totalproduto);
      });
          
          
*/
          




  });
  </script>

</body>

</html>
