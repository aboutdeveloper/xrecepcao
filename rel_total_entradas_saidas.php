<?php
require_once 'config.php';
require_once 'classes/materiais.class.php';

$m = new Materiais();


if(isset($_GET['data_inicial']) && !empty($_GET['data_inicial'])){


	if(empty($_GET['zerados'])){

		$zerados = 2;



	}else{

		$zerados = $_GET['zerados'];


	}
	

    $data_nova = str_replace('/', '-', $_GET['data_inicial']);
    $data_inicial = date('Y-m-d', strtotime($data_nova));
    $data_inicial = addslashes($data_inicial);
	$data_nova = str_replace('/', '-', $_GET['data_final']);
    $data_final = date('Y-m-d', strtotime($data_nova));
    $data_final = addslashes($data_final);



    $produtosReqSaida = $m->rel_total_entradas_saidas($data_inicial, $data_final, $zerados);


}else{
	?>
	<script type="text/javascript">window.location.href="relatorio_inventario.php"</script>
	<?php 
}

?>
<style rel="stylesheet" type="text/css" href="rel_impressao.css" media="print"></style>
<style type="text/css">
	body, div{

		
	}

	@media print{
		.botao-imprimir{
			display: none;
		}
		.exibir{

			display: inline-block !important;
			background-color: #ccc !important;
			
		}
		.aparece{
			background-color: black !important;

		}

	}

	.exibir{
		display: inline-block;
		background-color: red;

		}

	.botao-imprimir{
		margin-top: 30px;
		width: 62px;
		height: 16px;
		cursor: pointer;
		font-family: Arial, Helvetica, sans-serif;
		font-size: 14px;
		color: #050505;
		padding: 10px 20px;
		background: -webkit-gradient(
		linear, left top, left bottom,
		from(#ffffff),
		color-stop(0.50, #c7d95f),
		color-stop(0.50, #add136),
		to(#6d8000));
	background: linear-gradient(
		top,
		#ffffff 0%,
		#c7d95f 50%,
		#add136 50%,
		#6d8000);
	border-radius: 14px;
	border: 1px solid #6d8000;
	box-shadow:
		0px 1px 3px rgba(000,000,000,0.5),
		inset 0px 0px 2px rgba(255,255,255,1);
	text-shadow:
		0px -1px 0px rgba(000,000,000,0.2),
		0px 1px 0px rgba(255,255,255,0.4);



}
	
</style>
<!-- <body style="font-family:arial;" onload="window.print();"> -->
<body style="font-family:arial;" onload="window.print();">
	<!-- Botão imprimir -->

	<div align="center" id="funcoes"  align="center" style="margin-left: 580px; display: inline-block; margin-top: 36px; margin-bottom: 20px;" >
		<a class="botao-imprimir" onclick="document.getElementById('funcoes').style.display = 'none'; window.print();"><img border="0">Imprimir</a>
	</div>
	<div id="funcoes"  align="center" style="display: inline-block; margin-top: 36px; " >
		<a class="botao-imprimir" onclick="document.getElementById('funcoes').style.display = 'none';"  href="relatorio_total_entradas_saidas.php" ><img border="0">Voltar</a>
	<br>
	</div>
	<!-- Início do topo do cabecalho -->
  <table align="center" border="0" cellpadding="0" cellspacing="0" width="900">
	<tr  style="text-align: center;">

	<td colspan="3" width="500" style="padding-left:10px;padding-bottom:10px;">
			<table border="0" cellpadding="10">
	<td width="100" height="100">
		<img src="assets/img/logos/logo-camara.jpg">
	</td>
	<td width="900" style="font-size:14px;">
		<h3>CAMARA MUNICIPAL DE LUIS EDUARDO MAGALHAES</h3>
		<b>Endereço: </b>RUA OCTOGONAL, 684<br />
		<b>Bairro: </b>JARDIM IMPERIAL<br />
		<b>Cidade: </b>Luís Eduardo Magalhães - Bahia<br />
		<b>CEP: </b>47850-000<br /><br />
	</td>
</table>	
<hr>		
</td>
</tr>

<!-- Fim do topo do cabecalho -->
	<tr>
		<td colspan="3">
			<table>
				<tr>
					<td width="100"></td>
					<td></td>
					<td></td>
					<td></td>
					<td colspan="3"><h2>RELATÓRIO TOTAL DE ENTRADAS E SAÍDAS POR PRODUTO</h2></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="3" style="padding:5px;">
			<br />
			Emitido: <b style="font-size: 14px;"><?php echo date("d-m-Y");?></b>
			<br>
			Período inicial: <b style="font-size: 14px;"><?php echo $_GET['data_inicial']?></b> &nbsp
			Período final:  <b style="font-size: 14px;"><?php echo $_GET['data_final']?></b>
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<table>
				<tr>
					<td  style="padding:10px;"></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<table cellpadding="5" cellspacing="0">
				<tr style="font-weight:bold; font-size: 14px; background:#E8E8E8;">
						<td style="border:1px solid;     text-align: center; border-bottom: none; border-right: none;" width="20">Código</td>
						<td style="border:1px solid;     text-align: center; border-bottom: none; border-right: none;" width="1200" >Produto</td>
						<td style="border:1px solid;     text-align: center; border-bottom: none;" width="250" >Total entradas</td>
						<td style="border:1px solid;     text-align: center; border-bottom: none; border-left: none;" width="230">Total Saídas</td>
						<td style="border:1px solid;     text-align: center; border-bottom: none; border-left: none;" width="230">Saldo físico</td>
					</tr>
				<?php $valorTotal = 0; foreach ($produtosReqSaida as $produto): ?>
					<tr cellpadding="5">
						<td style="text-align: center; border:1px solid; "  width="20">
							<?php echo $produto['id_produto']; ?></td>
						<td style=" border: 1px solid; border-right: none; ;">
							<?php echo $produto['nome_produto']; ?></td>
						<td style="text-align: center; border:1px solid; ">
							<?php echo $produto['total_entradas']; ?></td>
						<td style="text-align: center; border:1px solid; ">
							<?php echo $produto['total_saidas']; ?></td>
						<td style="text-align: center; border:1px solid; ">
							<?php echo $produto['saldo_fisico']; ?></td>
						
					</tr>
				<?php endforeach; ?>
			</table>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>			
			<table align="center" style="color:#444;font-size:12px;padding-top:20px;">
			    <tr>
			        <td align="center" width="300" style="border-top: 2px solid;">
			        	<strong><h3>MARIA DAS GRAÇAS MORAIS ANDRADE</h3></strong>
			        </td>
			    </tr>


			</table>	
			
		</td>
	</tr>
</table>
</body>
</html>
		
	</table>

</body>
</html>